//
//  GameViewController.m
//  Kin pics
//
//  Created by Rizwan on 2/14/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import "GameViewController.h"
#import "GameManager.h"
#import "GameConfig.h"
#import "Card.h"
#import "CardView.h"

static const CGFloat GAMELOOP_INTERVAL_SEC = 1.0f / 30.0f;
static const CGFloat GAMELOOP_INTERVAL_MAX = 1.0f / 15.0f;
static const CGFloat CARDPLACEMENT_SPACING_X = 10.0f;
static const CGFloat CARDPLACEMENT_SPACING_Y = 10.0f;
static const CGFloat POSTROUND_DELAY = 0.5f;
static const CGFloat POSTGAME_DELAY = 1.0f;

enum GameStates
{
    GAMESTATE_INIT = 0,
    GAMESTATE_INPROGRESS,
    GAMESTATE_POSTROUND,
    GAMESTATE_POSTGAME,
    GAMESTATE_EXITM,
    
    GAMESTATE_NUMM,
    GAMESTATE_INVALID
};

@interface GameViewController ()

@end

@implementation GameViewController
@synthesize gameConfig = _gameConfig;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _gameConfig = nil;
        _gameState = GAMESTATE_INVALID;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    _cardViews = [NSMutableArray array];
    _activeCardViews = [NSMutableArray array];
    _selectedCards = [NSMutableArray array];
    
    [self initGameLoop];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) selectCard:(id)sender
{
    CardView* senderCardView = (CardView*) sender;
    
    if(![_selectedCards containsObject:senderCardView])
    {
        // add it if card not yet selected
        [_selectedCards addObject:senderCardView];
        [senderCardView setState:CARD_STATE_SELECTED];
    }
    
    // if two cards selected, ask GameManager to process them
    if(1 < [_selectedCards count])
    {
        CardView* card1 = [_selectedCards objectAtIndex:0];
        CardView* card2 = [_selectedCards objectAtIndex:1];
        BOOL result = [[GameManager getInstance] matchCards:[card1 card] :[card2 card]];
        if(result)
        {
            // matched, hide and remove them
            [card1 setState:CARD_STATE_MATCHED];
            [card2 setState:CARD_STATE_MATCHED];
            [_activeCardViews removeObject:card1];
            [_activeCardViews removeObject:card2];
            
            // if this was the last pair of cards, we are done with this round
            if(0 == [_activeCardViews count])
            {
                //    _triggerRoundCompleted = YES;
            }
            else if(1 < [[GameManager getInstance] numConsecutiveMatches])
            {
                // upgrade multiplier for next round
                //                [[StatsManager getInstance] upgradeMultiplier];
                
                // show banner
                //                [self showBanner:@"Multiplier Up"];
                //                [self showSubBanner:[NSString stringWithFormat:@"x%d", [[StatsManager getInstance] multiplier]]];
            }
        }
        else
        {
            // not matched, cover them
            [card1 setState:CARD_STATE_ACTIVE];
            [card2 setState:CARD_STATE_ACTIVE];
        }
        
        // refresh UI
        //        [self refreshScoreLabel];
        
        // clear the selected array
        [_selectedCards removeAllObjects];
    }
}
-(void) initGameLoop {
    
    [[GameManager getInstance] newGameWithConfig:[self gameConfig]];
    
    // go to the init-state
    _gameState = GAMESTATE_INIT;
    
    // reset trigger variables
    //    _triggerRoundCompleted = NO;
    //    _delayTimer = 0.0;
    
    [self gameLoop];
}

-(void) gameLoop {
    
    //NSTimeInterval elapsed = [self advanceTimer];
    
    [[GameManager getInstance] newRound];
    [self setupGameViewForNewRound];
    
}
- (void) gameViewExitRound
{
    [_selectedCards removeAllObjects];
    [_activeCardViews removeAllObjects];
    for(CardView* cur in _cardViews)
    {
        [cur removeTarget:self action:@selector(selectCard:) forControlEvents:UIControlEventTouchUpInside];
        [cur removeFromSuperview];
    }
    [_cardViews removeAllObjects];
}

- (void) setupGameViewForNewRound
{
    unsigned int rows = [[[GameManager getInstance] curConfig] numRows];
    unsigned int cols = [[[GameManager getInstance] curConfig] numColumns];
    
    //CGRect myFrame = [_gameView bounds];
   // UIView *gameView = [[UIView alloc]initWithFrame:CGRectMake(0.0f, 100.0f, 768.0f, 800.0f)];
    CGRect myFrame = [_gameView bounds];
    CGFloat cardWidth = floorf((myFrame.size.width - ((cols+1) * CARDPLACEMENT_SPACING_X)) / cols);
    CGFloat cardHeight = cardWidth;
    
    for(unsigned int curRow = 0; curRow < rows; ++curRow)
    {
        for(unsigned int curCol = 0; curCol < cols; ++curCol)
        {
            unsigned int index = (curRow * cols) + curCol;
            Card* curGameCard = [[GameManager getInstance] roundCardAtIndex:index];
            CGFloat posX = CARDPLACEMENT_SPACING_X + (curCol * (CARDPLACEMENT_SPACING_X + cardWidth));
            CGFloat posY = CARDPLACEMENT_SPACING_Y + (curRow * (CARDPLACEMENT_SPACING_Y + cardHeight));
            CGRect cardFrame = CGRectMake(posX, posY, cardWidth, cardHeight);
            
            CardView* newView = [[CardView alloc] initWithFrame:cardFrame forCard:curGameCard];
            [newView addTarget:self action:@selector(selectCard:) forControlEvents:UIControlEventTouchUpInside];
            [_cardViews addObject:newView];
            [_activeCardViews addObject:newView];
            [_gameView addSubview:newView];
            //[newView release];
        }
    }
    [_gameView setNeedsDisplay];
}


@end
