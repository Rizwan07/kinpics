//
//  GameManager.m
//  Kin pics
//
//  Created by Rizwan on 2/14/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import "GameManager.h"
#import "GameConfig.h"
#import "ImageLibrary.h"
#import "Card.h"
#import "CardView.h"

@implementation GameManager
@synthesize curConfig = _curConfig;
@synthesize imageLib = _imageLib;
@synthesize deck = _deck;
@synthesize scratchDeck = _scratchDeck;
@synthesize roundCards = _roundCards;


- (Card*) roundCardAtIndex:(unsigned int)index
{
    Card* result = nil;
    if(index < [_roundCards count])
    {
        result = [_roundCards objectAtIndex:index];
    }
    return result;
}

- (unsigned int) curGameMode
{
    unsigned int result = GAMEMODE_SINGLEPLAYER;
    if(_curConfig)
    {
        result = [_curConfig gameMode];
    }
    return result;
}

- (id) init
{
    self = [super init];
    if(self)
    {
        _curConfig = nil;
        _imageLib = nil;
        _deck = nil;
        _scratchDeck = nil;
        _roundCards = nil;
    }
    return self;
}
- (void) initDeck
{
    _deck = [NSMutableArray arrayWithCapacity:[[[self imageLib] images] count]];
    NSLog(@"This ooooo %@",_deck);
    unsigned int index = 0;
    for(NSString* cur in [[self imageLib] images])
    {
        UIImage* curImage = [[[self imageLib] images] objectForKey:cur];
        Card* newCard = [[Card alloc] initWithIdentifier:index image:curImage];
        [_deck addObject:newCard];
        ++index;
    }
}
- (void) newGameWithConfig:(GameConfig *)config
{
    self.curConfig = config;
    
    ImageLibrary* newLib = [[ImageLibrary alloc] init];
    self.imageLib = newLib;
    NSLog(@"%@",newLib);
    [self initDeck];
    
    _scratchDeck = [NSMutableArray array];
    _roundCards = [NSMutableArray array];
    return;
}

- (void) roundCardsRandomInsertCard:(Card*)newCard
{
    unsigned int roundCardsCount = [_roundCards count];
    unsigned int targetIndex = arc4random_uniform(roundCardsCount);
    if([NSNull null] == [_roundCards objectAtIndex:targetIndex])
    {
        [_roundCards replaceObjectAtIndex:targetIndex withObject:newCard];
    }
    else
    {
        unsigned int stepCount = 0;
        ++targetIndex;
        do
        {
            if(targetIndex >= roundCardsCount)
            {
                targetIndex = 0;
            }
            if([NSNull null] == [_roundCards objectAtIndex:targetIndex])
            {
                [_roundCards replaceObjectAtIndex:targetIndex withObject:newCard];
                break;
            }
            
            ++stepCount;
            ++targetIndex;
        } while (stepCount < roundCardsCount);
    }
}
- (BOOL) matchCard1:(Card *)card1 andCard2:(Card *)card2
{
    BOOL result = NO;
    if([card1 identifier] == [card2 identifier])
    {
        result = YES;
    }
    return result;
}

- (void) newRound
{
    int totalCards = self.curConfig.numRows * self.curConfig.numColumns;
    NSAssert(0 == (totalCards % 2), @"total cards per round must be an even number");
    NSAssert(0 == [_roundCards count], @"clear out roundCards before newRound is called");
    
    for(unsigned int i = 0; i < totalCards; ++i)
    {
        [_roundCards addObject:[NSNull null]];
    }
    
    [_scratchDeck addObjectsFromArray:[self deck]];
    
    while(0 < totalCards)
    {
        if(0 == [_scratchDeck count])
        {
            [_scratchDeck addObjectsFromArray:[self deck]];
        }
        
        unsigned int nextIndex = arc4random_uniform([_scratchDeck count]);
        Card* nextCard = [_scratchDeck objectAtIndex:nextIndex];
        
        Card* newCard1 = [[Card alloc] initWithCard:nextCard];
        Card* newCard2 = [[Card alloc] initWithCard:nextCard];
        [self roundCardsRandomInsertCard:newCard1];
        [self roundCardsRandomInsertCard:newCard2];
        [_scratchDeck removeObjectAtIndex:nextIndex];
        
        totalCards -= 2;
    }
    
    [_scratchDeck removeAllObjects];
        
    // DEBUG
    for(id cur in _roundCards)
    {
        NSAssert([NSNull null] != cur, @"_roundCards did not get completely filled, something is wrong");
    }
}

#pragma mark - Singleton
static GameManager *singleton = nil;

+ (GameManager*) getInstance
{
	@synchronized(self)
	{
		if (!singleton)
		{
			singleton = [[GameManager alloc] init];
		}
	}
	return singleton;
}

+ (void) destroyInstance
{
	@synchronized(self)
	{
		singleton = nil;
	}
}

@end
