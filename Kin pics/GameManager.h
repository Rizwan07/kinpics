//
//  GameManager.h
//  Kin pics
//
//  Created by Rizwan on 2/14/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GameConfig.h"

@class ImageLibrary;
@class Card;

@interface GameManager : NSObject
{
    GameConfig *_curConfig;
    
    ImageLibrary *_imageLib;
    NSMutableArray *_deck;
    NSMutableArray* _scratchDeck;
    NSMutableArray* _roundCards;
    
    unsigned int _gameState;
}
@property (nonatomic,retain) GameConfig* curConfig;
@property (nonatomic,retain) ImageLibrary* imageLib;
@property (nonatomic,retain) NSMutableArray* deck;
@property (nonatomic,retain) NSMutableArray* scratchDeck;
@property (nonatomic,retain) NSMutableArray* roundCards;

- (Card*) roundCardAtIndex:(unsigned int)index;
- (unsigned int) curGameMode;

- (void) newGameWithConfig:(GameConfig*)config;
- (void) newRound;

- (BOOL) matchCard1:(Card*)card1 andCard2:(Card*)card2;

+ (GameManager*) getInstance;
+ (void) destroyInstance;
- (void) initDeck;

@end
