//
//  PlayCardsViewController.m
//  Kin pics
//
//  Created by Rizwan on 2/3/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import "PlayCardsViewController.h"
#import "GameConfig.h"
#import "Card.h"
#import "CardView.h"
#import "GameManager.h"
#import <QuartzCore/QuartzCore.h>

#define CARDPLACEMENT_SPACING_X 10.0f
#define CARDPLACEMENT_SPACING_Y 10.0f

enum GameStates
{
    GAMESTATE_INIT = 0,
    GAMESTATE_INVALID
};

@interface PlayCardsViewController ()

@property (nonatomic, strong)AVAudioPlayer *audioPlayer;

@end

@implementation PlayCardsViewController
@synthesize gameConfig = _gameConfig;
@synthesize audioPlayer = _audioPlayer;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
//    [image setHidden:YES];
//    [lblJob setHidden:YES];
    [playBtn setHidden:YES];
    [[Singleton sharedSingleton] setSelectedOption:9];
    
    [self singleplayerGameConfigWithRow:3 andColumn:6];
    _cardViews = [NSMutableArray array];
    _activeCardViews = [NSMutableArray array];
    _selectedCards = [NSMutableArray array];
    [self initGameLoop];
    
    NSString *string = [[NSBundle mainBundle]pathForResource:@"great" ofType:@"mp3"];
    NSURL *url = [NSURL fileURLWithPath:string];
    self.audioPlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:url error:nil];
    [_audioPlayer prepareToPlay];

}
- (GameConfig*)singleplayerGameConfigWithRow:(int )row andColumn:(int )column {
    
    GameConfig* newConfig = [[GameConfig alloc] initWithImageLibraryName:@"CardImages" numRows:row numColumns:column];
    newConfig.gameMode = GAMEMODE_SINGLEPLAYER;
    
    _gameConfig = nil;
    _gameState = GAMESTATE_INVALID;
    _gameConfig = newConfig;

    
    return newConfig;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
        
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)imagesBtnPressed:(id)sender {
    
    UIButton *button = (UIButton *)sender;
    
    [popOver presentPopoverFromRect:button.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

- (IBAction)backBtnPressed:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    [[Singleton sharedSingleton] setDidHomeBtnPressed:NO];
}

- (IBAction)homeBtnPressed:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    [[Singleton sharedSingleton] setDidHomeBtnPressed:YES];
}
- (void)removeAllCards {
    for (UIView *view in [self.view subviews]) {
        if ([view tag] >= 1000 && [view tag] < 1000+18) {
            [view removeFromSuperview];
        }
    }
}

- (IBAction)cardOptionBtnsPressed:(id)sender {
    
    int tag = [sender tag];
    
    [btnSixOption setEnabled:tag != 0];
    [btnTwelveOption setEnabled:tag != 1];
    [btnEighteenOption setEnabled:tag != 2];
    [[Singleton sharedSingleton] setSelectedOption:(tag+1)*3];

    switch (tag) {
        case 0:
//            [image setHidden:YES];
//            [lblJob setHidden:YES];
            [jobView setHidden:YES];
            [self singleplayerGameConfigWithRow:2 andColumn:3];
            break;
        case 1:
//            [image setHidden:YES];
//            [lblJob setHidden:YES];
            [jobView setHidden:YES];

            [self singleplayerGameConfigWithRow:3 andColumn:4];
            break;
        case 2:
//            [image setHidden:YES];
//            [lblJob setHidden:YES];
            [jobView setHidden:YES];

            [self singleplayerGameConfigWithRow:3 andColumn:6];
            break;
            
        default:
            break;
    }
    
    _cardViews = [NSMutableArray array];
    _activeCardViews = [NSMutableArray array];
    _selectedCards = [NSMutableArray array];
    
    [self removeAllCards];
    [self initGameLoop];
    [playBtn setHidden:YES];
}

- (IBAction)playBtnPressed:(id)sender {
    
    [playBtn setHidden:YES];
    
    if(![btnSixOption isEnabled]){
        [self singleplayerGameConfigWithRow:2 andColumn:3];
    }
    else if (![btnTwelveOption isEnabled]){
        [self singleplayerGameConfigWithRow:3 andColumn:4];
    }
    else if (![btnEighteenOption isEnabled]){
        [self singleplayerGameConfigWithRow:3 andColumn:6];
    }
    _cardViews = [NSMutableArray array];
    _activeCardViews = [NSMutableArray array];
    _selectedCards = [NSMutableArray array];
    
    [self removeAllCards];
    [self initGameLoop];

}
- (void)selectCard:(id)sender
{
    CardView* senderCardView = (CardView*) sender;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults boolForKey:@"cards"]== NO){
        [UIView transitionWithView:sender duration:1.0 options:UIViewAnimationOptionTransitionFlipFromRight animations:^{} completion:nil];
    }
    else{
    if(![_selectedCards containsObject:senderCardView])
    {
        [UIView transitionWithView:sender duration:1.0 options:UIViewAnimationOptionTransitionFlipFromRight animations:^{
            
        } completion:nil
         ];

        [_selectedCards addObject:senderCardView];
        [senderCardView setState:CARD_STATE_SELECTED];
    }
    if([_selectedCards count] > 1)
    {
        CardView* card1 = [_selectedCards objectAtIndex:0];
        CardView* card2 = [_selectedCards objectAtIndex:1];
        BOOL result = [[GameManager getInstance] matchCard1:[card1 card] andCard2:[card2 card]];
        if(result)
        {
            [card1 setState:CARD_STATE_MATCHED];
            [card2 setState:CARD_STATE_MATCHED];
            [UIView transitionWithView:image duration:1.8f options:UIViewAnimationOptionTransitionCrossDissolve animations:^{[card1 setAlpha:0.3];
                [card2 setAlpha:0.3];
            } completion:^(BOOL finished){
                [card1 setAlpha:1.0];
                [card2 setAlpha:1.0];
                
            }

                
            
             ];
            
            if (![jobView isHidden]) {
                [jobView setHidden:YES];
            }
            
            [_activeCardViews removeObject:card1];
            [_activeCardViews removeObject:card2];
//            [self.view bringSubviewToFront:image];
//            [self.view bringSubviewToFront:lblJob];
            
            _audioPlayer.currentTime = 0;
            [_audioPlayer play];
            
            [self.view bringSubviewToFront:jobView];
            [UIView transitionWithView:jobView duration:1.8f options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                [jobView setHidden:NO];

            } completion:^(BOOL finished){
//                [image setHidden:YES];
//                [lblJob setHidden:YES];
                
                [jobView setHidden:YES];
            }
             ];
           
            
            if([_activeCardViews count]==0)
            {
                [playBtn setHidden:NO];
//                [UIView transitionWithView:jobView duration:1.8f options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
//                    [jobView setHidden:NO];
//                                        
//                } completion:^(BOOL finished){
//                   [playBtn setHidden:NO];                    
//                }
//                 ];
            }
        }
        else
        {
            [card1 setState:CARD_STATE_ACTIVE];
            [card2 setState:CARD_STATE_ACTIVE];
        }
        
        [_selectedCards removeAllObjects];
    }
    }
}
-(void) initGameLoop {
    
    [[GameManager getInstance] newGameWithConfig:[self gameConfig]];
    
    _gameState = GAMESTATE_INIT;
    
    [[GameManager getInstance] newRound];
    [self setupGameViewForNewRound];
}

- (void) gameViewExitRound
{
    [_selectedCards removeAllObjects];
    [_activeCardViews removeAllObjects];
    for(CardView* cur in _cardViews)
    {
        [cur removeTarget:self action:@selector(selectCard:) forControlEvents:UIControlEventTouchUpInside];
        [cur removeFromSuperview];
    }
    [_cardViews removeAllObjects];
}

- (void) setupGameViewForNewRound
{
    unsigned int rows = [[[GameManager getInstance] curConfig] numRows];
    unsigned int cols = [[[GameManager getInstance] curConfig] numColumns];
    
    CGFloat cardWidth = 107;
    CGFloat cardHeight = 150;
    CGFloat cardWidth1 = 147;
    CGFloat cardHeight1 = 190;
    CGRect cardFrame = CGRectZero;
    for(unsigned int curRow = 0; curRow < rows; ++curRow)
    {
        for(unsigned int curCol = 0; curCol < cols; ++curCol)
        {
            unsigned int index = (curRow * cols) + curCol;
            Card* curGameCard = [[GameManager getInstance] roundCardAtIndex:index];
            CGFloat posX = 0;
            CGFloat posY = 0;
            if (rows == 3 && cols == 6) {
                posX = 30 + 120*curCol;
                posY = CARDPLACEMENT_SPACING_Y + 180+(curRow * (CARDPLACEMENT_SPACING_Y*2 + cardHeight1));
                
                cardFrame = CGRectMake(posX, posY, cardWidth, cardHeight);

            }
            else if (rows == 3 && cols == 4) {
                posX = 74 + 170 *curCol;
                posY = CARDPLACEMENT_SPACING_Y +180+ (curRow * (CARDPLACEMENT_SPACING_Y*3 + cardHeight));
                
                cardFrame = CGRectMake(posX, posY, cardWidth, cardHeight);

            }
            else if (rows == 2 && cols == 3) {
                posX = 52 + 257*curCol;
                posY = CARDPLACEMENT_SPACING_Y*8 + 180 + (curRow * (CARDPLACEMENT_SPACING_Y*10 + cardHeight));
                
                cardFrame = CGRectMake(posX, posY, cardWidth1, cardHeight1);

            }
            
            newView = [[CardView alloc] initWithFrame:cardFrame forCard:curGameCard];
            
            [newView.layer setMasksToBounds:YES];
            [newView.layer setCornerRadius:8.0];
            
            [newView addTarget:self action:@selector(selectCard:) forControlEvents:UIControlEventTouchUpInside];
            [_cardViews addObject:newView];
            [_activeCardViews addObject:newView];
            [newView setTag:1000+index];
            [self.view addSubview:newView];
        }
    }
}

- (void)viewDidUnload {
    
    image = nil;
    [super viewDidUnload];
}

@end
