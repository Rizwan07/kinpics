//
//  DifferentLookViewController.h
//  Kin pics
//
//  Created by Rizwan on 1/21/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SetUpDifferentLookViewController : UIViewController {
    
    IBOutlet UIScrollView *theScrollView;
    BOOL didScroll;
    IBOutlet UIButton *btnSave;
    UIImage *myImage;
    NSMutableArray *mutableArrPics;
    IBOutlet UIImageView *dummyImageView;
    int previousSelectedOption;
    int previousSelectedIndex;
    
    NSArray *arrPics;
}

- (IBAction)backBtnPressed:(id)sender;
- (IBAction)homeBtnPressed:(id)sender;

- (IBAction)forwardBtnPressed:(id)sender;
- (IBAction)previousBtnPressed:(id)sender;

- (IBAction)lookBtnPressed:(id)sender;

- (IBAction)saveBtnPressed:(id)sender;

@end
