//
//  CardView.h
//  Kin pics
//
//  Created by Rizwan on 2/14/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

enum CardStates
{
    CARD_STATE_ACTIVE = 0,
    CARD_STATE_SELECTED,
    CARD_STATE_MATCHED,
    
    CARD_STATE_NUM
};
@class Card;

@interface CardView : UIControl
{
    Card *_card;
    UIImageView *_imageView;
    UIImageView *imagev;
    unsigned int _state;
    CardView *view;
}

@property (nonatomic, retain) Card *card;
@property (nonatomic, assign)unsigned int state;
@property (nonatomic, retain) UIImage *image;

-(id) initWithFrame:(CGRect)frame forCard:(Card *)gameCard;

@end
