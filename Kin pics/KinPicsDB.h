//
//  KinPicsDB.h
//  Kin pics
//
//  Created by Rizwan on 1/26/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface KinPicsDB : NSObject {
    
    NSString *dbname,*dbPath;
    
}

- (void)addKinPicsWithName:(NSString *)name voice:(NSData *)voice text:(NSString *)text heartVoice:(NSData *)heartVoice;
- (void)updateKinPicsVoice:(NSData *)voice heartVoice:(NSData *)heartVoice text:(NSString *)text wherePicName:(NSString *)picName;
- (void)deleteKinPicsImageWithName:(NSString *)picsName;

- (NSArray *)getAllKinPics;
- (NSArray *)getDataOfSlices;

- (KinPicsVoice *)getVoicesOfKinPic:(NSString *)name;

//- (BOOL)CopyDatabaseIfNeeded;

- (void)updateKinPicsImageSlicesWithName:(NSString *)picName andNumberOfSlices:(NSInteger )slices;


@end
