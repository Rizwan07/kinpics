//
//  SlideShowViewController.m
//  Kin pics
//
//  Created by Rizwan on 1/20/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import "SetUpSlideShowViewController.h"
#import "SetUpImageSlideShowViewController.h"

#import "UIImage+fixOrientation.h"

@interface SetUpSlideShowViewController ()

@property (nonatomic, retain)UIImagePickerController *imagePickerController;

@end

@implementation SetUpSlideShowViewController
@synthesize imagePickerController = _imagePickerController;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.imagePickerController = [[UIImagePickerController alloc]init];
    _imagePickerController.delegate = self;
    _imagePickerController.allowsEditing = NO;
    _imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;

    popOver = [[UIPopoverController alloc]initWithContentViewController:_imagePickerController];
    [popOver setDelegate:self];
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self removePreviousAddedImagesFromScrollView];
    
    arrPics = [[Singleton sharedSingleton] getDataOfAllKinPics];
    
    NSInteger numberOfImages = [arrPics count];
    
    int totalNumberPage = (numberOfImages%8 == 0)? numberOfImages/8 : numberOfImages/8+1;
    
    [scrollView setContentSize:CGSizeMake(768*totalNumberPage, scrollView.frame.size.height)];
    
    NSString *filePath = [[Singleton sharedSingleton] getDocumentDirectoryPath];

    
    for (int i = 0; i < numberOfImages; i++) {
        
        KinPics *kinPics = [arrPics objectAtIndex:i];
        
        NSString *fileName = [filePath stringByAppendingPathComponent:[kinPics name]];
        
        int xOffset = i/8 * 768;
        int yOffset = (i%8)/4 * 190;
        
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"image-border.png"]];
        [imageView setContentMode:UIViewContentModeScaleAspectFit];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:[UIImage imageWithContentsOfFile:fileName] forState:UIControlStateNormal];
        [button setTag:i];
        [button addTarget:self action:@selector(imageBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        if (i%4 == 0) {
            [imageView setFrame:CGRectMake(20 + xOffset, 20 + yOffset, 160, 150)];
            [button setFrame:CGRectMake(40 + xOffset, 40 + yOffset, 120, 110)];
        }
        else if (i%4 == 1) {
            [imageView setFrame:CGRectMake(20 + 187 + xOffset, 20 + yOffset, 160, 150)];
            [button setFrame:CGRectMake(40 + 187 + xOffset, 40 + yOffset, 120, 110)];
        }
        else if (i%4 == 2) {
            [imageView setFrame:CGRectMake(20 + 374 + xOffset, 20 + yOffset, 160, 150)];
            [button setFrame:CGRectMake(40 + 374 + xOffset, 40 + yOffset, 120, 110)];
        }
        else if (i%4 == 3) {
            [imageView setFrame:CGRectMake(20 + 561 + xOffset, 20 + yOffset, 160, 150)];
            [button setFrame:CGRectMake(40 + 561 + xOffset, 40 + yOffset, 120, 110)];
        }
        [scrollView addSubview:imageView];
        [scrollView addSubview:button];
        
    }

}

- (void)removePreviousAddedImagesFromScrollView {
    
    for (UIView *view in [scrollView subviews]) {
        [view removeFromSuperview];
    }
}


- (void)imageBtnPressed:(id)sender {
    
    UIButton *button = (UIButton *)sender;
    
    NSLog(@"%d button pressed", [button tag]);
    
    SetUpImageSlideShowViewController *slideVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SetUpImageSlideShowViewController"];
    [slideVC setArrPics:arrPics];
    [slideVC setSelectedImageIndex:[button tag]];
    //[slVC setOrgImage:[button imageForState:UIControlStateNormal]];
    [self.navigationController pushViewController:slideVC animated:YES];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cameraBtnPressed:(id)sender {
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.allowsEditing = NO;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
    
}

- (IBAction)uploadBtnPressed:(id)sender {
    
    UIButton *button = (UIButton *)sender;

    [popOver presentPopoverFromRect:button.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    UIImage *fixImage = [image fixOrientation];
    SetUpImageSlideShowViewController *slideVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SetUpImageSlideShowViewController"];
    [slideVC setArrPics:nil];
    [slideVC setOrgImage:fixImage];
    [slideVC setSelectedImageIndex:0];
    [self.navigationController pushViewController:slideVC animated:YES];
    
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Instruction" message:@"Press and hold icon to record, use heart to assign message, use play icon to label photo text, press icon to play back" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alertView show];
    if (_imagePickerController == picker) {
        [popOver dismissPopoverAnimated:YES];
    }
    else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)backBtnPressed:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    [[Singleton sharedSingleton] setDidHomeBtnPressed:NO];
}

- (IBAction)homeBtnPressed:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    [[Singleton sharedSingleton] setDidHomeBtnPressed:YES];
}
- (void)viewDidUnload{
    [super viewDidUnload];
    
}

@end
