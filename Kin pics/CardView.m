//
//  CardView.m
//  Kin pics
//
//  Created by Rizwan on 2/14/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import "CardView.h"
#import "Card.h"
#import <QuartzCore/QuartzCore.h>

//static const float SHAKE_INTERVAL = 0.05f;

@implementation CardView
@synthesize card = _card;

- (unsigned int) state
{
    return _state;
}
- (void) setState:(unsigned int)state
{
    switch(state)
    {
        case CARD_STATE_ACTIVE:{
            [self setEnabled:NO];
            [UIView animateWithDuration:0.3f
                                  delay:1.3f
                                options:UIViewAnimationOptionTransitionFlipFromRight
                             animations:^{
                                 [imagev setAlpha:1.0f];
                                 [_imageView setAlpha:0.0f];
                             }
                             completion:^(BOOL finished){
                                 [imagev setHidden:NO];
                                 [_imageView setHidden:YES];
                                 [self setEnabled:YES];
                             }];
            }
        break;
        
        case CARD_STATE_SELECTED:{
            [self setBackgroundColor:[UIColor clearColor]];
        
                [imagev setHidden:YES];

                [_imageView setAlpha:1.0f];
                [_imageView setHidden:NO];
            }
            break;
        
        case CARD_STATE_MATCHED:{
            
            [self setEnabled:NO];
            //[self setBackgroundColor:[UIColor clearColor]];
            [_imageView setAlpha:1.0f];
            //[_imageView setHidden:NO];
//            [UIView animateWithDuration:0.3f
//                                  delay:1.3f
//                                options:UIViewAnimationTransitionCurlUp
//                             animations:^{
//                                 [self setAlpha:0.0f];
//                             }
//                             completion:^(BOOL finished){
//                                 [self setHidden:YES];
//                             }];
        }
            break;
        
    }
    [self setNeedsDisplay];
}

- (id)initWithFrame:(CGRect)frame forCard:(Card *)gameCard
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        _card = gameCard;
        
        _imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0.0f, 0.0f, frame.size.width, frame.size.height)];
        _imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [_imageView setImage:[_card image]];
        [self addSubview:_imageView];
        
        [[self layer] setCornerRadius:8.0f];
        [[self layer] setMasksToBounds:YES];
        [[self layer] setBorderWidth:2.0f];
        [[self layer] setBorderColor:[[UIColor colorWithRed:1.0f green:1.0f blue:1.0f alpha:0.5f] CGColor]];
        
        _state = CARD_STATE_ACTIVE;
        
        UIImage *image = [UIImage imageNamed:@"card.png"];
        imagev = [[UIImageView alloc]initWithFrame:CGRectMake(0.0f, 0.0f, frame.size.width, frame.size.height)];
        [imagev setImage:image];
        [self addSubview:imagev];
        
        [_imageView setHidden:YES];

    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
