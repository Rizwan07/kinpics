//
//  ImageLib.m
//  Kin pics
//
//  Created by Rizwan on 2/14/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import "ImageLib.h"

@implementation ImageLib
@synthesize images = _images;
- (id) initWithPlistNamed:(NSString *)plistFilename
{
    self = [super init];
    if(self)
    {
        //int selectedOption = [[Singleton sharedSingleton] options];
        
        NSString* path = [[NSBundle mainBundle] pathForResource:plistFilename ofType:@"plist"];
        NSDictionary* config = [NSDictionary dictionaryWithContentsOfFile:path];
        NSArray* nameArray = [config objectForKey:@"images"];
        [self loadImagesFromNameArray:nameArray];
    }
    return self;
}
- (void) loadImagesFromNameArray:(NSArray *)nameArray
{
    _images = [NSMutableDictionary dictionary];
    
    if(nameArray)
    {
        for(NSString* cur in nameArray)
        {
            UIImage* newImage = [UIImage imageNamed:cur];
            if(newImage)
            {
                [_images setObject:newImage forKey:cur];
            }
        }
    }
}

@end
