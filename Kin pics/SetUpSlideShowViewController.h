//
//  SlideShowViewController.h
//  Kin pics
//
//  Created by Rizwan on 1/20/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SetUpSlideShowViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate, UIPopoverControllerDelegate> {
    
    UIPopoverController *popOver;
    IBOutlet UIScrollView *scrollView;
    
    NSArray *arrPics;
}

- (IBAction)cameraBtnPressed:(id)sender;
- (IBAction)uploadBtnPressed:(id)sender;

- (IBAction)backBtnPressed:(id)sender;
- (IBAction)homeBtnPressed:(id)sender;

@end
