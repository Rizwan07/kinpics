//
//  MatchSameViewController.h
//  Kin pics
//
//  Created by Rizwan on 1/21/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableViewController.h"

@interface SetUpCardsViewController : UIViewController<UIPopoverControllerDelegate> {
    
    IBOutlet UIButton *btnSixOption;
    IBOutlet UIButton *btnTwelveOption;
    IBOutlet UIButton *btnEighteenOption;
    
    IBOutlet UIButton *resetBtn;
    UIPopoverController *popOver;
    TableViewController *tableViewController;
    
    NSArray *arrPics;
    NSArray *arrrrrr;
    
    BOOL isFront;
}
- (IBAction)imagesBtnPressed:(id)sender;
- (IBAction)resetBtnPressed:(id)sender;

- (IBAction)backBtnPressed:(id)sender;
- (IBAction)homeBtnPressed:(id)sender;

- (IBAction)cardOptionBtnsPressed:(id)sender;

@property (nonatomic, retain)NSArray *array;

@end
