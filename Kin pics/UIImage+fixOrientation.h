//
//  UIImage+fixOrientation.h
//  Kin pics
//
//  Created by Rizwan on 4/1/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (fixOrientation)

- (UIImage *)fixOrientation;

@end
