//
//  PlaySlideShowViewController.m
//  Kin pics
//
//  Created by Rizwan on 2/3/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import "PlaySlideShowViewController.h"
#import "KinPicsDB.h"

@interface PlaySlideShowViewController () {
    
    KinPicsDB *kpDb;
}

@property (nonatomic, retain) AVAudioPlayer *audioPlayer;
@property (nonatomic, retain) AVAudioPlayer *audioForAll;

@property (nonatomic, retain) KinPicsVoice *kpVoice;

@end

@implementation PlaySlideShowViewController

@synthesize audioPlayer = _audioPlayer;
@synthesize audioForAll = _audioForAll;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    kpDb = [[KinPicsDB alloc] init];
    
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = [dirPaths objectAtIndex:0];
    
    NSString *sound4AllFilePath = [docsDir stringByAppendingPathComponent:@"soundForAll.caf"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if ([fileManager fileExistsAtPath:sound4AllFilePath]) {
        
        NSURL *sound4AllFileURL = [NSURL fileURLWithPath:sound4AllFilePath];
        self.audioForAll = [[AVAudioPlayer alloc]initWithContentsOfURL:sound4AllFileURL error:nil];
        [_audioForAll prepareToPlay];
    }
    didScroll = YES;
	// Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self removePreviousAddedImagesFromScrollView];
    
    arrPics = [[Singleton sharedSingleton] getDataOfAllKinPics];
    
    NSInteger numberOfImages = [arrPics count];
    
    [theScrollView setContentSize:CGSizeMake(theScrollView.frame.size.width*numberOfImages, theScrollView.frame.size.height)];
    //[theScrollView setContentOffset:CGPointMake(theScrollView.frame.size.width * _selectedImageIndex, 0)];
    
    NSString *filePath = [[Singleton sharedSingleton] getDocumentDirectoryPath];
    
    for (int i = 0; i < numberOfImages; i++) {
        
        KinPics *kinPics = [arrPics objectAtIndex:i];
        
        NSString *fileName = [filePath stringByAppendingPathComponent:[kinPics name]];
        
        int xOffset = i * theScrollView.bounds.size.width;
        
        
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(10 + xOffset, 10, theScrollView.frame.size.width - 20, theScrollView.frame.size.height)];
        [imgView setImage:[UIImage imageWithContentsOfFile:fileName]];
        [imgView setContentMode:UIViewContentModeScaleAspectFit];
        
        [theScrollView addSubview:imgView];
        int imageIndex = theScrollView.contentOffset.x/theScrollView.frame.size.width;

        [btnImageSound setTitle:[[arrPics objectAtIndex:imageIndex] text] forState:UIControlStateNormal];

    }
    //[lblImage setText:[[arrPics objectAtIndex:0] text]];
    
}

- (void)removePreviousAddedImagesFromScrollView {
    
    for (UIView *view in [theScrollView subviews]) {
        [view removeFromSuperview];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backBtnPressed:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    [[Singleton sharedSingleton] setDidHomeBtnPressed:NO];

}

- (IBAction)homeBtnPressed:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    [[Singleton sharedSingleton] setDidHomeBtnPressed:YES];
}

- (IBAction)previousBtnPressed:(id)sender {
    
    if (didScroll) {
        
        CGRect rect = [theScrollView frame];
        rect.origin = [theScrollView contentOffset];
        rect.origin.x -= theScrollView.frame.size.width;
        if (rect.origin.x >= 0) {
            didScroll = NO;
            [self scrollViewScrollToRect:rect];
            //[theScrollView scrollRectToVisible:rect animated:YES];
        }
    }
    
}

- (IBAction)forwardBtnPressed:(id)sender {
    
    if (didScroll) {
        
        CGRect rect = [theScrollView frame];
        rect.origin = [theScrollView contentOffset];
        rect.origin.x += theScrollView.frame.size.width;
        if (rect.origin.x < theScrollView.contentSize.width) {
            didScroll = NO;
            [self scrollViewScrollToRect:rect];
            //[theScrollView scrollRectToVisible:rect animated:YES];
        }
    }
}

- (IBAction)soundBtnPressed:(id)sender {

//    self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:sound4AllFileURL error:nil];
//    [_audioForAll play];
//    if (audioPlayer != nil) {
//        [audioPlayer play];
//    }
    if ([arrPics count]) {
        
        int imageIndex = theScrollView.contentOffset.x/theScrollView.frame.size.width;
        self.kpVoice = [kpDb getVoicesOfKinPic:[[arrPics objectAtIndex:imageIndex] name]];
        NSData *data = [_kpVoice heartVoice];
        //NSData *data = [[arrPics objectAtIndex:imageIndex] heartVoice];
        
        self.audioForAll = [[AVAudioPlayer alloc] initWithData:data error:nil];
        [_audioForAll play];
    }
}

- (void)scrollViewScrollToRect:(CGRect )rect {
    
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionAllowAnimatedContent
                     animations:^{
                         [theScrollView scrollRectToVisible:rect animated:YES];
                         [theScrollView setAlpha:0.7];
                         
                     }
                     completion:^(BOOL finished){
                         
                        [theScrollView setAlpha:1.0];
                     }];
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView 
{
    didScroll = YES;
    int imageIndex = theScrollView.contentOffset.x/theScrollView.frame.size.width;
    
    [btnImageSound setTitle:[[arrPics objectAtIndex:imageIndex] text] forState:UIControlStateNormal];
    
    //[lblImage setText:[[arrPics objectAtIndex:imageIndex] text]];

}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    
    didScroll = YES;
    int imageIndex = theScrollView.contentOffset.x/theScrollView.frame.size.width;
    
    [btnImageSound setTitle:[[arrPics objectAtIndex:imageIndex] text] forState:UIControlStateNormal];
    
    //[lblImage setText:[[arrPics objectAtIndex:imageIndex] text]];

}

- (void)viewDidUnload {
    btnImageSound = nil;
    [self setAudioPlayer:nil];
    [super viewDidUnload];
}
- (IBAction)imageSoundBtnPressed:(id)sender {
    
    if ([arrPics count]) {
        
        int imageIndex = theScrollView.contentOffset.x/theScrollView.frame.size.width;
        self.kpVoice = [kpDb getVoicesOfKinPic:[[arrPics objectAtIndex:imageIndex] name]];
        NSData *data = [_kpVoice textVoice];
        //NSData *data = [[arrPics objectAtIndex:imageIndex] voice];
        
        self.audioPlayer = [[AVAudioPlayer alloc] initWithData:data error:nil];
        [_audioPlayer play];
    }

}
@end
