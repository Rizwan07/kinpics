//
//  Singleton.m
//  Kin pics
//
//  Created by Rizwan on 2/3/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import "Singleton.h"
#import "KinPicsDB.h"

@implementation Singleton

@synthesize didEnterInSetUpOptions = _didEnterInSetUpOptions;
@synthesize didHomeBtnPressed = _didHomeBtnPressed;
@synthesize selectedOption = _selectedOption;

static Singleton *single = nil;

+ (Singleton *)sharedSingleton {
    if (single == nil) {
        single = [[Singleton alloc] init];
    }
    return single;
}

- (NSArray *)getDataOfAllKinPics {
    
    if (arrKinPics == nil) {
        
        KinPicsDB *dbData = [[KinPicsDB alloc] init];
        arrKinPics = [dbData getAllKinPics];

    }
    return arrKinPics;
}

- (void)removeAllKinPicsData {
    arrKinPics = nil;
}

- (NSString *)getDocumentDirectoryPath {
    
    if (docDirectoryPath == nil) {
        
        NSArray *docspaths=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        docDirectoryPath = [docspaths objectAtIndex:0];
    }
    return docDirectoryPath;
}

@end
