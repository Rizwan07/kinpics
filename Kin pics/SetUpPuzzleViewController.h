//
//  PuzzleViewController.h
//  Kin pics
//
//  Created by Rizwan on 1/21/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Tile.h"
typedef enum {
	NONE			= 0,
	UP				= 1,
	DOWN			= 2,
	LEFT			= 3,
	RIGHT			= 4
} ShuffleMove;

@interface SetUpPuzzleViewController : UIViewController {
    
    CGFloat tileWidth;
    CGFloat tileHeight;
    
    NSMutableArray *tiles;
    CGPoint blankPosition;
    
    Tile *tileImageView;
    
    BOOL touchCancel;
    NSTimer *timer;

    NSArray *arrPics;
    
    IBOutlet UIScrollView *theScrollView;
    IBOutlet UILabel *lblPicName;
    BOOL didScroll;
    IBOutlet UIButton *btnTwoOption;
    IBOutlet UIButton *btnFourOption;
    IBOutlet UIButton *btnSixOption;
    IBOutlet UIButton *btnEightOption;
}

@property (nonatomic, retain) NSMutableArray *tiles;
@property (nonatomic, retain) UIImage *orgImage;

@property (retain, nonatomic) IBOutlet UISwitch *CountMoves;
@property (retain, nonatomic) IBOutlet UISwitch *Timer;

- (IBAction)backBtnPressed:(id)sender;
- (IBAction)homeBtnPressed:(id)sender;

- (IBAction)previousBtnPressed:(id)sender;
- (IBAction)forwardBtnPressed:(id)sender;

- (IBAction)sliceBtnsPressed:(id)sender;
- (IBAction)pieceBtnPressed:(id)sender;
@end
