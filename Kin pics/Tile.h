//
//  Tile.h
//  Kin pics
//
//  Created by Rizwan on 1/31/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Tile : UIImageView{
    
	CGPoint originalPosition;
	CGPoint currentPosition;
}

@property (nonatomic,readwrite) CGPoint originalPosition;
@property (nonatomic,readwrite) CGPoint currentPosition;


@end
