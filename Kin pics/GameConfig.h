//
//  GameConfig.h
//  Kin pics
//
//  Created by Rizwan on 2/14/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

enum GAMEMODES
{
    GAMEMODE_SINGLEPLAYER = 0,
    GAMEMODE_NUM
};

@interface GameConfig : NSObject{
    
    unsigned int _gameMode;
    unsigned int _numRows;
    unsigned int _numColumns;
    NSString *_imageLibName;
}
@property (nonatomic,assign) unsigned int gameMode;
@property (nonatomic,readonly) unsigned int numRows;
@property (nonatomic,readonly) unsigned int numColumns;
@property (nonatomic,readonly) NSString* imageLibName;

- (id) initWithImageLibraryName:(NSString*)libname numRows:(unsigned int)rows numColumns:(unsigned int)cols;


@end
