//
//  MatchSameViewController.m
//  Kin pics
//
//  Created by Rizwan on 1/21/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import "SetUpCardsViewController.h"
#import "TableViewController.h"

#import <QuartzCore/QuartzCore.h>

@interface SetUpCardsViewController ()

@end

@implementation SetUpCardsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    tableViewController = [[TableViewController alloc]initWithNibName:nil bundle:nil];
    [tableViewController setContentSizeForViewInPopover:CGSizeMake(250, 400)];
    
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:tableViewController];
    popOver = [[UIPopoverController alloc]initWithContentViewController:nav];
    
    [popOver setDelegate:self];
    
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    isFront = YES;
    
    arrPics = [[Singleton sharedSingleton] getDataOfAllKinPics];
    
    [self cardOptionBtnsPressed:btnEighteenOption];
    
}

- (void) receiveTestNotification:(NSNotification *) notification
{
    arrPics = [[Singleton sharedSingleton] getDataOfAllKinPics];
    
    if ([[notification name] isEqualToString:@"TestNotification"]) {
        
        NSArray *arrSelectedIndexes = [notification object];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"TestNotification" object:nil];
        
        [popOver dismissPopoverAnimated:YES];
        NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
        
        int k=0;
        
        for (int i = 0; i < [arrPics count]; i ++) {
            
            KinPics *kinPic = [arrPics objectAtIndex:i];
            
            if ([[arrSelectedIndexes objectAtIndex:i]boolValue] == 1){
                
                if (![btnSixOption isEnabled]){
                    [user setBool:TRUE forKey:@"3cards"];
                    [user setObject:[NSString stringWithFormat:@"small%@",[kinPic name]] forKey:[NSString stringWithFormat:@"%d_3",k]];
                    [resetBtn setEnabled:YES];
                }
                else if (![btnTwelveOption isEnabled]){
                    [user setBool:TRUE forKey:@"6cards"];
                    [user setObject:[NSString stringWithFormat:@"small%@",[kinPic name]] forKey:[NSString stringWithFormat:@"%d_6",k]];
                    [resetBtn setEnabled:YES];
                }
                else if (![btnEighteenOption isEnabled]){
                    [user setBool:TRUE forKey:@"9cards"];
                    [user setObject:[NSString stringWithFormat:@"small%@",[kinPic name]] forKey:[NSString stringWithFormat:@"%d_9",k]];
                    [resetBtn setEnabled:YES];
                }
                [self changeBtnImageWithIndex:i andTag:k];
                k++;

            }
            else if ([[arrSelectedIndexes objectAtIndex:i]boolValue] == 0){
            }
        }
    }
}

- (void)changeBtnImageWithIndex:(int)i andTag:(int)tag {
    
    
    NSString *filePath = [[Singleton sharedSingleton] getDocumentDirectoryPath];
    KinPics *kinPics = [arrPics objectAtIndex:i];
    
    NSString *fileName = [filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"small%@",[kinPics name]]];
    UIImage *image = [UIImage imageWithContentsOfFile:fileName];
    
    UIButton *imageView = (UIButton *)[self.view viewWithTag:1000+tag];
    [imageView setImage:image forState:UIControlStateNormal];
    if ([btnTwelveOption isEnabled] && [btnEighteenOption isEnabled]) {
        UIButton *im = (UIButton *)[self.view viewWithTag:1003+tag];
        [im setImage:image forState:UIControlStateNormal];
    }
    else if ([btnSixOption isEnabled] && [btnEighteenOption isEnabled]){
        UIButton *im = (UIButton *)[self.view viewWithTag:1006+tag];
        [im setImage:image forState:UIControlStateNormal];
    }
    else if ([btnSixOption isEnabled] && [btnTwelveOption isEnabled]) {
        UIButton *im = (UIButton *)[self.view viewWithTag:1009+tag];
        [im setImage:image forState:UIControlStateNormal];
    }
}
- (void)setSixCards {
    
    for (int i = 0; i < 6; i++) {
        int yOffset = i/3 * 240;
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [button.layer setMasksToBounds:YES];
        [button.layer setCornerRadius:8.0];
        
        //[button setFrame:CGRectMake(74 + (257*(i%3)) , 40 + yOffset + 180, 107, 150)];
        [button setFrame:CGRectMake(52 + (257*(i%3)), 40 + yOffset + 180, 147, 190)];
        [button addTarget:self action:@selector(cardBtnsPressed:) forControlEvents:UIControlEventTouchUpInside];
        [button setTag:1000+i];
        [self.view addSubview:button];
        
        NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
        if ([user boolForKey:@"3cards"] == TRUE){
            [self setImageOfIndex:i andButton:button ];
            [resetBtn setEnabled:YES];
        }
        else {
            [button setImage:[UIImage imageNamed:@"card.png"] forState:UIControlStateNormal];
            [resetBtn setEnabled:NO];
        }
    }
}

- (void)setTwelveCards {
    
    for (int i = 0; i < 12; i++) {
        
        int yOffset = i/4 * 160;
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [button.layer setMasksToBounds:YES];
        [button.layer setCornerRadius:8.0];
        
        [button setFrame:CGRectMake(74 + (170*(i%4)) , 10 + yOffset + 180, 107, 150)];
        [button addTarget:self action:@selector(cardBtnsPressed:) forControlEvents:UIControlEventTouchUpInside];
        [button setTag:1000+i];
        [self.view addSubview:button];
        
        NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
        if ([user boolForKey:@"6cards"] == TRUE){
            [self setImageOfIndex:i andButton:button];
            [resetBtn setEnabled:YES];
        }
        else {
            [button setImage:[UIImage imageNamed:@"card.png"] forState:UIControlStateNormal];
            [resetBtn setEnabled:NO];
        }
    }
}

- (void)setEighteenCards {
    
    for (int i = 0; i < 18; i++) {
        
        int yOffset = i/6 * 160;
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [button.layer setMasksToBounds:YES];
        [button.layer setCornerRadius:8.0];

        [button setFrame:CGRectMake(30 + (120*(i%6)), 10 + yOffset + 180, 107, 150)];
        [button addTarget:self action:@selector(cardBtnsPressed:) forControlEvents:UIControlEventTouchUpInside];
        [button setTag:1000+i];
        [self.view addSubview:button];
        
        NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
        if ([user boolForKey:@"9cards"] == TRUE){
            [self setImageOfIndex:i andButton:button];
            [resetBtn setEnabled:YES];
        }
        else
            [button setImage:[UIImage imageNamed:@"card.png"] forState:UIControlStateNormal];
        [resetBtn setEnabled:NO];
    }
}

- (void)cardBtnsPressed:(id)sender {
    
    [UIView transitionWithView:sender duration:1.0 options:UIViewAnimationOptionTransitionFlipFromRight animations:^{} completion:nil];
}

- (void)setImageOfIndex:(int)i andButton:(UIButton *)btn {
    
    NSString *filePath = [[Singleton sharedSingleton] getDocumentDirectoryPath];
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    
    NSString *one = nil;
    
    if (![btnSixOption isEnabled]) {
        one = [user stringForKey:[NSString stringWithFormat:@"%d_3",i%3]];
    }
    else if (![btnTwelveOption isEnabled]){
        one = [user stringForKey:[NSString stringWithFormat:@"%d_6",i%6]];
    }
    else if (![btnEighteenOption isEnabled]){
        one = [user stringForKey:[NSString stringWithFormat:@"%d_9",i%9]];
    }
    
    NSString *fileName = [filePath stringByAppendingPathComponent:one];
    UIImage *image = [UIImage imageWithContentsOfFile:fileName];
    [btn setImage:image forState:UIControlStateNormal];

}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    int p = 0;
    int selectedOption = 3;
    if (![btnSixOption isEnabled]){
        selectedOption = 3;
    }
    else if (![btnTwelveOption isEnabled]){
        selectedOption = 6;
    }
    else if (![btnEighteenOption isEnabled]){
        selectedOption = 9;
    }
    NSString *filePath = [[Singleton sharedSingleton] getDocumentDirectoryPath];
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];

    for (id view in [self.view subviews]) {
        if ([view tag] >= 1000 && [view tag] < 1000+18) {
            if (isFront){
                [UIView beginAnimations:@"flipCard" context:nil];
                [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:view cache:YES];
                [UIView setAnimationDuration:1.0];
                [UIView commitAnimations];
                UIButton *button = (UIButton *)view;
                [button setImage:[UIImage imageNamed:@"card.png"] forState:UIControlStateNormal];
            }
            else {
                [UIView beginAnimations:@"flipCard" context:nil];
                [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:view cache:YES];
                [UIView setAnimationDuration:1.0];
                [UIView commitAnimations];

                NSString *one3 = [user stringForKey:[NSString stringWithFormat:@"%d_%d",p%selectedOption, selectedOption]];
                NSString *fileName = [filePath stringByAppendingPathComponent:one3];
                UIImage *image = [UIImage imageWithContentsOfFile:fileName];
                
                //UIButton *im = (UIButton *)[self.view viewWithTag:1000+p];
                UIButton *im = (UIButton *)view;
                [im setImage:[UIImage imageNamed:@"card.png"] forState:UIControlStateNormal];

                
                if (selectedOption == 3) {
                    
                    if ([user boolForKey:@"3cards"]) {
                        [im setImage:image forState:UIControlStateNormal];
                    }
                }
                else if (selectedOption == 6) {
                    
                    if ([user boolForKey:@"6cards"]) {
                        [im setImage:image forState:UIControlStateNormal];
                    }
                }
                else if (selectedOption == 9) {
                    
                    if ([user boolForKey:@"9cards"]) {
                        [im setImage:image forState:UIControlStateNormal];
                    }
                }

                p++;
            }

        }
        
    }
    isFront = !isFront;

    [UIView commitAnimations];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)imagesBtnPressed:(id)sender {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveTestNotification:) name:@"TestNotification"object:nil];
    
    UIButton *button1 = (UIButton *)sender;
    if ([btnSixOption isEnabled] && [btnTwelveOption isEnabled]){
        [tableViewController setBtnOption:9];
    }
    else if ([btnTwelveOption isEnabled] && [btnEighteenOption isEnabled]) {
        [tableViewController setBtnOption:3];
    }
    else if ([btnSixOption isEnabled] && [btnEighteenOption isEnabled]) {
        [tableViewController setBtnOption:6];
    }
    [popOver presentPopoverFromRect:button1.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];

}

- (void) shakeButtons:(UIButton *)imageView value: (BOOL) on
{
    if(on==NO)
    {
        [imageView.layer removeAnimationForKey:@"shakeAnimation"];
    }
    else
    {
        CGFloat rotation = 0.05;
        
        CABasicAnimation *shake = [CABasicAnimation animationWithKeyPath:@"transform"];
        shake.duration = 0.13;
        shake.autoreverses = YES;
        shake.repeatCount = 1000;
        shake.removedOnCompletion = NO;
        shake.fromValue = [NSValue valueWithCATransform3D:CATransform3DRotate(self.view.layer.transform,-rotation, 0.0 ,0.0 ,1.0)];
        shake.toValue = [NSValue valueWithCATransform3D:CATransform3DRotate(self.view.layer.transform, rotation, 0.0 ,0.0 ,1.0)];
        
        [imageView.layer addAnimation:shake forKey:@"shakeAnimation"];
    }
}


- (IBAction)resetBtnPressed:(id)sender {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    if (!isFront){

        int p = 0;
        int selectedOption = 3;
        if (![btnSixOption isEnabled]){
            selectedOption = 3;
        }
        else if (![btnTwelveOption isEnabled]){
            selectedOption = 6;
        }
        else if (![btnEighteenOption isEnabled]){
            selectedOption = 9;
        }
        NSString *filePath = [[Singleton sharedSingleton] getDocumentDirectoryPath];
        for (id view in [self.view subviews]) {
            if ([view tag] >= 1000 && [view tag] < 1000+18) {
                [UIView beginAnimations:@"flipCard" context:nil];
                [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:view cache:YES];
                [UIView setAnimationDuration:1.0];
                [UIView commitAnimations];
                
                NSString *one3 = [defaults stringForKey:[NSString stringWithFormat:@"%d_%d",p%selectedOption, selectedOption]];
                NSString *fileName = [filePath stringByAppendingPathComponent:one3];
                UIImage *image = [UIImage imageWithContentsOfFile:fileName];
                
                //UIButton *im = (UIButton *)[self.view viewWithTag:1000+p];
                UIButton *im = (UIButton *)view;
                [im setImage:[UIImage imageNamed:@"card.png"] forState:UIControlStateNormal];
                
                
                if (selectedOption == 3) {
                    
                    if ([defaults boolForKey:@"3cards"]) {
                        [im setImage:image forState:UIControlStateNormal];
                    }
                }
                else if (selectedOption == 6) {
                    
                    if ([defaults boolForKey:@"6cards"]) {
                        [im setImage:image forState:UIControlStateNormal];
                    }
                }
                else if (selectedOption == 9) {
                    
                    if ([defaults boolForKey:@"9cards"]) {
                        [im setImage:image forState:UIControlStateNormal];
                    }
                }
                
                p++;
                
            }
            
        }
        isFront = YES;
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Are you sure" message:@"Want to Reset" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    [alert show];
    
    for (id view in [self.view subviews]) {
        if ([view tag] >= 1000 && [view tag] < 1000+18) {
            [self shakeButtons:view value:YES];
        }
    }
    
    if (![btnSixOption isEnabled]){
        
        alert.tag = 3;
    }
    else if (![btnTwelveOption isEnabled]){
        
        alert.tag = 6;
    }
    else if (![btnEighteenOption isEnabled]){

        alert.tag = 9;
    }

}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 1) {
        NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
        [self removeAllCards];

        if (alertView.tag == 3) {
            [user setBool:FALSE forKey:@"3cards"];
            
            for (int i=0; i<3;i++) {
                NSString *str = [NSString stringWithFormat:@"%d_3", i];
                [user removeObjectForKey:str];
            }
            
            [self setSixCards];
            [resetBtn setEnabled:NO];
        }
        else if (alertView.tag == 6) {
            [user setBool:FALSE forKey:@"6cards"];
            
            for (int i=0; i<6;i++) {
                NSString *str = [NSString stringWithFormat:@"%d_6", i];
                [user removeObjectForKey:str];
            }
            
            [self setTwelveCards];
            [resetBtn setEnabled:NO];

        }
        else if (alertView.tag == 9) {
            [user setBool:FALSE forKey:@"9cards"];
            
            for (int i=0; i<9;i++) {
                NSString *str = [NSString stringWithFormat:@"%d_9", i];
                [user removeObjectForKey:str];
            }
            
            [self setEighteenCards];
            [resetBtn setEnabled:NO];

        }
    }
    else {
        for (id view in [self.view subviews]) {
            if ([view tag] >= 1000 && [view tag] < 1000+18) {
                [self shakeButtons:view value:NO];
            }
        }
    }
}

- (IBAction)backBtnPressed:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    [[Singleton sharedSingleton] setDidHomeBtnPressed:NO];
}

- (IBAction)homeBtnPressed:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    [[Singleton sharedSingleton] setDidHomeBtnPressed:YES];
}

- (void)removeAllCards {
    for (UIView *view in [self.view subviews]) {
        if ([view tag] >= 1000 && [view tag] < 1000+18) {
            [view removeFromSuperview];
        }
    }
}

- (IBAction)cardOptionBtnsPressed:(id)sender {
    
    int tag = [sender tag];
    
    [btnSixOption setEnabled:tag != 0];
    [btnTwelveOption setEnabled:tag != 1];
    [btnEighteenOption setEnabled:tag != 2];
    
    [UIView animateWithDuration:0.8
                          delay:0
                        options:UIViewAnimationOptionAllowAnimatedContent
                     animations:^{
                         [self removeAllCards];
                     }
                     completion:^(BOOL finished){
                         
                         [UIView animateWithDuration:0.5
                                               delay:0
                                             options:UIViewAnimationOptionBeginFromCurrentState
                                          animations:^{
                                              
                                              isFront = YES;
                                              
                                              switch (tag) {
                                                  case 0:
                                                      [self setSixCards];
                                                      break;
                                                  case 1:
                                                      [self setTwelveCards];
                                                      break;
                                                  case 2:
                                                      [self setEighteenCards];
                                                      break;
                                                      
                                                  default:
                                                      break;
                                              }
                                          }
                                          completion:nil];
                     }];
    
}

- (void)viewDidUnload {
    btnSixOption = nil;
    btnTwelveOption = nil;
    btnEighteenOption = nil;
    resetBtn = nil;
    [super viewDidUnload];
}
@end
