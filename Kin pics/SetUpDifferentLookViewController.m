//
//  DifferentLookViewController.m
//  Kin pics
//
//  Created by Rizwan on 1/21/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import "SetUpDifferentLookViewController.h"
#import "KinPicsDB.h"

@interface SetUpDifferentLookViewController ()

@end

@implementation SetUpDifferentLookViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    mutableArrPics = [[NSMutableArray alloc] initWithCapacity:0];
    didScroll = YES;
	// Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated {
    
    previousSelectedOption = -1;
    previousSelectedIndex = -1;
    
    [super viewDidAppear:animated];
    
    [mutableArrPics removeAllObjects];
    
    [self removePreviousAddedImagesFromScrollView];
    
    arrPics = [[Singleton sharedSingleton] getDataOfAllKinPics];
    
    NSInteger numberOfImages = [arrPics count];
    
    [theScrollView setContentSize:CGSizeMake(theScrollView.frame.size.width*numberOfImages, theScrollView.frame.size.height)];
    
    NSString *filePath = [[Singleton sharedSingleton] getDocumentDirectoryPath];
    
    for (int i = 0; i < numberOfImages; i++) {
        
        KinPics *kinPics = [arrPics objectAtIndex:i];
        
        NSString *fileName = [filePath stringByAppendingPathComponent:[kinPics name]];
        
        int xOffset = i * theScrollView.frame.size.width;
        
        
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(10 + xOffset, 10, theScrollView.frame.size.width-20, theScrollView.frame.size.height-20)];
        [imgView setImage:[UIImage imageWithContentsOfFile:fileName]];
        [imgView setContentMode:UIViewContentModeScaleAspectFit];
        [imgView setTag:1000+i];
        
        if (i==0) {
            [dummyImageView setImage:imgView.image];
        }
        
        [mutableArrPics addObject:imgView.image];
        [theScrollView addSubview:imgView];
        
    }
    if ([mutableArrPics count]) {
        
        myImage = [mutableArrPics[0] copy];
    }
    else {
        myImage = nil;
    }
}

- (void)removePreviousAddedImagesFromScrollView {
    
    for (UIView *view in [theScrollView subviews]) {
        [view removeFromSuperview];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backBtnPressed:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    [[Singleton sharedSingleton] setDidHomeBtnPressed:NO];
}

- (IBAction)homeBtnPressed:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    [[Singleton sharedSingleton] setDidHomeBtnPressed:YES];
}

- (IBAction)previousBtnPressed:(id)sender {
    
    if (didScroll) {
        
        CGRect rect = [theScrollView frame];
        rect.origin = [theScrollView contentOffset];
        rect.origin.x -= theScrollView.frame.size.width;
        if (rect.origin.x >= 0) {
            didScroll = NO;
//            [theScrollView scrollRectToVisible:rect animated:YES];
            [self scrollViewScrollToRect:rect];

        }
    }
}

- (IBAction)forwardBtnPressed:(id)sender {
    
    if (didScroll) {
        
        CGRect rect = [theScrollView frame];
        rect.origin = [theScrollView contentOffset];
        rect.origin.x += theScrollView.frame.size.width;
        if (rect.origin.x < theScrollView.contentSize.width) {
            didScroll = NO;
            //[theScrollView scrollRectToVisible:rect animated:YES];
            [self scrollViewScrollToRect:rect];
        }
    }
}
- (void)scrollViewScrollToRect:(CGRect )rect {
    
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionAllowAnimatedContent
                     animations:^{
                         [theScrollView setAlpha:0.7];
                         //[self removePreviousAddedImagesFromView];
                     }
                     completion:^(BOOL finished){
                         
                         [UIView animateWithDuration:0.5
                                               delay:0
                                             options:UIViewAnimationOptionCurveEaseIn
                                          animations:^{
                                              [theScrollView setAlpha:1.0];
                                              [theScrollView scrollRectToVisible:rect animated:YES];
                                          }
                                          completion:nil];
                     }];
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    didScroll = YES;
    [btnSave setEnabled:YES];
    previousSelectedOption = -1;
    
    int currentImageIndex = theScrollView.contentOffset.x/theScrollView.frame.size.width;
    UIImage *anImage = mutableArrPics[currentImageIndex];
    myImage = [anImage copy];

}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    
    didScroll = YES;
    [btnSave setEnabled:YES];
    previousSelectedOption = -1;
    
    int currentImageIndex = theScrollView.contentOffset.x/theScrollView.frame.size.width;
    
    myImage = [mutableArrPics[currentImageIndex] copy];

}

- (IBAction)lookBtnPressed:(id)sender {
    
    int tag = [sender tag];
    
    if (tag == previousSelectedOption || myImage == nil) {
        return;
    }
    previousSelectedOption = tag;

    UIImage *anImage = [myImage copy];

    
    switch (tag) {
        case 0:
            [self lookFour:anImage];
            break;
        case 1:{
            [self makeImageSepiton:anImage];
            break;
        }
        case 2:{
            [self makeImageNegative:anImage];
            break;
        }
        case 3:{
            [self lookOne:anImage];
            break;
        }
        default:{
            break;
        }
    }
    
}
- (void)lookOne:(UIImage *)anImage {
    
    int currentImageIndex = theScrollView.contentOffset.x/theScrollView.frame.size.width;

//    float weight[5] = {0.2270270270, 0.1945945946, 0.1216216216, 0.0540540541, 0.0162162162};
//    // Blur horizontally
//    UIGraphicsBeginImageContextWithOptions(anImage.size, NO, 1.0);
//    [anImage drawInRect:CGRectMake(0, 0, anImage.size.width, anImage.size.height) blendMode:kCGBlendModePlusLighter alpha:weight[0]];
//    for (int x = 1; x < 5; ++x) {
//        [anImage drawInRect:CGRectMake(x, 0, anImage.size.width, anImage.size.height) blendMode:kCGBlendModePlusDarker alpha:weight[x]];
//        [anImage drawInRect:CGRectMake(-x, 0, anImage.size.width, anImage.size.height) blendMode:kCGBlendModePlusDarker alpha:weight[x]];
//    }
//    UIImage *horizBlurredImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    // Blur vertically
//    UIGraphicsBeginImageContextWithOptions(anImage.size, NO, anImage.scale);
//    [horizBlurredImage drawInRect:CGRectMake(0, 0, anImage.size.width, anImage.size.height) blendMode:kCGBlendModePlusLighter alpha:weight[0]];
//    for (int y = 1; y < 5; ++y) {
//        [horizBlurredImage drawInRect:CGRectMake(0, y, anImage.size.width, anImage.size.height) blendMode:kCGBlendModePlusLighter alpha:weight[y]];
//        [horizBlurredImage drawInRect:CGRectMake(0, -y, anImage.size.width, anImage.size.height) blendMode:kCGBlendModePlusLighter alpha:weight[y]];
//    }
//    UIImage *blurredImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
    [(UIImageView *)[theScrollView viewWithTag:1000+currentImageIndex] setImage:anImage];

}
- (void)lookFour:(UIImage *)anImage {
    
    int currentImageIndex = theScrollView.contentOffset.x/theScrollView.frame.size.width;

    UIImage *bottomImage = anImage;
    UIImage *image       = [UIImage imageNamed:@"Rainbow.png"]; //foreground image
    
    CGSize newSize = bottomImage.size;
    UIGraphicsBeginImageContext( newSize );
    
    // Use existing opacity as is
    [bottomImage drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    
    // Apply supplied opacity if applicable
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height) blendMode:kCGBlendModeNormal alpha:0.8];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    [(UIImageView *)[theScrollView viewWithTag:1000+currentImageIndex] setImage:newImage];
}
- (void)makeImageSepiton:(UIImage *)anImage {
    
    int currentImageIndex = theScrollView.contentOffset.x/theScrollView.frame.size.width;
    
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    
//    CGRect imageRect = CGRectMake(0, 0, anImage.size.width, anImage.size.height);
//	imageRect.origin = CGPointMake(8.0, 8.0);
//	imageRect.size = CGSizeMake(64.0, 64.0);
//	
//	// Note: The images are actually drawn upside down because Quartz image drawing expects
//	// the coordinate system to have the origin in the lower-left corner, but a UIView
//	// puts the origin in the upper-left corner. For the sake of brevity (and because
//	// it likely would go unnoticed for the image used) this is not addressed here.
//	// For the demonstration of PDF drawing however, it is addressed, as it would definately
//	// be noticed, and one method of addressing it is shown there.
//    
//	// Draw the image in the upper left corner (0,0) with size 64x64
//	CGContextDrawImage(context, imageRect, anImage.CGImage);
//	
//	// Tile the same image across the bottom of the view
//	// CGContextDrawTiledImage() will fill the entire clipping area with the image, so to avoid
//	// filling the entire view, we'll clip the view to the rect below. This rect extends
//	// past the region of the view, but since the view's rectangle has already been applied as a clip
//	// to our drawing area, it will be intersected with this rect to form the final clipping area
//	CGContextClipToRect(context, CGRectMake(0.0, 0.0, anImage.size.width, anImage.size.height));
//	
//	// The origin of the image rect works similarly to the phase parameter for SetLineDash and
//	// SetPatternPhase and specifies where in the coordinate system the "first" image is drawn.
//	// The size (previously set to 64x64) specifies the size the image is scaled to before being tiled.
//	imageRect.origin = CGPointMake(32.0, 112.0);
//	CGContextDrawTiledImage(context, imageRect, anImage.CGImage);
//	
//	// Highlight the "first" image from the DrawTiledImage call.
//	CGContextSetRGBFillColor(context, 1.0, 0.0, 0.0, 0.5);
//	CGContextFillRect(context, imageRect);
//	// And stroke the clipped area
//	CGContextSetLineWidth(context, 3.0);
//	CGContextSetRGBStrokeColor(context, 1.0, 0.0, 0.0, 1.0);
//	CGContextStrokeRect(context, CGContextGetClipBoundingBox(context));

    
    
    UIGraphicsBeginImageContext(anImage.size);
    CGContextSetBlendMode(UIGraphicsGetCurrentContext(), kCGBlendModeCopy);
    [anImage drawInRect:CGRectMake(0, 0, anImage.size.width, anImage.size.height)];
    CGContextSetBlendMode(UIGraphicsGetCurrentContext(), kCGBlendModeColorBurn);
    CGContextSetFillColorWithColor(UIGraphicsGetCurrentContext(),[UIColor redColor].CGColor);
    CGContextFillRect(UIGraphicsGetCurrentContext(), CGRectMake(0, 0,   anImage.size.width, anImage.size.height));
    UIImage *newImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    
//    CIImage *beginImage = [CIImage imageWithCGImage:anImage.CGImage];
//    CIContext *context = [CIContext contextWithOptions:nil];
//    
//    CIFilter *filter = [CIFilter filterWithName:@"CISepiaTone" keysAndValues: kCIInputImageKey, beginImage, @"inputIntensity", [NSNumber numberWithFloat:1.0], nil];
//    CIImage *outputImage = [filter outputImage];
//    
//    CGImageRef cgimg = [context createCGImage:outputImage fromRect:[outputImage extent]];
//    UIImage *newImg = [UIImage imageWithCGImage:cgimg];
    
    [(UIImageView *)[theScrollView viewWithTag:1000+currentImageIndex] setImage:newImg];

}

- (IBAction)saveBtnPressed:(id)sender {
    
    
    if (previousSelectedOption == -1) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Please change Look" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
    }
    else {
//        KinPicsDB *kpDb = [[KinPicsDB alloc] init];
//        int imageIndex = theScrollView.contentOffset.x/theScrollView.frame.size.width;
//
//        [kpDb updateKinPicsLookWithImageName:[arrPics[imageIndex] name] andLookNo:previousSelectedIndex];
//        //[kpDb updateKinPicsLookWithId:[[arrPics objectAtIndex:imageIndex] picsId] andLookNo:previousSelectedOption];
    }
    
    [btnSave setEnabled:NO];
}
- (void)makeImageNegative:(UIImage *)image{
    int currentImageIndex = theScrollView.contentOffset.x/theScrollView.frame.size.width;

    UIGraphicsBeginImageContext(image.size);
    CGContextSetBlendMode(UIGraphicsGetCurrentContext(), kCGBlendModeCopy);
    [image drawInRect:CGRectMake(0, 0, image.size.width, image.size.height)];
    CGContextSetBlendMode(UIGraphicsGetCurrentContext(), kCGBlendModeDifference);
    CGContextSetFillColorWithColor(UIGraphicsGetCurrentContext(),[UIColor whiteColor].CGColor);
    CGContextFillRect(UIGraphicsGetCurrentContext(), CGRectMake(0, 0,   image.size.width, image.size.height));
    UIImage *returnImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [(UIImageView *)[theScrollView viewWithTag:1000+currentImageIndex] setImage:returnImage];

   // [_imageViewLarge setImage:returnImage];
}

- (void)viewDidUnload {
    theScrollView = nil;
    btnSave = nil;
    dummyImageView = nil;
    [mutableArrPics removeAllObjects];
    mutableArrPics = nil;
    [super viewDidUnload];
}

@end
