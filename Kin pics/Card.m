//
//  Card.m
//  Kin pics
//
//  Created by Rizwan on 2/14/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import "Card.h"

@implementation Card
@synthesize identifier = _identifier;
@synthesize image = _image;

- (id) initWithIdentifier:(unsigned int)initId image:(UIImage *)initImage
{
    self = [super init];
    if(self)
    {
            _identifier = initId;
            _image = initImage;
    }
    return self;
}

- (id) initWithCard:(Card *)anotherCard
{
    self = [super init];
    if(self)
    {
            _identifier = [anotherCard identifier];
            _image = [anotherCard image];
    }
    return self;
}

@end
