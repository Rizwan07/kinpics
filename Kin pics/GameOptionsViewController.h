//
//  SetupViewController.h
//  Kin pics
//
//  Created by Rizwan on 1/17/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GameOptionsViewController : UIViewController {
    
    IBOutlet UILabel *lblHeading;
}
- (IBAction)backBtnPressed:(id)sender;

- (IBAction)setupBtnPressed:(id)sender;
@end
