//
//  PuzzleGameView.h
//  Kin pics
//
//  Created by Rizwan on 3/8/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Tile.h"
typedef enum {
	NONE			= 0,
	UP				= 1,
	DOWN			= 2,
	LEFT			= 3,
	RIGHT			= 4
} ShuffleMove;

@interface PuzzleGameView : UIView{
    
    CGPoint blankPosition;
    CGFloat tileWidth;
    CGFloat tileHeight;
    Tile *tileImageView;
    CGPoint tileLastPiecePosition;
    IBOutlet Tile *tileLastPiece;
    int currentImageIndex;
    NSArray *arrPics;
    NSMutableArray *tiles;

    BOOL isGameOver;
    BOOL isSlide;
    BOOL isMoved;
}
@property (nonatomic, retain) NSMutableArray *tiles;
@property (nonatomic, retain) UIImage *orgImage;

-(Tile *) getPieceAtPoint:(CGPoint) point;

-(ShuffleMove) validMove:(Tile *) tile;
-(void) movePiece:(Tile *) tile withAnimation:(BOOL) animate;
-(void) movePiece:(Tile *) tile inDirectionX:(NSInteger) dx inDirectionY:(NSInteger) dy withAnimation:(BOOL) animate;
-(void) shuffle;

-(BOOL) puzzleCompleted;

@end
