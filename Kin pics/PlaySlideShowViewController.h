//
//  PlaySlideShowViewController.h
//  Kin pics
//
//  Created by Rizwan on 2/3/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface PlaySlideShowViewController : UIViewController {
    
    IBOutlet UIScrollView *theScrollView;
    IBOutlet UIButton *btnImageSound;
    BOOL didScroll;
    NSArray *arrPics;
    
    
}

- (IBAction)backBtnPressed:(id)sender;
- (IBAction)homeBtnPressed:(id)sender;

- (IBAction)previousBtnPressed:(id)sender;
- (IBAction)forwardBtnPressed:(id)sender;

- (IBAction)soundBtnPressed:(id)sender;
- (IBAction)imageSoundBtnPressed:(id)sender;

@end
