//
//  PuzzleGameView.m
//  Kin pics
//
//  Created by Rizwan on 3/8/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import "PuzzleGameView.h"
#define TILE_SPACING    1
#define SHUFFLE_NUMBER  200

@implementation PuzzleGameView
@synthesize tiles = _tiles;
@synthesize orgImage = _orgImage;
int i = 0;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)initPuzzle:(int)newSlices {
    
    
    //int currentImageIndex = theScrollView.contentOffset.x/theScrollView.frame.size.width;
    
    //_orgImage = (UIImage *)[(UIImageView *)[viewCarousel currentItemView] image];
    //_orgImage = (UIImage *)[(UIImageView *)[theScrollView viewWithTag:1000+currentImageIndex] image];
    
	if( _orgImage == nil ){
        
		return;
	}
    self.tiles = [[NSMutableArray alloc]init];

   	[self.tiles removeAllObjects];
    int NUM_HORIZONTAL_PIECES = [self numHorizontalPieces:currentImageIndex];
    int NUM_VERTICAL_PIECES = [self numVerticalPieces:currentImageIndex];
    
	tileWidth = _orgImage.size.width /NUM_HORIZONTAL_PIECES;
	tileHeight = _orgImage.size.height/NUM_VERTICAL_PIECES;
    
    if (NUM_HORIZONTAL_PIECES == 1 && NUM_VERTICAL_PIECES == 1) {
        
        [tileLastPiece setContentMode:UIViewContentModeScaleAspectFit];
        //        [self initPuzzle];
        return;
    }
    else {
        [tileLastPiece setContentMode:UIViewContentModeCenter];
    }
	
    blankPosition = CGPointMake( NUM_HORIZONTAL_PIECES - 1, NUM_VERTICAL_PIECES - 1);
    
	for( int x=0; x<NUM_HORIZONTAL_PIECES; x++ ){
		for( int y=0; y<NUM_VERTICAL_PIECES; y++ ){
			CGPoint orgPosition = CGPointMake(x,y);
			
			if( blankPosition.x == orgPosition.x && blankPosition.y == orgPosition.y ){
                
                KinPics *kin = [arrPics objectAtIndex:currentImageIndex];
                
                [tileLastPiece setTransform:CGAffineTransformIdentity];
                
                if ([kin slices] == 0) {
                    CGAffineTransform transform = CGAffineTransformTranslate(CGAffineTransformMakeScale(0.4, 0.4), 90, 80);
                    [tileLastPiece setTransform:transform];
                }
                else if ([kin slices] == 1) {
                    CGAffineTransform transform = CGAffineTransformTranslate(CGAffineTransformMakeScale(0.75, 0.75), 20, 18);
                    [tileLastPiece setTransform:transform];
                    
                }
                else if ([kin slices] == 2 || [kin slices] == 3) {
                    CGAffineTransform transform = CGAffineTransformTranslate(CGAffineTransformMakeScale(0.8, 0.8), 15, 15);
                    [tileLastPiece setTransform:transform];
                }
                
                CGRect frame = CGRectMake(tileWidth*x  , tileHeight*y ,tileWidth, tileHeight );
                CGImageRef tileImageRef = CGImageCreateWithImageInRect( _orgImage.CGImage, frame );
                UIImage *tileImage = [UIImage imageWithCGImage:tileImageRef];
                
                int x = frame.origin.x + (TILE_SPACING*(NUM_HORIZONTAL_PIECES-1)) + (tileWidth/2) + 250;
                int y = frame.origin.y + (TILE_SPACING*(NUM_VERTICAL_PIECES-1)) + (tileHeight/2) + 420;
                
                tileLastPiecePosition = CGPointMake(x, y);
                
                [tileLastPiece setImage:tileImage];
                CGImageRelease( tileImageRef );
				continue;
			}
			
			CGRect frame = CGRectMake(tileWidth*x  , tileHeight*y ,tileWidth, tileHeight );
			CGImageRef tileImageRef = CGImageCreateWithImageInRect( _orgImage.CGImage, frame );
			UIImage *tileImage = [UIImage imageWithCGImage:tileImageRef];
			
			CGRect tileFrame =  CGRectMake(( tileWidth+TILE_SPACING)*x, (tileHeight+TILE_SPACING)*y + 100, tileWidth, tileHeight );
			
			tileImageView = [[Tile alloc] initWithImage:tileImage];
			tileImageView.frame = tileFrame;
			tileImageView.originalPosition = orgPosition;
			tileImageView.currentPosition = orgPosition;
            
			CGImageRelease( tileImageRef );
			
			[_tiles addObject:tileImageView];
			
            [self addSubview:tileImageView];
		}
	}
    //    [self shuffle];
}
//- (void)initPuzzle {
//
//
//    //int currentImageIndex = theScrollView.contentOffset.x/theScrollView.frame.size.width;
//
//    //_orgImage = (UIImage *)[(UIImageView *)[theScrollView viewWithTag:1000+currentImageIndex] image];
//
//    _orgImage = (UIImage *)[(UIImageView *)[viewCarousel currentItemView] image];
//
//
//	if( _orgImage == nil ){
//
//		return;
//	}
//   	[self.tiles removeAllObjects];
//    int NUM_HORIZONTAL_PIECES = 1;
//    int NUM_VERTICAL_PIECES = 1;
//
//    isGameOver = YES;
//
//	tileWidth = _orgImage.size.width /NUM_HORIZONTAL_PIECES;
//	tileHeight = _orgImage.size.height/NUM_VERTICAL_PIECES;
//
//    blankPosition = CGPointMake( NUM_HORIZONTAL_PIECES -1, NUM_VERTICAL_PIECES -1);
//
//	for( int x=0; x<NUM_HORIZONTAL_PIECES; x++ ){
//		for( int y=0; y<NUM_VERTICAL_PIECES; y++ ){
//			CGPoint orgPosition = CGPointMake(x,y);
//
//			if( blankPosition.x == orgPosition.x && blankPosition.y == orgPosition.y ){
//
//                CGRect frame = CGRectMake(tileWidth*x  , tileHeight*y ,tileWidth, tileHeight );
//                CGImageRef tileImageRef = CGImageCreateWithImageInRect( _orgImage.CGImage, frame );
//                UIImage *tileImage = [UIImage imageWithCGImage:tileImageRef];
//
//                int x = frame.origin.x + (TILE_SPACING*(NUM_HORIZONTAL_PIECES)) + (tileWidth/2) + 250;
//                int y = frame.origin.y + (TILE_SPACING*(NUM_VERTICAL_PIECES)) + (tileHeight/2) + 420;
//
//                tileLastPiecePosition = CGPointMake(x, y);
//
//                [tileLastPiece setImage:tileImage];
//                [tileLastPiece setTransform:CGAffineTransformIdentity];
//                CGImageRelease( tileImageRef );
//				//continue;
//			}
//            int HORIZONTAL_SPACE = (768 - _orgImage.size.width)/2;
//
//			CGRect frame = CGRectMake(tileWidth*x  , tileHeight*y ,tileWidth, tileHeight );
//			CGImageRef tileImageRef = CGImageCreateWithImageInRect( _orgImage.CGImage, frame );
//			UIImage *tileImage = [UIImage imageWithCGImage:tileImageRef];
//
//			CGRect tileFrame =  CGRectMake(( tileWidth+TILE_SPACING)*x + HORIZONTAL_SPACE, (tileHeight+TILE_SPACING)*y + 420, tileWidth, tileHeight );
//
//			tileImageView = [[Tile alloc] initWithImage:tileImage];
//			tileImageView.frame = tileFrame;
//			tileImageView.originalPosition = orgPosition;
//			tileImageView.currentPosition = orgPosition;
//
//			CGImageRelease( tileImageRef );
//
//			[_tiles addObject:tileImageView];
//
//            [self.view addSubview:tileImageView];
//		}
//	}
//}



-(ShuffleMove) validMove:(Tile *) tile{
    
	if( tile.currentPosition.x == blankPosition.x && tile.currentPosition.y == blankPosition.y+1 ){
		return UP;
	}
	
	if( tile.currentPosition.x == blankPosition.x && tile.currentPosition.y == blankPosition.y-1 ){
		return DOWN;
	}
	
	if( tile.currentPosition.x == blankPosition.x+1 && tile.currentPosition.y == blankPosition.y ){
		return LEFT;
	}
	
	if( tile.currentPosition.x == blankPosition.x-1 && tile.currentPosition.y == blankPosition.y ){
		return RIGHT;
	}
    return NONE;
}

-(void) movePiece:(Tile *) tile withAnimation:(BOOL) animate{
	switch ( [self validMove:tile] ) {
		case UP:
			[self movePiece:tile
			   inDirectionX:0 inDirectionY:-1 withAnimation:animate];
			break;
		case DOWN:
			[self movePiece:tile
			   inDirectionX:0 inDirectionY:1 withAnimation:animate];
			break;
		case LEFT:
			[self movePiece:tile
			   inDirectionX:-1 inDirectionY:0 withAnimation:animate];
			break;
		case RIGHT:
			[self movePiece:tile
			   inDirectionX:1 inDirectionY:0 withAnimation:animate];
			break;
		default:
            
			break;
	}
}

-(void) movePiece:(Tile *) tile inDirectionX:(NSInteger) dx inDirectionY:(NSInteger) dy withAnimation:(BOOL) animate{
    
	tile.currentPosition = CGPointMake( tile.currentPosition.x+dx, tile.currentPosition.y+dy );
	blankPosition = CGPointMake( blankPosition.x-dx, blankPosition.y-dy );
	int HORIZONTAL_SPACE = (768 - _orgImage.size.width)/2;
	int x = tile.currentPosition.x;
	int y = tile.currentPosition.y;
	
	if( animate ){
		[UIView beginAnimations:@"frame" context:nil];
        // [UIView beginAnimations:@"Dragging A DraggableView" context:nil];
	}
	tile.frame = CGRectMake((tileWidth+TILE_SPACING)*x + HORIZONTAL_SPACE, (tileHeight+TILE_SPACING)*y + 420,
                            tileWidth, tileHeight );
	if( animate ){
		[UIView commitAnimations];
	}
}

-(void) shuffle{
    isSlide = YES;
	NSMutableArray *validMoves = [[NSMutableArray alloc] init];
	
	srandom(time(NULL));
	
	for( int i=0; i<SHUFFLE_NUMBER; i++ ){
		[validMoves removeAllObjects];
		
		for( Tile *t in _tiles ){
			if( [self validMove:t] != NONE ){
				[validMoves addObject:t];
			}
		}
        NSInteger pick = random()%[validMoves count];
        NSLog(@"shuffleRandom using pick: %d from array of size %d", pick, [validMoves count]);
        [self movePiece:(Tile *)[validMoves objectAtIndex:pick] withAnimation:NO];
	}
}


#pragma mark helper methods
-(Tile *) getPieceAtPoint:(CGPoint) point{
	CGRect touchRect = CGRectMake(point.x, point.y, 1.0, 1.0);
	
	for( Tile *t in _tiles ){
		if( CGRectIntersectsRect(t.frame, touchRect) ){
			return t;
		}
	}
	return nil;
}
-(Tile *) getPieceAtPointMoved:(CGPoint) point{
	CGRect touchRect = CGRectMake(point.x, point.y, 1.0, 1.0);
	
	for( Tile *t in _tiles ){
		if( CGRectIntersectsRect(t.frame, touchRect) ){
			return t;
		}
	}
	return nil;
}


-(BOOL) puzzleCompleted{
	for( Tile *t in _tiles ){
		if( t.originalPosition.x != t.currentPosition.x || t.originalPosition.y != t.currentPosition.y ){
			return NO;
		}
	}
	
	return YES;
}
-(void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    return;
    if ( i == 1){
        
        return;
    }
    //    if (!isSlide){
    //        return;
    //    }
    if (isGameOver) {
        return;
    }
    NSLog(@"touched moved");
    isMoved = YES;
    UITouch *touch = [touches anyObject];
    CGPoint currentTouch = [touch locationInView:self];
    Tile *t = [self getPieceAtPointMoved:currentTouch];
    if (t == nil ){
        return;
    }
    [self movePiece:t withAnimation:YES];
    CGAffineTransform lastTransform = [tileLastPiece transform];
    
    if( [self puzzleCompleted] ){
        
        [UIView transitionWithView:tileLastPiece duration:0.5 options:UIViewAnimationOptionCurveLinear animations:^ {
            
            [tileLastPiece setCenter:tileLastPiecePosition];
            [tileLastPiece setTransform:CGAffineTransformIdentity];
            [tileLastPiece setAlpha:0.8];
            
        } completion:^(BOOL finish) {
            
            isGameOver = YES;
            
            [tileLastPiece setAlpha:1.0];
            //            [self onePuzzlePic];
            CGAffineTransform transform = CGAffineTransformMakeScale(1.2, 1.2);
            
            Tile *aTile = [_tiles objectAtIndex:0];
            
            [UIView transitionWithView:aTile duration:1.0 options:UIViewAnimationOptionTransitionNone animations:^ {
                
                [aTile setTransform:transform];
                [tileLastPiece setTransform:lastTransform];
                
            } completion:^(BOOL finish) {
                Tile *aTile = [_tiles objectAtIndex:0];
                [aTile setTransform:CGAffineTransformIdentity];
                
                [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(showMessage) userInfo:nil repeats:NO];
                
            }];
            
        }];
    }
    // isSlide = NO;
    i = 1;
}



- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    return;
    i = 0;
    
    if (isMoved){
        isMoved = NO;
        return;
    }
    NSLog(@"touches Ended");
    
    if (isGameOver) {
        return;
    }
    
	UITouch *touch = [touches anyObject];
    CGPoint currentTouch = [touch locationInView:self];
	
    if (_orgImage == nil) {
        UIAlertView *aView = [[UIAlertView alloc]initWithTitle:nil message:@"Select the image please" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [aView show];
        
        return;
    }
    
    Tile *t = [self getPieceAtPoint:currentTouch];
    if (t == nil){
        //        self.audioPlayer.currentTime = 0;
        //        [self.audioPlayer play];
        
        return;
    }
    
    [self movePiece:t withAnimation:YES];
    
    CGAffineTransform lastTransform = [tileLastPiece transform];
    
    if( [self puzzleCompleted] ){
        
        [UIView transitionWithView:tileLastPiece duration:0.5 options:UIViewAnimationOptionCurveLinear animations:^ {
            
            [tileLastPiece setCenter:tileLastPiecePosition];
            [tileLastPiece setTransform:CGAffineTransformIdentity];
            [tileLastPiece setAlpha:0.8];
            
        } completion:^(BOOL finish) {
            
            isGameOver = YES;
            
            [tileLastPiece setAlpha:1.0];
            //            [self onePuzzlePic];
            CGAffineTransform transform = CGAffineTransformMakeScale(1.2, 1.2);
            
            Tile *aTile = [_tiles objectAtIndex:0];
            
            [UIView transitionWithView:aTile duration:1.0 options:UIViewAnimationOptionTransitionNone animations:^ {
                
                [aTile setTransform:transform];
                [tileLastPiece setTransform:lastTransform];
                
            } completion:^(BOOL finish) {
                Tile *aTile = [_tiles objectAtIndex:0];
                [aTile setTransform:CGAffineTransformIdentity];
                
                [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(showMessage) userInfo:nil repeats:NO];
                
            }];
            
        }];
    }
	isMoved = NO;
}

- (void)showMessage {
    
    //NSString *Winning = [NSString stringWithFormat:@"It took you: %i Moves in %i seconds!", countMoves, theTime];
    
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Good Job" message:@"Well Done!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
    [message show];
    
    //[lblWon setText:@"You Won!"];
    
}
//-(void)onePuzzlePic {
//
//    [self removePreviousAddedImagesFromView];
//    //int currentImageIndex = theScrollView.contentOffset.x/theScrollView.frame.size.width;
//
//    //_orgImage = (UIImage *)[(UIImageView *)[theScrollView viewWithTag:1000+currentImageIndex] image];
//
//    _orgImage = (UIImage *)[(UIImageView *)[viewCarousel currentItemView] image];
//
//
//	if( _orgImage == nil ){
//
//		return;
//	}
//
//   	[self.tiles removeAllObjects];
//    int NUM_HORIZONTAL_PIECES = 1;
//    int NUM_VERTICAL_PIECES = 1;
//    int horizontalSpace = (self.view.frame.size.width - _orgImage.size.width)/2;
//    isGameOver = YES;
//	tileWidth = _orgImage.size.width;
//	tileHeight = _orgImage.size.height;
//    blankPosition = CGPointMake( NUM_HORIZONTAL_PIECES -1, NUM_VERTICAL_PIECES -1);
//	for( int x=0; x<NUM_HORIZONTAL_PIECES; x++ ){
//		for( int y=0; y<NUM_VERTICAL_PIECES; y++ ){
//
//			CGPoint orgPosition = CGPointMake(x,y);
//
//			CGRect frame = CGRectMake(tileWidth*x  , tileHeight*y ,tileWidth, tileHeight );
//			CGImageRef tileImageRef = CGImageCreateWithImageInRect( _orgImage.CGImage, frame );
//			UIImage *tileImage = [UIImage imageWithCGImage:tileImageRef];
//
//			CGRect tileFrame =  CGRectMake(( tileWidth+TILE_SPACING)*x+ horizontalSpace , (tileHeight+TILE_SPACING)*y + 420,
//                                           tileWidth, tileHeight );
//
//			tileImageView = [[Tile alloc] initWithImage:tileImage];
//			tileImageView.frame = tileFrame;
//			tileImageView.originalPosition = orgPosition;
//			tileImageView.currentPosition = orgPosition;
//
//			CGImageRelease( tileImageRef );
//			
//			[_tiles addObject:tileImageView];
//			
//            [self.view addSubview:tileImageView];
//		}
//	}
//}

- (int)numHorizontalPieces:(int)cur {
    
    KinPics *kin = [arrPics objectAtIndex:cur];
    
    if (kin.slices > 0 && kin.slices <= 3) {
        return 2;
    }
    else {
        return 1;
    }
}

- (int)numVerticalPieces:(int)cur {
    
    KinPics *kin = [arrPics objectAtIndex:cur];
    //    int numVerticalPieces;
    //        if (kin.slices == 0){
    //            numVerticalPieces = 2;
    //        }
    //        else if (kin.slices == 1){
    //            numVerticalPieces = 2;
    //        }
    //        else if (kin.slices == 2){
    //            numVerticalPieces = 3;
    //        }
    //        else if (kin.slices == 3){
    //            numVerticalPieces = 4;
    //        }
    //        else
    //            numVerticalPieces = 1;
    //
    //    return numVerticalPieces;
    
    if (kin.slices == 0 || kin.slices == 1) {
        return 2;
    }
    else if (kin.slices == 2) {
        return 3;
    }
    else if (kin.slices == 3) {
        return 4;
    }
    else {
        return 1;
    }
    
}
- (void)removePreviousAddedImagesFromView {
    
    if (tileImageView != nil) {
        for (Tile *tile in _tiles) {
            [tile removeFromSuperview];
        }
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
