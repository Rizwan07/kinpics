//
//  Singleton.h
//  Kin pics
//
//  Created by Rizwan on 2/3/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Singleton : NSObject {
    
    NSArray *arrKinPics;
    NSString *docDirectoryPath;
}

@property (nonatomic, assign) BOOL didEnterInSetUpOptions;
@property (nonatomic, assign) BOOL didHomeBtnPressed;
@property (nonatomic, assign) int selectedOption;

+ (Singleton *)sharedSingleton;

- (NSArray *)getDataOfAllKinPics;
- (void)removeAllKinPicsData;

- (NSString *)getDocumentDirectoryPath;

@end
