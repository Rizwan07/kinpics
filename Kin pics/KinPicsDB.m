//
//  KinPicsDB.m
//  Kin pics
//
//  Created by Rizwan on 1/26/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import "KinPicsDB.h"
#import "KinPics.h"

@implementation KinPicsDB

-(id)init{
    
    
    dbname = @"KinPicsDB.sqlite";
    
    NSArray *docspaths=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *docsdir=[docspaths objectAtIndex:0];
    dbPath = [docsdir stringByAppendingPathComponent:dbname];
    NSLog(@"Directries %@", dbPath);
    
    NSFileManager *filemgr=[NSFileManager defaultManager];
    
    if(![filemgr fileExistsAtPath:dbPath])
    {
        NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath ] stringByAppendingPathComponent:dbname];
        [filemgr copyItemAtPath:databasePathFromApp toPath:dbPath error:nil];
    }

    
    return self;
}
//-(BOOL)CopyDatabaseIfNeeded{
//    
//    NSFileManager *filemgr=[NSFileManager defaultManager];
//    BOOL success=[filemgr fileExistsAtPath:dbPath];
//    if(success)
//    {
//        NSLog(@"Success!!!");
//        return YES;
//    }
//    else
//    {
//        NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath ] stringByAppendingPathComponent:dbname];
//        [filemgr copyItemAtPath:databasePathFromApp toPath:dbPath error:nil];
//        NSLog(@"Fail!!!");
//        return  NO;
//    }
//    
//}
-(void)addKinPicsWithName:(NSString *)name voice:(NSData *)voice text:(NSString *)text heartVoice:(NSData *)heartVoice{
    
    sqlite3 *database;
    
    if(sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK) {
        
        const char* sqlStatement1 = "INSERT INTO KinPics (PicName, Text, Slices) VALUES (?, ?, ?)";
        const char* sqlStatement2 = "INSERT INTO Voices (PicName, TextVoice, HeartVoice) VALUES (?, ?, ?)";
        sqlite3_stmt* compiledStatement;
        
        if( sqlite3_prepare_v2(database, sqlStatement1, -1, &compiledStatement, NULL) == SQLITE_OK )
        {
            sqlite3_bind_text(compiledStatement,    1, [name UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(compiledStatement,    2, [text UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_int(compiledStatement,     3, 7);
            
            if (sqlite3_step(compiledStatement) == SQLITE_DONE) {
                NSLog(@"successfuly insert1");
            }
            else {
                NSLog(@"not inserted");
            }

        }
        else {
            NSAssert1(0, @"Error while saving. '%s'", sqlite3_errmsg(database));
            NSLog( @"SaveBody: Failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(database) );
        }
        
        if( sqlite3_prepare_v2(database, sqlStatement2, -1, &compiledStatement, NULL) == SQLITE_OK )
        {
            sqlite3_bind_text(compiledStatement,    1, [name UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_blob(compiledStatement,    2, [voice bytes], [voice length], SQLITE_TRANSIENT);
            sqlite3_bind_blob(compiledStatement,    3, [heartVoice bytes], [heartVoice length], SQLITE_TRANSIENT);
            
            if (sqlite3_step(compiledStatement) == SQLITE_DONE) {
                NSLog(@"successfuly insert2");
            }
            else {
                NSLog(@"not inserted");
            }
            
        }
        else {
            NSAssert1(0, @"Error while saving. '%s'", sqlite3_errmsg(database));
            NSLog( @"SaveBody: Failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(database) );
        }
        
        // Finalize and close database.
        sqlite3_finalize(compiledStatement);

    }
    sqlite3_close(database);
    
}

-(void)deleteKinPicsImageWithName:(NSString *)picsName {
    sqlite3 *database;
    
    if(sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK)
    {
        NSString *query;
        
        query = [NSString stringWithFormat:@"DELETE FROM KinPics WHERE PicName = \"%@\"",picsName];
        const char *sqlStatement = [query cStringUsingEncoding:NSUTF8StringEncoding];
        sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(compiledStatement) == SQLITE_DONE)
            {
                NSLog(@"successfuly delete");
            }
            else
            {
                NSLog(@"Not deleted");
            }
        }
        sqlite3_finalize(compiledStatement);
    }
    sqlite3_close(database);
}

-(void)updateKinPicsVoice:(NSData *)voice heartVoice:(NSData *)heartVoice text:(NSString *)text wherePicName:(NSString *)picName {
    
    sqlite3 *database;
    
    if(sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK)
    {
//        NSString *Query;
//        
//        Query = [NSString stringWithFormat:@"Update KinPics set voice=\"%@\", text=\"%@\" where Id=%d", voice, text, picId];
//        const char *sqlStatement = [Query cStringUsingEncoding:NSUTF8StringEncoding];
//        sqlite3_stmt *compiledStatement;
//        @try {
//            if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
//            {
//                if (sqlite3_step(compiledStatement) == SQLITE_DONE)
//                {
//                    
//                    NSLog(@"Updated!");
//                }
//            }
//        }
//        @catch (NSException *exception) {
//            NSLog(@"error occured %@", [exception description]);
//        }
//
//
//        sqlite3_finalize(compiledStatement);

        const char *sqlStatement1 = "UPDATE KinPics SET Text = ? Where PicName = ?";
        const char *sqlStatement2 = "UPDATE Voices SET TextVoice = ?, HeartVoice = ? Where PicName = ?";
        sqlite3_stmt *compiledStatement;
        
        if(sqlite3_prepare_v2(database, sqlStatement1, -1, &compiledStatement, NULL) == SQLITE_OK ) {
            
            //NSAssert1(0, @"Error while creating update statement. '%s'", sqlite3_errmsg(database));
            
            sqlite3_bind_text(compiledStatement, 1, [text UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(compiledStatement, 2, [picName UTF8String], -1, SQLITE_TRANSIENT);
            //sqlite3_bind_int(compiledStatement,  3, picId);
            
            if (sqlite3_step(compiledStatement) == SQLITE_DONE) {
                NSLog(@"successfuly updated1");
            }
            else {
                NSLog(@"not updated");
                NSAssert1(0, @"Error while updating. '%s'", sqlite3_errmsg(database));
            }
        }
        else {
            NSAssert1(0, @"Error while updating. '%s'", sqlite3_errmsg(database));
            NSLog( @"SaveBody: Failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(database) );
        }
        if(sqlite3_prepare_v2(database, sqlStatement2, -1, &compiledStatement, NULL) == SQLITE_OK ) {
            
            //NSAssert1(0, @"Error while creating update statement. '%s'", sqlite3_errmsg(database));
            
            sqlite3_bind_blob(compiledStatement, 1, [voice bytes], [voice length], SQLITE_TRANSIENT);
            sqlite3_bind_blob(compiledStatement, 2, [heartVoice bytes], [heartVoice length], SQLITE_TRANSIENT);
            sqlite3_bind_text(compiledStatement, 3, [picName UTF8String], -1, SQLITE_TRANSIENT);
            //sqlite3_bind_int(compiledStatement,  3, picId);
            
            if (sqlite3_step(compiledStatement) == SQLITE_DONE) {
                NSLog(@"successfuly updated2");
            }
            else {
                NSLog(@"not updated");
                NSAssert1(0, @"Error while updating. '%s'", sqlite3_errmsg(database));
            }
        }
        else {
            NSAssert1(0, @"Error while updating. '%s'", sqlite3_errmsg(database));
            NSLog( @"SaveBody: Failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(database) );
        }
        sqlite3_reset(compiledStatement);

    }
    sqlite3_close(database);
}

- (NSArray *)getAllKinPics {
    
    NSMutableArray *itemsArr = [[NSMutableArray alloc]init];
    
    sqlite3 *database;
    
    if(sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK)
    {
        NSString *query;
        
        query = @"SELECT * FROM KinPics";
        const char *sqlStatement = [query cStringUsingEncoding:NSUTF8StringEncoding];
        sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(compiledStatement) == SQLITE_ROW)
            {
                KinPics *kp =[[KinPics alloc]init];
                
                @try {
                    kp.name     = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 0)];
                    kp.text     = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 1)];
                    kp.slices   = sqlite3_column_int(compiledStatement, 2);
                }
                @catch (NSException *exception) {
                    NSLog(@"error occured %@", [exception description]);
                }
                [itemsArr addObject:kp];
            }
        }
        sqlite3_finalize(compiledStatement);
    }
    sqlite3_close(database);
    
    return itemsArr;
}

- (KinPicsVoice *)getVoicesOfKinPic:(NSString *)name {
    
    KinPicsVoice *kpv = nil;
    sqlite3 *database;
    
    if(sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK)
    {
//        NSString *query = [NSString stringWithFormat:@"SELECT * FROM Voices WHERE PicName= %@",name];
//        const char *sqlStatement = [query cStringUsingEncoding:NSUTF8StringEncoding];
//        sqlite3_stmt *compiledStatement;
//        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
//        {
//            if (sqlite3_step(compiledStatement) == SQLITE_DONE)
//            {
//                NSLog(@"successfuly delete");
//            }
//            else
//            {
//                NSLog(@"Not deleted");
//            }
//        }
//        sqlite3_finalize(compiledStatement);
        
        NSString *query = [NSString stringWithFormat:@"SELECT * FROM Voices WHERE PicName = \"%@\"",name];
        const char *sqlStatement = [query cStringUsingEncoding:NSUTF8StringEncoding];
        sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
            
//            if (sqlite3_step(compiledStatement) == SQLITE_DONE) {
            
            while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                
                kpv =[[KinPicsVoice alloc]init];
                @try {
                    kpv.name     = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 0)];
                    int tBytes  = sqlite3_column_bytes(compiledStatement, 1);
                    kpv.textVoice     = [NSData dataWithBytes:sqlite3_column_blob(compiledStatement, 1) length:tBytes];
                    int hBytes  = sqlite3_column_bytes(compiledStatement, 2);
                    kpv.heartVoice   = [NSData dataWithBytes:sqlite3_column_blob(compiledStatement, 2) length:hBytes];
                }
                @catch (NSException *exception) {
                    NSLog(@"error occured %@", [exception description]);
                }
                
            }
        }
        sqlite3_finalize(compiledStatement);

    }
    sqlite3_close(database);

    return kpv;
}

- (NSArray *)getDataOfSlices {
    NSMutableArray *sliceArray = [[NSMutableArray alloc]init];
    
    sqlite3 *database;
    
    if(sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK)
    {
        NSString *query;
        
        query = @"SELECT * FROM KinPics where Slices != 7";
        const char *sqlStatement = [query cStringUsingEncoding:NSUTF8StringEncoding];
        sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(compiledStatement) == SQLITE_ROW)
            {
                KinPics *kp =[[KinPics alloc]init];
                
                @try {
//                    kp.picsId   = sqlite3_column_int(compiledStatement, 0);
                    kp.name     = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 0)];
//                    int tBytes  = sqlite3_column_bytes(compiledStatement, 2);
//                    kp.voice    = [NSData dataWithBytes:sqlite3_column_blob(compiledStatement, 2) length:tBytes];
                    
                    kp.text     = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 1)];
                    kp.slices   = sqlite3_column_int(compiledStatement, 2);
//                    kp.look     = sqlite3_column_int(compiledStatement, 5);
//                    int hBytes  = sqlite3_column_bytes(compiledStatement, 6);
//                    kp.heartVoice    = [NSData dataWithBytes:sqlite3_column_blob(compiledStatement, 6) length:hBytes];
                }
                @catch (NSException *exception) {
                    NSLog(@"error occured %@", [exception description]);
                }
                
                [sliceArray addObject:kp];
            }
        }
        sqlite3_finalize(compiledStatement);
    }
    sqlite3_close(database);
    
    return sliceArray;
}

- (void)updateKinPicsImageSlicesWithName:(NSString *)picName andNumberOfSlices:(NSInteger)slices {
    
    sqlite3 *database;
    
    if(sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK)
    {
        NSString *Query;
        
        Query = [NSString stringWithFormat:@"UPDATE KinPics SET slices=%d where PicName=\"%@\"", slices, picName];
        const char *sqlStatement = [Query cStringUsingEncoding:NSUTF8StringEncoding];
        sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(compiledStatement) == SQLITE_DONE)
            {
                
                NSLog(@"Updated!");
            }
        }
        sqlite3_finalize(compiledStatement);
    }
    sqlite3_close(database);

}

@end
