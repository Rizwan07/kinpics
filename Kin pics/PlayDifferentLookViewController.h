//
//  PlayDifferentLookViewController.h
//  Kin pics
//
//  Created by Rizwan on 2/3/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlayDifferentLookViewController : UIViewController {
    
    IBOutlet UIScrollView *theScrollView;
    BOOL didScroll;
    UIImage *myImage;
    NSMutableArray *mutableArrPics;

    int previousSelectedOption;
}

- (IBAction)backBtnPressed:(id)sender;
- (IBAction)homeBtnPressed:(id)sender;

- (IBAction)forwardBtnPressed:(id)sender;
- (IBAction)previousBtnPressed:(id)sender;

- (IBAction)differentLookBtnPressed:(id)sender;
@end
