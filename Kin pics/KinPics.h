//
//  KinPics.h
//  Kin pics
//
//  Created by Rizwan on 1/26/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KinPics : NSObject

@property (nonatomic, strong) NSString  *name;
@property (nonatomic, strong) NSString  *text;
@property (nonatomic, assign) NSInteger slices;

@end
