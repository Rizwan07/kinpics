//
//  PlayPuzzleViewController.h
//  Kin pics
//
//  Created by Rizwan on 2/3/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"
#import <AVFoundation/AVFoundation.h>
#import "PuzzleImageView.h"

@interface PlayPuzzleViewController : UIViewController<puzzleImageViewDelegate> {
    
    IBOutlet UIScrollView *theScrollView;
    BOOL didScroll;
    
    IBOutlet UIView *gameView;
    NSMutableArray *arrPositionNum;
    NSMutableArray *arrPoints;
    NSMutableArray *arrScrambling;
    NSMutableArray *arrScramblePoints;
    
    IBOutlet UILabel *lblGreat;
    NSArray *arrPics;
    IBOutlet UILabel *lblWon;
    
    IBOutlet iCarousel *viewCarousel;
  
    UIAlertView *alert;

    int currentImageIndex;

    IBOutlet UIImageView *mainImageIV, *previewIV, *bgIV;
    UIImage *puzzleImage1 , *puzzleImage2 , *puzzleImage3 , *puzzleImage4 , *puzzleImage5 , *puzzleImage6 , *puzzleImage7 , *puzzleImage8 , *puzzleImage9;
    
    PuzzleImageView *puzzleImageV1 , *puzzleImageV2 , *puzzleImageV3 , *puzzleImageV4 , *puzzleImageV5 , *puzzleImageV6 , *puzzleImageV7 , *puzzleImageV8 , *puzzleImageV9;
    
    PuzzleImageView *puzzleObj;

    IBOutlet UIButton *playBtn;
    IBOutlet UIImageView *eightSliceImage;
    IBOutlet UIImageView *fourSliceImage;
    IBOutlet UIImageView *sixSliceImage;
    IBOutlet UIImageView *twoSliceImage;
}

@property (nonatomic, weak) id <puzzleImageViewDelegate> puzzleDelegate;
@property BOOL value1, value2;

- (IBAction)backBtnPressed:(id)sender;
- (IBAction)homeBtnPressed:(id)sender;

- (IBAction)forwardBtnPressed:(id)sender;
- (IBAction)previousBtnPressed:(id)sender;

- (IBAction) maskingTestAction;
- (IBAction)scrambleBtnPressed:(id)sender;

@end
