//
//  PuzzleImageView.h
//  Kin pics
//
//  Created by Rizwan on 2/16/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@class PuzzleImageView;

@protocol puzzleImageViewDelegate <NSObject>

@required

- (void) passCallWhenImageViewIsTapped: (PuzzleImageView *) imageView;

- (void) movePuzzlePieceToBG: (PuzzleImageView *) imageView;

@end

@interface PuzzleImageView : UIImageView

- (void) shakeImage:(UIImageView *) imageView value: (BOOL) on;

@property (nonatomic, retain) id <puzzleImageViewDelegate> pDelegate;

@property (nonatomic , assign) CGRect originRect;

@end
