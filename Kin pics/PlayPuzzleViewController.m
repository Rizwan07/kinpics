//
//  PlayPuzzleViewController.m
//  Kin pics
//
//  Created by Rizwan on 2/3/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import "PlayPuzzleViewController.h"
#import <QuartzCore/QuartzCore.h>
#import <CoreImage/CoreImage.h>
#import "JKCustomAlert.h"
#import "KinPicsDB.h"
@interface PlayPuzzleViewController ()

@property (strong, nonatomic) AVAudioPlayer *audioPlayer;

@end

@implementation PlayPuzzleViewController

@synthesize audioPlayer = _audioPlayer;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    didScroll = YES;
    arrPositionNum = [NSMutableArray array];
    NSString *str = [[NSBundle mainBundle]pathForResource:@"beep4" ofType:@"mp3"];
    NSURL *fileURL = [NSURL fileURLWithPath:str];
    self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:nil];
    //self.audioPlayer.delegate = self;
    [_audioPlayer prepareToPlay];
    [lblGreat setHidden:YES];

   
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    KinPicsDB *kin_db = [[KinPicsDB alloc]init];

    viewCarousel.type = iCarouselTypeCoverFlow;
//    arrPics = [[Singleton sharedSingleton] getDataOfAllKinPics];
    arrPics = [kin_db getDataOfSlices];
    [viewCarousel reloadData];

    [playBtn setHidden:YES];
    
    //[self setOtherData];
    
//    [self removePreviousAddedImagesFromScrollView];
//    
//    arrPics = [[Singleton sharedSingleton] getDataOfAllKinPics];
//    
//    NSInteger numberOfImages = [arrPics count];
//    
//    [theScrollView setContentSize:CGSizeMake(theScrollView.frame.size.width*numberOfImages, theScrollView.frame.size.height)];
//    //[theScrollView setContentOffset:CGPointMake(theScrollView.frame.size.width * _selectedImageIndex, 0)];
//    
//    NSString *filePath = [[Singleton sharedSingleton] getDocumentDirectoryPath];
//    
//    for (int i = 0; i < numberOfImages; i++) {
//        
//        KinPics *kinPics = [arrPics objectAtIndex:i];
//        
//        NSString *fileName = [filePath stringByAppendingPathComponent:[kinPics name]];
//        
//        int xOffset = i * theScrollView.bounds.size.width;
//        
//        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(10 + xOffset, 10, theScrollView.frame.size.width - 20, theScrollView.frame.size.height-20)];
//        [imgView setImage:[UIImage imageWithContentsOfFile:fileName]];
//        [imgView setContentMode:UIViewContentModeScaleAspectFit];
//        
//        [imgView setTag:1000+i];
//
//        [theScrollView addSubview:imgView];
//        
//    }
    //[self setOtherData];
//    [lblWon setFont:[UIFont fontWithName:@"Chalkboard SE" size:26]];
}

- (void)removePreviousAddedImagesFromScrollView {
    
    for (UIView *view in [theScrollView subviews]) {
        [view removeFromSuperview];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backBtnPressed:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    [[Singleton sharedSingleton] setDidHomeBtnPressed:NO];
}

- (IBAction)homeBtnPressed:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    [[Singleton sharedSingleton] setDidHomeBtnPressed:YES];
}

- (IBAction)previousBtnPressed:(id)sender {
    
    if (didScroll) {
        
        CGRect rect = [theScrollView frame];
        rect.origin = [theScrollView contentOffset];
        rect.origin.x -= theScrollView.frame.size.width;
        if (rect.origin.x >= 0) {
            didScroll = NO;
            [theScrollView scrollRectToVisible:rect animated:YES];
        }
    }
}

- (IBAction)forwardBtnPressed:(id)sender {
    
    if (didScroll) {
        
        CGRect rect = [theScrollView frame];
        rect.origin = [theScrollView contentOffset];
        rect.origin.x += theScrollView.frame.size.width;
        if (rect.origin.x < theScrollView.contentSize.width) {
            didScroll = NO;
            [theScrollView scrollRectToVisible:rect animated:YES];
        }
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    didScroll = YES;
    [self setOtherData];
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    
    didScroll = YES;
    [self setOtherData];
}
- (void)setOtherData {
    
    //isGameOver = NO;
    
    //int currentImageIndex = theScrollView.contentOffset.x/theScrollView.frame.size.width;
    
    //if (currentImageIndex < [arrPics count]) {
        
    //    _orgImage = (UIImage *)[(UIImageView *)[theScrollView viewWithTag:1000+currentImageIndex] image];
    //}
    
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionAllowAnimatedContent
                     animations:^{
    //                     [self removePreviousAddedImagesFromView];
                         [lblWon setText:@"Join the pieces to stitch the image as the given image"];
                     }
                     completion:^(BOOL finished){
                         
                         [UIView animateWithDuration:0.5
                                               delay:0
                                             options:UIViewAnimationOptionBeginFromCurrentState
                                          animations:^{
                                              //[self initPuzzle:currentImageIndex];
                                          }
                                          completion:nil];
                     }];

    
}

#pragma mark -
#pragma mark iCarousel methods

- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    return [arrPics count];
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view
{
    
    NSString *filePath = [[Singleton sharedSingleton] getDocumentDirectoryPath];
    
    
    KinPics *kinPics = [arrPics objectAtIndex:index];
    
    NSString *fileName = [filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"small%@",[kinPics name]]];
    
    //UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 200, 200)];
    view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 250.0f, 250.0f)];
    [view setBackgroundColor:[UIColor lightGrayColor]];
    [(UIImageView*)view setImage:[UIImage imageWithContentsOfFile:fileName]];
    [(UIImageView*)view setContentMode:UIViewContentModeScaleAspectFit];
    
    //[view addSubview:imgView];
    
    
    //create a numbered view
//    view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 200.0f, 200.0f)];
//    view.backgroundColor = [UIColor lightGrayColor];
//    UILabel *label = [[UILabel alloc] initWithFrame:view.bounds];
//    label.text = [NSString stringWithFormat:@"%i", index];
//    label.backgroundColor = [UIColor clearColor];
//    label.textAlignment = UITextAlignmentCenter;
//    label.font = [label.font fontWithSize:50];
//    [view addSubview:label];
//    [(UIImageView *)view setImage:[UIImage imageNamed:@"card.png"]];
    return view;
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    switch (option)
    {
        case iCarouselOptionWrap:
        {
            return YES;
        }
        default:
        {
            return value;
        }
    }
}

- (void)carouselWillBeginDragging:(iCarousel *)carousel
{
	NSLog(@"Carousel will begin dragging");
}

- (void)carouselDidEndDragging:(iCarousel *)carousel willDecelerate:(BOOL)decelerate
{
	NSLog(@"Carousel did end dragging and %@ decelerate", decelerate? @"will": @"won't");
}

- (void)carouselWillBeginDecelerating:(iCarousel *)carousel
{
	NSLog(@"Carousel will begin decelerating");
}

- (void)carouselDidEndDecelerating:(iCarousel *)carousel
{
	NSLog(@"Carousel did end decelerating");
}

- (void)carouselWillBeginScrollingAnimation:(iCarousel *)carousel
{
	NSLog(@"Carousel will begin scrolling");
}

- (void)carouselDidEndScrollingAnimation:(iCarousel *)carousel
{
	NSLog(@"Carousel did end scrolling");
    NSLog(@"%d", carousel.currentItemIndex);
    
    currentImageIndex = carousel.currentItemIndex;
    if ([arrPics count]) {
        
        [self preparePuzzleImages];
    }
    //[self setOtherData];
  
}

#pragma jigsaw game lifecycle

- (void) movePuzzlePieceToBG: (PuzzleImageView *) imageView
{
    NSLog(@"moved");
    _audioPlayer.currentTime = 0;
    [_audioPlayer play];
    [self puzzleComplete];
}

- (void)puzzleComplete{
    KinPics *kinpic = [arrPics objectAtIndex:currentImageIndex];

    if (kinpic.slices == 0)
    {
        if (puzzleImageV1.frame.origin.x == (puzzleImageV1.originRect.origin.x+20) &&
            puzzleImageV1.frame.origin.y == (puzzleImageV1.originRect.origin.y+20) &&
            
            puzzleImageV2.frame.origin.x == (puzzleImageV2.originRect.origin.x+20) &&
            puzzleImageV2.frame.origin.y == (puzzleImageV2.originRect.origin.y+20))
        {
            [lblGreat setHidden:NO];
            [playBtn setHidden:NO];
            [self.view insertSubview:lblGreat aboveSubview:puzzleObj];
            [lblWon setHidden:YES];
        }
        
    }
    else if (kinpic.slices == 1)
    {
        if (puzzleImageV1.frame.origin.x == (puzzleImageV1.originRect.origin.x+20) &&
            puzzleImageV1.frame.origin.y == (puzzleImageV1.originRect.origin.y+20) &&
            
            puzzleImageV2.frame.origin.x == (puzzleImageV2.originRect.origin.x+20) &&
            puzzleImageV2.frame.origin.y == (puzzleImageV2.originRect.origin.y+20) &&
            puzzleImageV3.frame.origin.x == (puzzleImageV3.originRect.origin.x+20) &&
            puzzleImageV3.frame.origin.y == (puzzleImageV3.originRect.origin.y+20) &&
            puzzleImageV4.frame.origin.x == (puzzleImageV4.originRect.origin.x+20) &&
            puzzleImageV4.frame.origin.y == (puzzleImageV4.originRect.origin.y+20))
        {
            [lblGreat setHidden:NO];
            [playBtn setHidden:NO];
            [self.view insertSubview:lblGreat aboveSubview:puzzleObj];
            [lblWon setHidden:YES];

        }

    }
    else if (kinpic.slices == 2)
    {
        if (puzzleImageV1.frame.origin.x == (puzzleImageV1.originRect.origin.x+20) &&
            puzzleImageV1.frame.origin.y == (puzzleImageV1.originRect.origin.y+20) &&
            puzzleImageV2.frame.origin.x == (puzzleImageV2.originRect.origin.x+20) &&
            puzzleImageV2.frame.origin.y == (puzzleImageV2.originRect.origin.y+20) &&
            puzzleImageV3.frame.origin.x == (puzzleImageV3.originRect.origin.x+20) &&
            puzzleImageV3.frame.origin.y == (puzzleImageV3.originRect.origin.y+20) &&
            puzzleImageV4.frame.origin.x == (puzzleImageV4.originRect.origin.x+20) &&
            puzzleImageV4.frame.origin.y == (puzzleImageV4.originRect.origin.y+20) &&
            puzzleImageV5.frame.origin.x == (puzzleImageV5.originRect.origin.x+20) &&
            puzzleImageV5.frame.origin.y == (puzzleImageV5.originRect.origin.y+20) &&
            puzzleImageV6.frame.origin.x == (puzzleImageV6.originRect.origin.x+20) &&
            puzzleImageV6.frame.origin.y == (puzzleImageV6.originRect.origin.y+20))
        {
            [lblGreat setHidden:NO];
            [playBtn setHidden:NO];
            [self.view insertSubview:lblGreat aboveSubview:puzzleObj];
            [lblWon setHidden:YES];

        }

    }
    else if (kinpic.slices == 3)
    {
         if (puzzleImageV1.frame.origin.x == (puzzleImageV1.originRect.origin.x+20) &&
            puzzleImageV1.frame.origin.y == (puzzleImageV1.originRect.origin.y+20) &&
            puzzleImageV2.frame.origin.x == (puzzleImageV2.originRect.origin.x+20) &&
            puzzleImageV2.frame.origin.y == (puzzleImageV2.originRect.origin.y+20) &&
            puzzleImageV3.frame.origin.x == (puzzleImageV3.originRect.origin.x+20) &&
            puzzleImageV3.frame.origin.y == (puzzleImageV3.originRect.origin.y+20) &&
            puzzleImageV4.frame.origin.x == (puzzleImageV4.originRect.origin.x+20) &&
            puzzleImageV4.frame.origin.y == (puzzleImageV4.originRect.origin.y+20) &&
            puzzleImageV5.frame.origin.x == (puzzleImageV5.originRect.origin.x+20) &&
            puzzleImageV5.frame.origin.y == (puzzleImageV5.originRect.origin.y+20) &&
            puzzleImageV6.frame.origin.x == (puzzleImageV6.originRect.origin.x+20) &&
            puzzleImageV6.frame.origin.y == (puzzleImageV6.originRect.origin.y+20) &&
            puzzleImageV7.frame.origin.x == (puzzleImageV7.originRect.origin.x+20) &&
            puzzleImageV7.frame.origin.y == (puzzleImageV7.originRect.origin.y+20) &&
            puzzleImageV8.frame.origin.x == (puzzleImageV8.originRect.origin.x+20) &&
            puzzleImageV8.frame.origin.y == (puzzleImageV8.originRect.origin.y+20))
        {
         //   [self showAlert];
            [lblGreat setHidden:NO];
            [playBtn setHidden:NO];
            [self.view insertSubview:lblGreat aboveSubview:puzzleObj];
            [lblWon setHidden:YES];

        }
    }
}
- (void)showAlert {
    
    UIImage *backgroundImage = [UIImage imageNamed:@"Splatter.png"];
    alert = [[JKCustomAlert alloc] initWithImage:backgroundImage text:NSLocalizedString(@"Game Complete", nil)];
    [alert show];
    [lblWon setText:@"You Won!"];
    [self performSelector:@selector(hideAlert) withObject:nil afterDelay:3.0];

}
- (void) hideAlert {
	[alert dismissWithClickedButtonIndex:0 animated:YES];
//    [playBtn setHidden:NO];
}

- (void) passCallWhenImageViewIsTapped: (PuzzleImageView *) imageView
{
    [gameView bringSubviewToFront:imageView];
}

- (void) preparePuzzleImages
{
//    arrPics = [[Singleton sharedSingleton] getDataOfAllKinPics];
    KinPics *kinpic = [arrPics objectAtIndex:currentImageIndex];
     NSString *filePath = [[Singleton sharedSingleton] getDocumentDirectoryPath];
    NSString * fileName = [filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"small%@",[kinpic name]]];
    UIImage *image = [UIImage imageWithContentsOfFile:fileName];
    [mainImageIV setImage:image];
    
    for (PuzzleImageView *obj in gameView.subviews)
    {
        if ([obj isKindOfClass:[PuzzleImageView class]])
        {
            [obj removeFromSuperview];
        }
        
    }
    if (kinpic.slices == 0){
        [self setTwoSlices];
        
    }
    else if (kinpic.slices == 1){
        [self setFourSlices];
        
    }
    else if (kinpic.slices == 2) {
        [self setSixSlices];
        
    }
    else if (kinpic.slices == 3){
        [self setEightSlices];
    }
    mainImageIV.hidden = YES;
    
}
- (void)setTwoSlices
{
    [eightSliceImage setHidden:YES];
    [sixSliceImage setHidden:YES];
    [fourSliceImage setHidden:YES];
    [twoSliceImage setHidden:NO];
    [lblGreat setHidden:YES];
    [playBtn setHidden:YES];
//    [lblWon setHidden:NO];
    //[arrPositionNum removeAllObjects];
    arrPositionNum = [NSMutableArray arrayWithObjects:@"0", @"1", nil];
    CGPoint point1 = CGPointMake(380, 430);
    CGPoint point2 = CGPointMake(226, 80);
    
    NSString *str1 = NSStringFromCGPoint(point1);
    NSString *str2 = NSStringFromCGPoint(point2);
    arrPoints = [NSMutableArray arrayWithObjects:str1,str2, nil];
    [lblWon setText:@"Join the pieces to stitch the image as the given image"];

    for (int i = 1; i <= 2; i ++)
    {
        NSString *maskImageName = [NSString stringWithFormat:@"Two_%i.png",i];
        UIImage *maskImage = [UIImage imageNamed:maskImageName];

        if (i == 1)
        {
            CGRect myImageArea = CGRectMake (0,183, maskImage.size.width, maskImage.size.height);

            CGImageRef mySubimage = CGImageCreateWithImageInRect (mainImageIV.image.CGImage, myImageArea);


            CGImageRef maskRef = maskImage.CGImage;

            CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
                                                CGImageGetHeight(maskRef),
                                                CGImageGetBitsPerComponent(maskRef),
                                                CGImageGetBitsPerPixel(maskRef),
                                                CGImageGetBytesPerRow(maskRef),
                                                CGImageGetDataProvider(maskRef), NULL, false);

            CGImageRef masked = CGImageCreateWithMask(mySubimage, mask);

            puzzleImage1 = [UIImage imageWithCGImage:masked];

            puzzleImageV1 = [[PuzzleImageView alloc] initWithFrame:CGRectMake(174, 215, maskImage.size.width, maskImage.size.height)];
            puzzleImageV1.originRect = CGRectMake(174-20, 215-20, maskImage.size.width+40, maskImage.size.height+40);
            puzzleImageV1.image = puzzleImage1;
            puzzleImageV1.tag = 1;
            puzzleImageV1.pDelegate = self;
            [gameView addSubview:puzzleImageV1];
            
            CGImageRelease(mySubimage);
            CGImageRelease(mask);
            CGImageRelease(masked);
            
        }
        else if (i == 2)
        {
            CGRect myImageArea = CGRectMake (0,0, maskImage.size.width, maskImage.size.height);

            CGImageRef mySubimage = CGImageCreateWithImageInRect (mainImageIV.image.CGImage, myImageArea);

            CGImageRef maskRef = maskImage.CGImage;

            CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
                                                CGImageGetHeight(maskRef),
                                                CGImageGetBitsPerComponent(maskRef),
                                                CGImageGetBitsPerPixel(maskRef),
                                                CGImageGetBytesPerRow(maskRef),
                                                CGImageGetDataProvider(maskRef), NULL, false);

            CGImageRef masked = CGImageCreateWithMask(mySubimage, mask);

            puzzleImage2 = [UIImage imageWithCGImage:masked];

            puzzleImageV2 = [[PuzzleImageView alloc] initWithFrame:CGRectMake(174, 31, maskImage.size.width, maskImage.size.height)];
            puzzleImageV2.originRect = CGRectMake(174-20, 31-20, maskImage.size.width+40, maskImage.size.height+40);
            puzzleImageV2.image = puzzleImage2;
            puzzleImageV2.tag = 2;
            puzzleImageV2.pDelegate = self;
            [gameView addSubview:puzzleImageV2];
            
            CGImageRelease(mySubimage);
            CGImageRelease(mask);
            CGImageRelease(masked);

        }
    }
    puzzleObj = [[PuzzleImageView alloc] init];
    
    [puzzleObj shakeImage:puzzleImageV1 value:YES];
    [puzzleObj shakeImage:puzzleImageV2 value:YES];

    [self performSelector:@selector(randomMovement:) withObject:puzzleImageV1 afterDelay:0.5f];
    [self performSelector:@selector(randomMovement:) withObject:puzzleImageV2 afterDelay:0.5f];

}

- (void)setFourSlices
{
    [eightSliceImage setHidden:YES];
    [sixSliceImage setHidden:YES];
    [fourSliceImage setHidden:NO];
    [twoSliceImage setHidden:YES];

    [lblGreat setHidden:YES];
    [playBtn setHidden:YES];
//    [lblWon setHidden:NO];

    [arrPositionNum removeAllObjects];
    arrPositionNum = [NSMutableArray arrayWithObjects:@"0", @"1", @"2", @"3", nil];
    CGPoint point1 = CGPointMake(110, 90);
    CGPoint point2 = CGPointMake(630, 90);
    CGPoint point3 = CGPointMake(110, 390);
    CGPoint point4 = CGPointMake(630, 400);
    NSString *str1 = NSStringFromCGPoint(point1);
    NSString *str2 = NSStringFromCGPoint(point2);
    NSString *str3 = NSStringFromCGPoint(point3);
    NSString *str4 = NSStringFromCGPoint(point4);
    arrPoints = [NSMutableArray arrayWithObjects:str1,str2,str3,str4, nil];
    [lblWon setText:@"Join the pieces to stitch the image as the given image"];

    for (int i = 1; i <= 4; i ++) {

        NSString *maskImageName = [NSString stringWithFormat:@"Four_%i.png",i];
        UIImage *maskImage = [UIImage imageNamed:maskImageName];

        if (i == 1)
        {
            CGRect myImageArea = CGRectMake (0, 0, maskImage.size.width, maskImage.size.height);

            CGImageRef mySubimage = CGImageCreateWithImageInRect (mainImageIV.image.CGImage, myImageArea);


            CGImageRef maskRef = maskImage.CGImage;

            CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
                                                CGImageGetHeight(maskRef),
                                                CGImageGetBitsPerComponent(maskRef),
                                                CGImageGetBitsPerPixel(maskRef),
                                                CGImageGetBytesPerRow(maskRef),
                                                CGImageGetDataProvider(maskRef), NULL, false);

            CGImageRef masked = CGImageCreateWithMask(mySubimage, mask);

            puzzleImage1 = [UIImage imageWithCGImage:masked];

            puzzleImageV1 = [[PuzzleImageView alloc] initWithFrame:CGRectMake(170, 29, maskImage.size.width, maskImage.size.height)];
            puzzleImageV1.originRect = CGRectMake(170-20, 29-20, maskImage.size.width+40, maskImage.size.height+40);
            puzzleImageV1.image = puzzleImage1;
            puzzleImageV1.tag = 3;
            puzzleImageV1.pDelegate = self;
            [gameView addSubview:puzzleImageV1];
            
            CGImageRelease(mySubimage);
            CGImageRelease(mask);
            CGImageRelease(masked);

        }
        else if (i == 2)
        {
            CGRect myImageArea = CGRectMake (204, 0, maskImage.size.width, maskImage.size.height);

            CGImageRef mySubimage = CGImageCreateWithImageInRect (mainImageIV.image.CGImage, myImageArea);

            CGImageRef maskRef = maskImage.CGImage;

            CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
                                                CGImageGetHeight(maskRef),
                                                CGImageGetBitsPerComponent(maskRef),
                                                CGImageGetBitsPerPixel(maskRef),
                                                CGImageGetBytesPerRow(maskRef),
                                                CGImageGetDataProvider(maskRef), NULL, false);

            CGImageRef masked = CGImageCreateWithMask(mySubimage, mask);

            puzzleImage2 = [UIImage imageWithCGImage:masked];

            puzzleImageV2 = [[PuzzleImageView alloc] initWithFrame:CGRectMake(372, 29, maskImage.size.width, maskImage.size.height)];
            puzzleImageV2.originRect = CGRectMake(372-20, 29-20, maskImage.size.width+40, maskImage.size.height+40);
            puzzleImageV2.image = puzzleImage2;
            puzzleImageV2.tag = 4;
            puzzleImageV2.pDelegate = self;
            [gameView addSubview:puzzleImageV2];
            
            CGImageRelease(mySubimage);
            CGImageRelease(mask);
            CGImageRelease(masked);

        }
        else if (i == 3)
        {
            CGRect myImageArea = CGRectMake (0, 201, maskImage.size.width, maskImage.size.height);
            CGImageRef mySubimage = CGImageCreateWithImageInRect (mainImageIV.image.CGImage, myImageArea);

            CGImageRef maskRef = maskImage.CGImage;

            CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
                                                CGImageGetHeight(maskRef),
                                                CGImageGetBitsPerComponent(maskRef),
                                                CGImageGetBitsPerPixel(maskRef),
                                                CGImageGetBytesPerRow(maskRef),
                                                CGImageGetDataProvider(maskRef), NULL, false);

            CGImageRef masked = CGImageCreateWithMask(mySubimage, mask);

            puzzleImage3 = [UIImage imageWithCGImage:masked];

            puzzleImageV3 = [[PuzzleImageView alloc] initWithFrame:CGRectMake(170, 229, maskImage.size.width, maskImage.size.height)];
            puzzleImageV3.originRect = CGRectMake(170-20, 229-20, maskImage.size.width+40, maskImage.size.height+40);
            puzzleImageV3.image = puzzleImage3;
            puzzleImageV3.tag = 5;
            puzzleImageV3.pDelegate = self;
            [gameView addSubview:puzzleImageV3];
            
            CGImageRelease(mySubimage);
            CGImageRelease(mask);
            CGImageRelease(masked);

        }
        else if (i == 4)
        {
            CGRect myImageArea = CGRectMake (151, 201, maskImage.size.width, maskImage.size.height);

            CGImageRef mySubimage = CGImageCreateWithImageInRect (mainImageIV.image.CGImage, myImageArea);

            CGImageRef maskRef = maskImage.CGImage;

            CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
                                                CGImageGetHeight(maskRef),
                                                CGImageGetBitsPerComponent(maskRef),
                                                CGImageGetBitsPerPixel(maskRef),
                                                CGImageGetBytesPerRow(maskRef),
                                                CGImageGetDataProvider(maskRef), NULL, false);
            
            CGImageRef masked = CGImageCreateWithMask(mySubimage, mask);
            
            puzzleImage4 = [UIImage imageWithCGImage:masked];
            
            puzzleImageV4 = [[PuzzleImageView alloc] initWithFrame:CGRectMake (320, 229, maskImage.size.width, maskImage.size.height)];
            puzzleImageV4.originRect = CGRectMake(320-20, 229-20, maskImage.size.width+40, maskImage.size.height+40);
            puzzleImageV4.image = puzzleImage4;
            puzzleImageV4.tag = 6;
            puzzleImageV4.pDelegate = self;
            [gameView addSubview:puzzleImageV4];
            
            CGImageRelease(mySubimage);
            CGImageRelease(mask);
            CGImageRelease(masked);

        }

    }
    puzzleObj = [[PuzzleImageView alloc] init];

    [puzzleObj shakeImage:puzzleImageV1 value:YES];
    [puzzleObj shakeImage:puzzleImageV2 value:YES];
    [puzzleObj shakeImage:puzzleImageV3 value:YES];
    [puzzleObj shakeImage:puzzleImageV4 value:YES];
    
    [self performSelector:@selector(randomMovement:) withObject:puzzleImageV1 afterDelay:0.5f];
    [self performSelector:@selector(randomMovement:) withObject:puzzleImageV2 afterDelay:0.5f];
    [self performSelector:@selector(randomMovement:) withObject:puzzleImageV3 afterDelay:0.5f];
    [self performSelector:@selector(randomMovement:) withObject:puzzleImageV4 afterDelay:0.5f];

}

- (void)setSixSlices
{
    [eightSliceImage setHidden:YES];
    [sixSliceImage setHidden:NO];
    [fourSliceImage setHidden:YES];
    [twoSliceImage setHidden:YES];

    [lblGreat setHidden:YES];
//    [lblWon setHidden:NO];

    [playBtn setHidden:YES];
    //[arrPositionNum removeAllObjects];
    arrPositionNum = [NSMutableArray arrayWithObjects:@"0", @"1", @"2", @"3", @"4", @"5", nil];
    CGPoint point1 = CGPointMake(350, 60);
    CGPoint point2 = CGPointMake(100, 98);
    CGPoint point3 = CGPointMake(90, 364);
    CGPoint point4 = CGPointMake(350, 480);
    CGPoint point5 = CGPointMake(610, 353);
    CGPoint point6 = CGPointMake(595, 113);
    NSString *str1 = NSStringFromCGPoint(point1);
    NSString *str2 = NSStringFromCGPoint(point2);
    NSString *str3 = NSStringFromCGPoint(point3);
    NSString *str4 = NSStringFromCGPoint(point4);
    NSString *str5 = NSStringFromCGPoint(point5);
    NSString *str6 = NSStringFromCGPoint(point6);

    arrPoints = [NSMutableArray arrayWithObjects:str1,str2,str3,str4,str5,str6, nil];
    [lblWon setText:@"Join the pieces to stitch the image as the given image"];

    for (int i = 1; i <= 6; i ++) {

        NSString *maskImageName = [NSString stringWithFormat:@"Six_%i.png",i];
        UIImage *maskImage = [UIImage imageNamed:maskImageName];

        if (i == 1)
        {
            CGRect myImageArea = CGRectMake (0, 0, maskImage.size.width, maskImage.size.height);

            CGImageRef mySubimage = CGImageCreateWithImageInRect (mainImageIV.image.CGImage, myImageArea);


            CGImageRef maskRef = maskImage.CGImage;

            CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
                                                CGImageGetHeight(maskRef),
                                                CGImageGetBitsPerComponent(maskRef),
                                                CGImageGetBitsPerPixel(maskRef),
                                                CGImageGetBytesPerRow(maskRef),
                                                CGImageGetDataProvider(maskRef), NULL, false);

            CGImageRef masked = CGImageCreateWithMask(mySubimage, mask);

            puzzleImage1 = [UIImage imageWithCGImage:masked];

            puzzleImageV1 = [[PuzzleImageView alloc] initWithFrame:CGRectMake(170, 26, maskImage.size.width, maskImage.size.height)];
            puzzleImageV1.originRect = CGRectMake(170-20, 26-20, maskImage.size.width+40, maskImage.size.height+40);
            puzzleImageV1.image = puzzleImage1;
            puzzleImageV1.tag = 7;
            puzzleImageV1.pDelegate = self;
            [gameView addSubview:puzzleImageV1];
            
            CGImageRelease(mySubimage);
            CGImageRelease(mask);
            CGImageRelease(masked);

        }
        else if (i == 2)
        {

            CGRect myImageArea = CGRectMake (203, 0, maskImage.size.width, maskImage.size.height);

            CGImageRef mySubimage = CGImageCreateWithImageInRect (mainImageIV.image.CGImage, myImageArea);

            CGImageRef maskRef = maskImage.CGImage;

            CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
                                                CGImageGetHeight(maskRef),
                                                CGImageGetBitsPerComponent(maskRef),
                                                CGImageGetBitsPerPixel(maskRef),
                                                CGImageGetBytesPerRow(maskRef),
                                                CGImageGetDataProvider(maskRef), NULL, false);

            CGImageRef masked = CGImageCreateWithMask(mySubimage, mask);

            puzzleImage2 = [UIImage imageWithCGImage:masked];

            puzzleImageV2 = [[PuzzleImageView alloc] initWithFrame:CGRectMake(373, 26, maskImage.size.width, maskImage.size.height)];
            puzzleImageV2.originRect = CGRectMake(373-20, 26-20, maskImage.size.width+40, maskImage.size.height+40);
            puzzleImageV2.image = puzzleImage2;
            puzzleImageV2.tag = 8;
            puzzleImageV2.pDelegate = self;
            [gameView addSubview:puzzleImageV2];
            
            CGImageRelease(mySubimage);
            CGImageRelease(mask);
            CGImageRelease(masked);

        }
        else if (i == 3)
        {
            CGRect myImageArea = CGRectMake (0, 95, maskImage.size.width, maskImage.size.height);
            CGImageRef mySubimage = CGImageCreateWithImageInRect (mainImageIV.image.CGImage, myImageArea);

            CGImageRef maskRef = maskImage.CGImage;

            CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
                                                CGImageGetHeight(maskRef),
                                                CGImageGetBitsPerComponent(maskRef),
                                                CGImageGetBitsPerPixel(maskRef),
                                                CGImageGetBytesPerRow(maskRef),
                                                CGImageGetDataProvider(maskRef), NULL, false);

            CGImageRef masked = CGImageCreateWithMask(mySubimage, mask);

            puzzleImage3 = [UIImage imageWithCGImage:masked];

            puzzleImageV3 = [[PuzzleImageView alloc] initWithFrame:CGRectMake(170, 121, maskImage.size.width, maskImage.size.height)];
            puzzleImageV3.originRect = CGRectMake(170-20,121-20, maskImage.size.width+40, maskImage.size.height+40);
            puzzleImageV3.image = puzzleImage3;
            puzzleImageV3.tag = 9;
            puzzleImageV3.pDelegate = self;
            [gameView addSubview:puzzleImageV3];
            
            CGImageRelease(mySubimage);
            CGImageRelease(mask);
            CGImageRelease(masked);

        }
        else if (i == 4)
        {
            CGRect myImageArea = CGRectMake (163, 135, maskImage.size.width, maskImage.size.height);

            CGImageRef mySubimage = CGImageCreateWithImageInRect (mainImageIV.image.CGImage, myImageArea);

            CGImageRef maskRef = maskImage.CGImage;

            CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
                                                CGImageGetHeight(maskRef),
                                                CGImageGetBitsPerComponent(maskRef),
                                                CGImageGetBitsPerPixel(maskRef),
                                                CGImageGetBytesPerRow(maskRef),
                                                CGImageGetDataProvider(maskRef), NULL, false);

            CGImageRef masked = CGImageCreateWithMask(mySubimage, mask);

            puzzleImage4 = [UIImage imageWithCGImage:masked];

            puzzleImageV4 = [[PuzzleImageView alloc] initWithFrame:CGRectMake (333, 160, maskImage.size.width, maskImage.size.height)];
            puzzleImageV4.originRect = CGRectMake(333-20, 160-20, maskImage.size.width+40, maskImage.size.height+40);
            puzzleImageV4.image = puzzleImage4;
            puzzleImageV4.tag = 10;
            puzzleImageV4.pDelegate = self;
            [gameView addSubview:puzzleImageV4];
            
            CGImageRelease(mySubimage);
            CGImageRelease(mask);
            CGImageRelease(masked);

        }
        else if (i == 5)
        {
            CGRect myImageArea = CGRectMake (0, 231, maskImage.size.width, maskImage.size.height);

            CGImageRef mySubimage = CGImageCreateWithImageInRect (mainImageIV.image.CGImage, myImageArea);

            CGImageRef maskRef = maskImage.CGImage;

            CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
                                                CGImageGetHeight(maskRef),
                                                CGImageGetBitsPerComponent(maskRef),
                                                CGImageGetBitsPerPixel(maskRef),
                                                CGImageGetBytesPerRow(maskRef),
                                                CGImageGetDataProvider(maskRef), NULL, false);

            CGImageRef masked = CGImageCreateWithMask(mySubimage, mask);

            puzzleImage5 = [UIImage imageWithCGImage:masked];

            puzzleImageV5 = [[PuzzleImageView alloc] initWithFrame:CGRectMake (170, 256, maskImage.size.width, maskImage.size.height)];
            puzzleImageV5.originRect = CGRectMake(170-20, 256-20, maskImage.size.width+40, maskImage.size.height+40);
            puzzleImageV5.tag = 11;
            puzzleImageV5.pDelegate = self;
            puzzleImageV5.image = puzzleImage5;
            [gameView addSubview:puzzleImageV5];
            
            CGImageRelease(mySubimage);
            CGImageRelease(mask);
            CGImageRelease(masked);

        }
        else if (i == 6)
        {
            CGRect myImageArea = CGRectMake (163, 271, maskImage.size.width, maskImage.size.height);

            CGImageRef mySubimage = CGImageCreateWithImageInRect (mainImageIV.image.CGImage, myImageArea);

            CGImageRef maskRef = maskImage.CGImage;

            CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
                                                CGImageGetHeight(maskRef),
                                                CGImageGetBitsPerComponent(maskRef),
                                                CGImageGetBitsPerPixel(maskRef),
                                                CGImageGetBytesPerRow(maskRef),
                                                CGImageGetDataProvider(maskRef), NULL, false);

            CGImageRef masked = CGImageCreateWithMask(mySubimage, mask);
            
            puzzleImage6 = [UIImage imageWithCGImage:masked];
            
            puzzleImageV6 = [[PuzzleImageView alloc] initWithFrame:CGRectMake (333, 296, maskImage.size.width, maskImage.size.height)];
            puzzleImageV6.originRect = CGRectMake(333-20, 296-20, maskImage.size.width+40, maskImage.size.height+40);
            puzzleImageV6.tag = 12;
            puzzleImageV6.pDelegate = self;
            puzzleImageV6.image = puzzleImage6;
            [gameView addSubview:puzzleImageV6];
            
            CGImageRelease(mySubimage);
            CGImageRelease(mask);
            CGImageRelease(masked);

        }
    }
    puzzleObj = [[PuzzleImageView alloc] init];

    [puzzleObj shakeImage:puzzleImageV1 value:YES];
    [puzzleObj shakeImage:puzzleImageV2 value:YES];
    [puzzleObj shakeImage:puzzleImageV3 value:YES];
    [puzzleObj shakeImage:puzzleImageV4 value:YES];
    [puzzleObj shakeImage:puzzleImageV5 value:YES];
    [puzzleObj shakeImage:puzzleImageV6 value:YES];
  
    [self performSelector:@selector(randomMovement:) withObject:puzzleImageV1 afterDelay:0.5f];
    [self performSelector:@selector(randomMovement:) withObject:puzzleImageV2 afterDelay:0.5f];
    [self performSelector:@selector(randomMovement:) withObject:puzzleImageV3 afterDelay:0.5f];
    [self performSelector:@selector(randomMovement:) withObject:puzzleImageV4 afterDelay:0.5f];
    [self performSelector:@selector(randomMovement:) withObject:puzzleImageV5 afterDelay:0.5f];
    [self performSelector:@selector(randomMovement:) withObject:puzzleImageV6 afterDelay:0.5f];

}

- (void)setEightSlices
{
    [eightSliceImage setHidden:NO];
    [sixSliceImage setHidden:YES];
    [fourSliceImage setHidden:YES];
    [twoSliceImage setHidden:YES];

    [lblGreat setHidden:YES];
//    [lblWon setHidden:NO];
    [playBtn setHidden:YES];
    //[arrPositionNum removeAllObjects];
    arrPositionNum = [NSMutableArray arrayWithObjects:@"0", @"1", @"2", @"3", @"4", @"5", @"6", @"7", nil];
    CGPoint point1 = CGPointMake(450, 60);
    CGPoint point2 = CGPointMake(220, 60);
    CGPoint point3 = CGPointMake(100, 160);
    CGPoint point4 = CGPointMake(100, 302);
    CGPoint point5 = CGPointMake(190, 440);
    CGPoint point6 = CGPointMake(550, 435);
    CGPoint point7 = CGPointMake(610, 290);
    CGPoint point8 = CGPointMake(610, 150);
    NSString *str1 = NSStringFromCGPoint(point1);
    NSString *str2 = NSStringFromCGPoint(point2);
    NSString *str3 = NSStringFromCGPoint(point3);
    NSString *str4 = NSStringFromCGPoint(point4);
    NSString *str5 = NSStringFromCGPoint(point5);
    NSString *str6 = NSStringFromCGPoint(point6);
    NSString *str7 = NSStringFromCGPoint(point7);
    NSString *str8 = NSStringFromCGPoint(point8);
    
    arrPoints = [NSMutableArray arrayWithObjects:str1,str2,str3,str4,str5,str6,str7,str8, nil];
    [lblWon setText:@"Join the pieces to stitch the image as the given image"];
    for (int i = 1; i <= 8; i ++) {
        
        NSString *maskImageName = [NSString stringWithFormat:@"Eight_%i.png",i];
        UIImage *maskImage = [UIImage imageNamed:maskImageName];
        
        if (i == 1)
        {
            CGRect myImageArea = CGRectMake (0, 0, maskImage.size.width, maskImage.size.height);
            
            CGImageRef mySubimage = CGImageCreateWithImageInRect (mainImageIV.image.CGImage, myImageArea);
            
            
            CGImageRef maskRef = maskImage.CGImage;
            
            CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
                                                CGImageGetHeight(maskRef),
                                                CGImageGetBitsPerComponent(maskRef),
                                                CGImageGetBitsPerPixel(maskRef),
                                                CGImageGetBytesPerRow(maskRef),
                                                CGImageGetDataProvider(maskRef), NULL, false);
            
            CGImageRef masked = CGImageCreateWithMask(mySubimage, mask);
            
            puzzleImage1 = [UIImage imageWithCGImage:masked];
            
            puzzleImageV1 = [[PuzzleImageView alloc] initWithFrame:CGRectMake(170, 25, maskImage.size.width, maskImage.size.height)];
            puzzleImageV1.originRect = CGRectMake(170-20, 25-20, maskImage.size.width+40, maskImage.size.height+40);
            puzzleImageV1.image = puzzleImage1;
            puzzleImageV1.tag = 13;
            puzzleImageV1.pDelegate = self;
            [gameView addSubview:puzzleImageV1];
            
            CGImageRelease(mySubimage);
            CGImageRelease(mask);
            CGImageRelease(masked);

        }
        else if (i == 2)
        {
            
            CGRect myImageArea = CGRectMake (164, 0, maskImage.size.width, maskImage.size.height);
            
            CGImageRef mySubimage = CGImageCreateWithImageInRect (mainImageIV.image.CGImage, myImageArea);
            
            CGImageRef maskRef = maskImage.CGImage;
            
            CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
                                                CGImageGetHeight(maskRef),
                                                CGImageGetBitsPerComponent(maskRef),
                                                CGImageGetBitsPerPixel(maskRef),
                                                CGImageGetBytesPerRow(maskRef),
                                                CGImageGetDataProvider(maskRef), NULL, false);
            
            CGImageRef masked = CGImageCreateWithMask(mySubimage, mask);
            
            puzzleImage2 = [UIImage imageWithCGImage:masked];
            
            puzzleImageV2 = [[PuzzleImageView alloc] initWithFrame:CGRectMake(334, 25, maskImage.size.width, maskImage.size.height)];
            puzzleImageV2.originRect = CGRectMake(334-20, 25-20, maskImage.size.width+40, maskImage.size.height+40);
            puzzleImageV2.image = puzzleImage2;
            puzzleImageV2.tag = 14;
            puzzleImageV2.pDelegate = self;
            [gameView addSubview:puzzleImageV2];
            
            CGImageRelease(mySubimage);
            CGImageRelease(mask);
            CGImageRelease(masked);

        }
        else if (i == 3)
        {
            CGRect myImageArea = CGRectMake (0, 61, maskImage.size.width, maskImage.size.height);
            CGImageRef mySubimage = CGImageCreateWithImageInRect (mainImageIV.image.CGImage, myImageArea);
            
            CGImageRef maskRef = maskImage.CGImage;
            
            CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
                                                CGImageGetHeight(maskRef),
                                                CGImageGetBitsPerComponent(maskRef),
                                                CGImageGetBitsPerPixel(maskRef),
                                                CGImageGetBytesPerRow(maskRef),
                                                CGImageGetDataProvider(maskRef), NULL, false);
            
            CGImageRef masked = CGImageCreateWithMask(mySubimage, mask);
            
            puzzleImage3 = [UIImage imageWithCGImage:masked];
            
            puzzleImageV3 = [[PuzzleImageView alloc] initWithFrame:CGRectMake(170, 86, maskImage.size.width, maskImage.size.height)];
            puzzleImageV3.originRect = CGRectMake(170-20,86-20, maskImage.size.width+40, maskImage.size.height+40);
            puzzleImageV3.image = puzzleImage3;
            puzzleImageV3.tag = 15;
            puzzleImageV3.pDelegate = self;
            [gameView addSubview:puzzleImageV3];
            
            CGImageRelease(mySubimage);
            CGImageRelease(mask);
            CGImageRelease(masked);

        }
        else if (i == 4)
        {
            CGRect myImageArea = CGRectMake (164, 104, maskImage.size.width, maskImage.size.height);
            
            CGImageRef mySubimage = CGImageCreateWithImageInRect (mainImageIV.image.CGImage, myImageArea);
            
            CGImageRef maskRef = maskImage.CGImage;
            
            CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
                                                CGImageGetHeight(maskRef),
                                                CGImageGetBitsPerComponent(maskRef),
                                                CGImageGetBitsPerPixel(maskRef),
                                                CGImageGetBytesPerRow(maskRef),
                                                CGImageGetDataProvider(maskRef), NULL, false);
            
            CGImageRef masked = CGImageCreateWithMask(mySubimage, mask);
            
            puzzleImage4 = [UIImage imageWithCGImage:masked];
            
            puzzleImageV4 = [[PuzzleImageView alloc] initWithFrame:CGRectMake (334, 129, maskImage.size.width, maskImage.size.height)];
            puzzleImageV4.originRect = CGRectMake(334-20, 129-20, maskImage.size.width+40, maskImage.size.height+40);
            puzzleImageV4.image = puzzleImage4;
            puzzleImageV4.tag = 16;
            puzzleImageV4.pDelegate = self;
            [gameView addSubview:puzzleImageV4];
            
            CGImageRelease(mySubimage);
            CGImageRelease(mask);
            CGImageRelease(masked);

        }
        else if (i == 5)
        {
            CGRect myImageArea = CGRectMake (0, 165, maskImage.size.width, maskImage.size.height);
            
            CGImageRef mySubimage = CGImageCreateWithImageInRect (mainImageIV.image.CGImage, myImageArea);
            
            CGImageRef maskRef = maskImage.CGImage;
            
            CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
                                                CGImageGetHeight(maskRef),
                                                CGImageGetBitsPerComponent(maskRef),
                                                CGImageGetBitsPerPixel(maskRef),
                                                CGImageGetBytesPerRow(maskRef),
                                                CGImageGetDataProvider(maskRef), NULL, false);
            
            CGImageRef masked = CGImageCreateWithMask(mySubimage, mask);
            
            puzzleImage5 = [UIImage imageWithCGImage:masked];
            
            puzzleImageV5 = [[PuzzleImageView alloc] initWithFrame:CGRectMake (170, 190, maskImage.size.width, maskImage.size.height)];
            puzzleImageV5.originRect = CGRectMake(170-20, 190-20, maskImage.size.width+40, maskImage.size.height+40);
            puzzleImageV5.tag = 17;
            puzzleImageV5.pDelegate = self;
            puzzleImageV5.image = puzzleImage5;
            [gameView addSubview:puzzleImageV5];
            
            CGImageRelease(mySubimage);
            CGImageRelease(mask);
            CGImageRelease(masked);

        }
        else if (i == 6)
        {
            CGRect myImageArea = CGRectMake (164, 168, maskImage.size.width, maskImage.size.height);
            
            CGImageRef mySubimage = CGImageCreateWithImageInRect (mainImageIV.image.CGImage, myImageArea);
            
            CGImageRef maskRef = maskImage.CGImage;
            
            CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
                                                CGImageGetHeight(maskRef),
                                                CGImageGetBitsPerComponent(maskRef),
                                                CGImageGetBitsPerPixel(maskRef),
                                                CGImageGetBytesPerRow(maskRef),
                                                CGImageGetDataProvider(maskRef), NULL, false);
            
            CGImageRef masked = CGImageCreateWithMask(mySubimage, mask);
            
            puzzleImage6 = [UIImage imageWithCGImage:masked];
            
            puzzleImageV6 = [[PuzzleImageView alloc] initWithFrame:CGRectMake (334, 193, maskImage.size.width, maskImage.size.height)];
            puzzleImageV6.originRect = CGRectMake(334-20, 193-20, maskImage.size.width+40, maskImage.size.height+40);
            puzzleImageV6.tag = 18;
            puzzleImageV6.pDelegate = self;
            puzzleImageV6.image = puzzleImage6;
            [gameView addSubview:puzzleImageV6];
            
            CGImageRelease(mySubimage);
            CGImageRelease(mask);
            CGImageRelease(masked);

        }
        else if (i == 7)
        {
            CGRect myImageArea = CGRectMake (0, 268, maskImage.size.width, maskImage.size.height);
            
            CGImageRef mySubimage = CGImageCreateWithImageInRect (mainImageIV.image.CGImage, myImageArea);
            
            CGImageRef maskRef = maskImage.CGImage;
            
            CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
                                                CGImageGetHeight(maskRef),
                                                CGImageGetBitsPerComponent(maskRef),
                                                CGImageGetBitsPerPixel(maskRef),
                                                CGImageGetBytesPerRow(maskRef),
                                                CGImageGetDataProvider(maskRef), NULL, false);
            
            CGImageRef masked = CGImageCreateWithMask(mySubimage, mask);
            
            puzzleImage7 = [UIImage imageWithCGImage:masked];
            
            puzzleImageV7 = [[PuzzleImageView alloc] initWithFrame:CGRectMake (170, 293, maskImage.size.width, maskImage.size.height)];
            puzzleImageV7.originRect = CGRectMake(170-20, 293-20, maskImage.size.width+40, maskImage.size.height+40);
            puzzleImageV7.tag = 19;
            puzzleImageV7.pDelegate = self;
            puzzleImageV7.image = puzzleImage7;
            [gameView addSubview:puzzleImageV7];
            
            CGImageRelease(mySubimage);
            CGImageRelease(mask);
            CGImageRelease(masked);

        }
        else if (i == 8)
        {
            CGRect myImageArea = CGRectMake (204, 309, maskImage.size.width, maskImage.size.height);
            
            CGImageRef mySubimage = CGImageCreateWithImageInRect (mainImageIV.image.CGImage, myImageArea);
            
            CGImageRef maskRef = maskImage.CGImage;
            
            CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
                                                CGImageGetHeight(maskRef),
                                                CGImageGetBitsPerComponent(maskRef),
                                                CGImageGetBitsPerPixel(maskRef),
                                                CGImageGetBytesPerRow(maskRef),
                                                CGImageGetDataProvider(maskRef), NULL, false);
            
            CGImageRef masked = CGImageCreateWithMask(mySubimage, mask);
            
            puzzleImage8 = [UIImage imageWithCGImage:masked];
            
            puzzleImageV8 = [[PuzzleImageView alloc] initWithFrame:CGRectMake (374, 334, maskImage.size.width, maskImage.size.height)];
            puzzleImageV8.originRect = CGRectMake(374-20, 334-20, maskImage.size.width+40, maskImage.size.height+40);
            puzzleImageV8.tag = 20;
            puzzleImageV8.pDelegate = self;
            puzzleImageV8.image = puzzleImage8;
            [gameView addSubview:puzzleImageV8];
            
            CGImageRelease(mySubimage);
            CGImageRelease(mask);
            CGImageRelease(masked);

        }

    }
    puzzleObj = [[PuzzleImageView alloc] init];
    
    [puzzleObj shakeImage:puzzleImageV1 value:YES];
    [puzzleObj shakeImage:puzzleImageV2 value:YES];
    [puzzleObj shakeImage:puzzleImageV3 value:YES];
    [puzzleObj shakeImage:puzzleImageV4 value:YES];
    [puzzleObj shakeImage:puzzleImageV5 value:YES];
    [puzzleObj shakeImage:puzzleImageV6 value:YES];
    [puzzleObj shakeImage:puzzleImageV7 value:YES];
    [puzzleObj shakeImage:puzzleImageV8 value:YES];

    [self performSelector:@selector(randomMovement:) withObject:puzzleImageV1 afterDelay:0.5f];
    [self performSelector:@selector(randomMovement:) withObject:puzzleImageV2 afterDelay:0.5f];
    [self performSelector:@selector(randomMovement:) withObject:puzzleImageV3 afterDelay:0.5f];
    [self performSelector:@selector(randomMovement:) withObject:puzzleImageV4 afterDelay:0.5f];
    [self performSelector:@selector(randomMovement:) withObject:puzzleImageV5 afterDelay:0.5f];
    [self performSelector:@selector(randomMovement:) withObject:puzzleImageV6 afterDelay:0.5f];
    [self performSelector:@selector(randomMovement:) withObject:puzzleImageV7 afterDelay:0.5f];
    [self performSelector:@selector(randomMovement:) withObject:puzzleImageV8 afterDelay:0.5f];
}
- (void)randomMovement: (PuzzleImageView *) movementImageV
{
    int a1 = arc4random()%[arrPositionNum count];
    NSString *str = [arrPositionNum objectAtIndex:a1];
    int num = [str intValue];
    [arrPositionNum removeObjectAtIndex:a1];
    
    NSString *str1 =  [arrPoints objectAtIndex:num];
    CGPoint point = CGPointFromString(str1);
    
    switch (num) {
        case 0: {
            [UIView animateWithDuration:1.0f animations:^{movementImageV.center = point;}
                             completion:^(BOOL finished) {[puzzleObj shakeImage:movementImageV value:NO];}];
            break;
        }
        case 1: {
            [UIView animateWithDuration:1.0f animations:^{movementImageV.center = point;}
                             completion:^(BOOL finished) {[puzzleObj shakeImage:movementImageV value:NO];}];
            break;
        }
        case 2: {
            [UIView animateWithDuration:1.0f animations:^{movementImageV.center = point;}
                             completion:^(BOOL finished) {[puzzleObj shakeImage:movementImageV value:NO];}];
            break;
        }
        case 3: {
            [UIView animateWithDuration:1.0f animations:^{movementImageV.center = point;}
                             completion:^(BOOL finished) {[puzzleObj shakeImage:movementImageV value:NO];}];
            break;
        }
        case 4: {
            [UIView animateWithDuration:1.0f animations:^{movementImageV.center = point;}
                             completion:^(BOOL finished) {[puzzleObj shakeImage:movementImageV value:NO];}];
            break;
        }
        case 5: {
            [UIView animateWithDuration:1.0f animations:^{movementImageV.center = point;}
                             completion:^(BOOL finished) {[puzzleObj shakeImage:movementImageV value:NO];}];
            break;
        }
        case 6: {
            [UIView animateWithDuration:1.0f animations:^{movementImageV.center = point;}
                             completion:^(BOOL finished) {[puzzleObj shakeImage:movementImageV value:NO];}];
            break;
        }
        case 7: {
            [UIView animateWithDuration:1.0f animations:^{movementImageV.center = point;}
                             completion:^(BOOL finished) {[puzzleObj shakeImage:movementImageV value:NO];}];
            break;
        }
        default:
            break;        
    }
}
- (IBAction) maskingTestAction{
    
        [self preparePuzzleImages];
        [lblGreat setHidden:YES];
}

- (IBAction)scrambleBtnPressed:(id)sender
{
    KinPics *kin = [arrPics objectAtIndex:currentImageIndex];
    
    if (kin.slices == 0){
        CGPoint point1 = CGPointMake(376, 327);
        CGPoint point2 = CGPointMake(375, 158);
        
        [UIView animateWithDuration:1.0f animations:^{puzzleImageV1.center = point1;
            [puzzleObj shakeImage:puzzleImageV1 value:YES];
        }
                         completion:^(BOOL finished) {[puzzleObj shakeImage:puzzleImageV1 value:NO];
                             [UIView animateWithDuration:1.0f animations:^{puzzleImageV2.center = point2;
                                 [puzzleObj shakeImage:puzzleImageV2 value:YES];}
                                              completion:^(BOOL finished) {[puzzleObj shakeImage:puzzleImageV2 value:NO];
                                                  [self showAlert];
                                                  [playBtn setTitle:@"Play again" forState:UIControlStateNormal];
                                              }];
                         }];
        
    }
    else if (kin.slices == 1) {
        CGPoint point1 = CGPointMake(299, 154);
        CGPoint point2 = CGPointMake(476, 154);
        CGPoint point3 = CGPointMake(299, 330);
        CGPoint point4 = CGPointMake(450, 330);
        [UIView animateWithDuration:1.0f animations:^{puzzleImageV1.center = point1;
            [puzzleObj shakeImage:puzzleImageV1 value:YES];
        }
                         completion:^(BOOL finshed) {[puzzleObj shakeImage:puzzleImageV1 value:NO];
                             
                             [UIView animateWithDuration:1.0f animations:^{puzzleImageV2.center = point2;
                                 [puzzleObj shakeImage:puzzleImageV2 value:YES];
                             }
                                              completion:^(BOOL finished) {[puzzleObj shakeImage:puzzleImageV2 value:NO];
                                                  [UIView animateWithDuration:1.0f animations:^{puzzleImageV3.center = point3;
                                                      [puzzleObj shakeImage:puzzleImageV3 value:YES];
                                                  }
                                                                   completion:^(BOOL finished) {[puzzleObj shakeImage:puzzleImageV3 value:NO];
                                                                       [UIView animateWithDuration:1.0f animations:^{puzzleImageV4.center = point4;
                                                                           [puzzleObj shakeImage:puzzleImageV4 value:YES];}
                                                                                        completion:^(BOOL finished) {[puzzleObj shakeImage:puzzleImageV4 value:NO];
                                                                                            [self showAlert];
                                                                                            [playBtn setTitle:@"Play again" forState:UIControlStateNormal];
                                                                                        }];
                                                                   }];
                                              }];
                             
                         }];
        
        
    }
    else if (kin.slices == 2) {
        CGPoint point1 = CGPointMake(294, 100);
        CGPoint point2 = CGPointMake(476, 120);
        CGPoint point3 = CGPointMake(273, 216);
        CGPoint point4 = CGPointMake(455, 256);
        CGPoint point5 = CGPointMake(273, 350);
        CGPoint point6 = CGPointMake(455, 370);
        
        [UIView animateWithDuration:1.0f animations:^{puzzleImageV1.center = point1;
            [puzzleObj shakeImage:puzzleImageV1 value:YES];
        }
                         completion:^(BOOL finshed) {[puzzleObj shakeImage:puzzleImageV1 value:NO];
                             
                             [UIView animateWithDuration:1.0f animations:^{puzzleImageV2.center = point2;
                                 [puzzleObj shakeImage:puzzleImageV2 value:YES];
                             }
                                              completion:^(BOOL finished) {[puzzleObj shakeImage:puzzleImageV2 value:NO];
                                                  [UIView animateWithDuration:1.0f animations:^{puzzleImageV3.center = point3;
                                                      [puzzleObj shakeImage:puzzleImageV3 value:YES];
                                                  }
                                                                   completion:^(BOOL finished) {[puzzleObj shakeImage:puzzleImageV3 value:NO];
                                                                       [UIView animateWithDuration:1.0f animations:^{puzzleImageV4.center = point4;
                                                                           [puzzleObj shakeImage:puzzleImageV4 value:YES];
                                                                       }
                                                                                        completion:^(BOOL finished) {[puzzleObj shakeImage:puzzleImageV4 value:NO];
                                                                                            [UIView animateWithDuration:1.0f animations:^{puzzleImageV5.center = point5;
                                                                                                [puzzleObj shakeImage:puzzleImageV5 value:YES];
                                                                                            }
                                                                                                             completion:^(BOOL finished){[puzzleObj shakeImage:puzzleImageV5 value:NO];
                                                                                                                 [UIView animateWithDuration:1.0f animations:^{puzzleImageV6.center = point6;
                                                                                                                     [puzzleObj shakeImage:puzzleImageV6 value:YES];
                                                                                                                 }
                                                                                                                                  completion:^(BOOL finished) {
                                                                                                                                      [puzzleImageV6 shakeImage:puzzleImageV6 value:NO];
                                                                                                                                      [self showAlert];
                                                                                                                                      [playBtn setTitle:@"Play again" forState: UIControlStateNormal];
                                                                                                                                  }];
                                                                                                             }];
                                                                                        }];
                                                                       
                                                                   }];
                                              }];
                         }];
        
        
    }
    
}


- (void)viewDidUnload {
    lblWon = nil;
    viewCarousel = nil;
    gameView = nil;
    playBtn = nil;
    [super viewDidUnload];
}
@end
