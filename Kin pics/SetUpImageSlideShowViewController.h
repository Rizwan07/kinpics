//
//  SlideViewController.h
//  Kin pics
//
//  Created by Rizwan on 1/31/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AVFoundation/AVFoundation.h>

@interface SetUpImageSlideShowViewController : UIViewController<AVAudioPlayerDelegate, AVAudioRecorderDelegate> {
    
    @private
    
   // IBOutlet UITextField *textField;
    IBOutlet UIImageView *imageView;
    IBOutlet UIScrollView *theScrollView;
    IBOutlet UIButton *btnPrevious;
    IBOutlet UIButton *btnForward;
    IBOutlet UIButton *btnSave;
    
//    NSData *piData;
//    NSData *piDataHeart;
    
    BOOL didScroll;
    BOOL didAudioRecorded;
    BOOL didHeartAudioRecorded;
}

@property (strong, nonatomic) AVAudioRecorder *audioRecorder;
@property (strong, nonatomic) AVAudioRecorder *audioRecorderForAll;
@property (strong, nonatomic) UIImage *orgImage;

@property (strong, nonatomic) IBOutlet UIButton *btnAudioPlay;
@property (strong, nonatomic) IBOutlet UIButton *btnAudioRecord;
@property (strong, nonatomic) IBOutlet UITextField *textField;

@property (strong, nonatomic) IBOutlet UIButton *heartAudioBtn;
@property (strong, nonatomic) IBOutlet UIButton *heartRecordBtn;
@property (strong, nonatomic) NSArray *arrPics;
@property (assign, nonatomic) int selectedImageIndex;

- (IBAction)backBtnPressed:(id)sender;
- (IBAction)homeBtnPressed:(id)sender;

- (IBAction)audioPlayBtnPressed:(id)sender;
- (IBAction)audioRecordTouchDownBtnPressed:(id)sender;
- (IBAction)audioRecordTouchUpInsideBtnPressed:(id)sender;

- (IBAction)audioPlayForAllBtnPressed:(id)sender;
- (IBAction)audioRecordTouchDownForAllBtnPressed:(id)sender;
- (IBAction)audioRecordTouchUpInsideForAllBtnPressed:(id)sender;


- (IBAction)saveBtnPressed:(id)sender;
- (IBAction)previousBtnPressed:(id)sender;
- (IBAction)forwardBtnPressed:(id)sender;
@end
