//
//  RumexCustomTabBar.m
//  Kinpics
//
//  Created by Rizwan on 2/16/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import "RXCustomTabBar.h"

@implementation RXCustomTabBar

@synthesize btn1 = _btn1, btn2 = _btn2, btn3 = _btn3, btn4 = _btn4;
@synthesize mySelectedIndex = _mySelectedIndex;

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //[self.view setFrame:CGRectMake(0, 0, 320, 580)];
	
	[self hideTabBar];
	[self addCustomElements];
}

- (void)hideTabBar
{
	for(UIView *view in self.view.subviews)
	{
		if([view isKindOfClass:[UITabBar class]])
		{
			view.hidden = YES;
			break;
		}
	}
}

- (void)hideNewTabBar 
{
    self.btn1.hidden = 1;
    self.btn2.hidden = 1;
    self.btn3.hidden = 1;
    self.btn4.hidden = 1;
}

- (void)showNewTabBar 
{
    self.btn1.hidden = 0;
    self.btn2.hidden = 0;
    self.btn3.hidden = 0;
    self.btn4.hidden = 0;
}

-(void)addCustomElements
{
	// Initialise our two images
    
    UIImage *bar = [UIImage imageNamed:@"bottom-bar.png"];
    
    UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(0, 958, 768, 72)];
    [image setContentMode:UIViewContentModeScaleAspectFill];
    [image setImage:bar];
    [self.view addSubview:image];
    
	UIImage *btnImage = [UIImage imageNamed:@"kin-pic-orange.png"];
	UIImage *btnImageSelected = [UIImage imageNamed:@"kin-pic.png"];
	
	self.btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
	_btn1.frame = CGRectMake(48, 920, 94, 94);
	[_btn1 setBackgroundImage:btnImage forState:UIControlStateNormal];
	[_btn1 setBackgroundImage:btnImageSelected forState:UIControlStateSelected];
    [_btn1 setBackgroundImage:btnImage forState:UIControlStateHighlighted];
	[_btn1 setTag:0];
    [_btn1 setSelected:_mySelectedIndex == 0];
	
	btnImage = [UIImage imageNamed:@"puzzle-orange.png"];
	btnImageSelected = [UIImage imageNamed:@"puzzle-1.png"];
	self.btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
	_btn2.frame = CGRectMake(201, 920, 94, 94);
	[_btn2 setBackgroundImage:btnImage forState:UIControlStateNormal];
	[_btn2 setBackgroundImage:btnImageSelected forState:UIControlStateSelected];
    [_btn2 setBackgroundImage:btnImage forState:UIControlStateHighlighted];
	[_btn2 setTag:1];
	[_btn2 setSelected:_mySelectedIndex == 1];
    
	btnImage = [UIImage imageNamed:@"faimly-match-orange.png"];
	btnImageSelected = [UIImage imageNamed:@"faimly-match.png"];
	self.btn3 = [UIButton buttonWithType:UIButtonTypeCustom];
    _btn3.frame = CGRectMake(476, 920, 94, 94);
	[_btn3 setBackgroundImage:btnImage forState:UIControlStateNormal];
	[_btn3 setBackgroundImage:btnImageSelected forState:UIControlStateSelected];
    [_btn3 setBackgroundImage:btnImage forState:UIControlStateHighlighted];
	[_btn3 setTag:2];
	[_btn3 setSelected:_mySelectedIndex == 2];
    
	btnImage = [UIImage imageNamed:@"fun-effect-orange.png"];
	btnImageSelected = [UIImage imageNamed:@"fun-effect.png"];
	self.btn4 = [UIButton buttonWithType:UIButtonTypeCustom];
	_btn4.frame = CGRectMake(626, 920, 94, 94);
	[_btn4 setBackgroundImage:btnImage forState:UIControlStateNormal];
	[_btn4 setBackgroundImage:btnImageSelected forState:UIControlStateSelected];
    [_btn4 setBackgroundImage:btnImage forState:UIControlStateHighlighted];
	[_btn4 setTag:3];
    [_btn4 setSelected:_mySelectedIndex == 3];
	
	// Add my new buttons to the view
	[self.view addSubview:_btn1];
	[self.view addSubview:_btn2];
	[self.view addSubview:_btn3];
	[self.view addSubview:_btn4];
	
	[_btn1 addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
	[_btn2 addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
	[_btn3 addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
	[_btn4 addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)buttonClicked:(id)sender
{    
	int tagNum = [sender tag];
	
    [_btn1 setSelected:tagNum == 0];
    [_btn2 setSelected:tagNum == 1];
    [_btn3 setSelected:tagNum == 2];
    [_btn4 setSelected:tagNum == 3];
    
    self.selectedIndex = tagNum;
}

@end
