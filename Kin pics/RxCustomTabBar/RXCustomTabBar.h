//
//  RumexCustomTabBar.h
//  Kinpics
//
//  Created by Rizwan on 2/16/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RXCustomTabBar : UITabBarController
{
	UIButton *_btn1;
	UIButton *_btn2;
	UIButton *_btn3;
	UIButton *_btn4;
}

@property (nonatomic, retain) UIButton *btn1;
@property (nonatomic, retain) UIButton *btn2;
@property (nonatomic, retain) UIButton *btn3;
@property (nonatomic, retain) UIButton *btn4;

@property (nonatomic, assign) int mySelectedIndex;

-(void) hideTabBar;
-(void) addCustomElements;

- (void)buttonClicked:(id)sender;

-(void) hideNewTabBar;
//-(void) ShowNewTabBar;

@end
