//
//  GameViewController.h
//  Kin pics
//
//  Created by Rizwan on 2/14/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
@class GameConfig;
@interface GameViewController : UIViewController
{
    IBOutlet UIView *_gameView;
    
    unsigned int _gameState;
    
    NSMutableArray *_cardViews;
    NSMutableArray *_activeCardViews;
    NSMutableArray *_selectedCards;
    
    GameConfig *_gameConfig;
}

@property (nonatomic,retain) GameConfig* gameConfig;

@end
