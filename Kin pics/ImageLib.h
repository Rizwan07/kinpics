//
//  ImageLib.h
//  Kin pics
//
//  Created by Rizwan on 2/14/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageLib : NSObject
{
    NSMutableDictionary *_images;
}
@property (nonatomic,retain) NSMutableDictionary *images;

- (id) initWithPlistNamed:(NSString*)plistFilename;
- (void) loadImagesFromNameArray:(NSArray*)nameArray;

@end
