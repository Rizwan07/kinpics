//
//  SetupViewController.m
//  Kin pics
//
//  Created by Rizwan on 1/17/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import "GameOptionsViewController.h"
#import "RXCustomTabBar.h"
#import "SetUpSlideShowViewController.h"
#import "SetUpPuzzleViewController.h"
#include "SetUpCardsViewController.h"
#import "SetUpDifferentLookViewController.h"

#import "Singleton.h"

@interface GameOptionsViewController ()

@end

@implementation GameOptionsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([[Singleton sharedSingleton] didEnterInSetUpOptions]) {
        [lblHeading setText:@"Set Up"];
    }
    else {
        [lblHeading setText:@"Play"];
    }
    
//    UIImage *image = [UIImage imageNamed:@"splash.png"];
//    
//    NSData *data = UIImagePNGRepresentation(image);
//    
//    NSString *path = NSHomeDirectory();
//    
//    int random = arc4random()%1000000;
//    
//    
//    path = [path stringByAppendingPathComponent:@"Documents"];
//    
//    path = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"%d.png",random]];
//    
//    [data writeToFile:path atomically:YES];
//    NSLog(@"%@",path);
    
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backBtnPressed:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}
- (void)viewDidAppear:(BOOL)animated {
    
    if ([[Singleton sharedSingleton] didHomeBtnPressed]) {
        [[Singleton sharedSingleton] setDidHomeBtnPressed:NO];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

- (IBAction)setupBtnPressed:(id)sender {
    
    int tag = [sender tag];
    
    RXCustomTabBar *tabBar;
    
    if ([[Singleton sharedSingleton] didEnterInSetUpOptions]) {
        tabBar = [self.storyboard instantiateViewControllerWithIdentifier:@"SetUpTabBarController"];
    }
    else {
        tabBar = [self.storyboard instantiateViewControllerWithIdentifier:@"PlayTabBarController"];
    }
    
    [tabBar setMySelectedIndex:tag];
    [tabBar buttonClicked:sender];
    
    [self presentViewController:tabBar animated:YES completion:nil];
    //[self presentModalViewController:tabBar animated:YES];
    
}
- (void)viewDidUnload {
    lblHeading = nil;
    [super viewDidUnload];
}
@end
