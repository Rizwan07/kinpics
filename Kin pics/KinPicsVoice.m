//
//  KinPicsVoice.m
//  Kin pics
//
//  Created by Rizwan on 4/1/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import "KinPicsVoice.h"

@implementation KinPicsVoice

@synthesize name = _name;
@synthesize textVoice = _textVoice;
@synthesize heartVoice = _heartVoice;

@end
