//
//  SlideViewController.m
//  Kin pics
//
//  Created by Rizwan on 1/31/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import "SetUpImageSlideShowViewController.h"
#import "KinPicsDB.h"

@interface SetUpImageSlideShowViewController () {
    
    KinPicsDB *kpDb;
    
}

@property (strong, nonatomic) AVAudioPlayer *audioPlayer;
@property (strong, nonatomic) AVAudioPlayer *audioPlayer2;

@property (strong, nonatomic) KinPicsVoice *kpVoice;

@end

@implementation SetUpImageSlideShowViewController
@synthesize audioPlayer = _audioPlayer, audioRecorder = _audioRecorder, audioRecorderForAll = _audioRecorderForAll;
@synthesize textField = _textField;
@synthesize orgImage = _orgImage;
@synthesize arrPics = _arrPics;
@synthesize selectedImageIndex = _selectedImageIndex;
@synthesize audioPlayer2 = _audioPlayer2;


- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    
//    if (&NSURLIsExcludedFromBackupKey == nil) { // iOS <= 5.0.1
//        const char* filePath = [[URL path] fileSystemRepresentation];
//        
//        const char* attrName = "com.apple.MobileBackup";
//        u_int8_t attrValue = 1;
//        
//        int result = setxattr(filePath, attrName, &attrValue, sizeof(attrValue), 0, 0);
//        return result == 0;
//    }
//    else { // iOS >= 5.1
        NSError *error = nil;
        [URL setResourceValue:[NSNumber numberWithBool:YES] forKey:NSURLIsExcludedFromBackupKey error:&error];
        return error == nil;
//    }
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    kpDb = [[KinPicsDB alloc] init];
    
    if (_arrPics == nil) {
        
        didAudioRecorded = NO;
        didHeartAudioRecorded = NO;

        [imageView setImage:_orgImage];
        [theScrollView setHidden:YES];
        [btnPrevious setHidden:YES];
        [btnForward setHidden:YES];
    }
    else {
        didAudioRecorded = YES;
        didHeartAudioRecorded = YES;
        
        KinPics *kinPic = [_arrPics objectAtIndex:_selectedImageIndex];
        self.kpVoice = [kpDb getVoicesOfKinPic:[kinPic name]];
        [imageView setHidden:YES];
    }

    didScroll = YES;
    
    [self setSoundFileData];
    
}

- (void)setSoundFileData {
    
    NSDictionary *recordSettings = [NSDictionary
                                    dictionaryWithObjectsAndKeys:
                                    [NSNumber numberWithInt:AVAudioQualityMin],
                                    AVEncoderAudioQualityKey,
                                    [NSNumber numberWithInt:16],
                                    AVEncoderBitRateKey,
                                    [NSNumber numberWithInt: 2],
                                    AVNumberOfChannelsKey,
                                    [NSNumber numberWithFloat:44100.0],
                                    AVSampleRateKey,
                                    nil];
    NSDictionary *recordSettings2 = [NSDictionary
                                    dictionaryWithObjectsAndKeys:
                                    [NSNumber numberWithInt:AVAudioQualityMin],
                                    AVEncoderAudioQualityKey,
                                    [NSNumber numberWithInt:16],
                                    AVEncoderBitRateKey,
                                    [NSNumber numberWithInt: 2],
                                    AVNumberOfChannelsKey,
                                    [NSNumber numberWithFloat:44100.0],
                                    AVSampleRateKey,
                                    nil];

    
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = [dirPaths objectAtIndex:0];
    
    NSString *soundFilePath = [docsDir stringByAppendingPathComponent:@"sound.caf"];
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    
    NSString *heartSoundFilePath = [docsDir stringByAppendingPathComponent:@"soundForAll.caf"];
    NSURL *heartSoundFileURL = [NSURL fileURLWithPath:heartSoundFilePath];
    
    NSError *error1 = nil;
    NSError *error2 = nil;
    
    self.audioRecorder = [[AVAudioRecorder alloc] initWithURL:soundFileURL settings:recordSettings error:&error1];
    self.audioRecorderForAll = [[AVAudioRecorder alloc] initWithURL:heartSoundFileURL settings:recordSettings2 error:&error2];
    _audioRecorder.delegate = self;
    _audioRecorderForAll.delegate = self;

    if (error1) {
        NSLog(@"Error1: %@", [error1 localizedDescription]);
    } else {
        [_audioRecorder prepareToRecord];
    }
    
    if (error2) {
        NSLog(@"Error2: %@", [error2 localizedDescription]);
    } else {
        [_audioRecorderForAll prepareToRecord];
    }

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self removePreviousAddedImagesFromScrollView];
    
    NSInteger numberOfImages = [_arrPics count];
    
    //int totalNumberPage = (numberOfImages%8 == 0)? numberOfImages/8 : numberOfImages/8+1;
    
    [theScrollView setContentSize:CGSizeMake(imageView.frame.size.width*numberOfImages, theScrollView.frame.size.height)];
    [theScrollView setContentOffset:CGPointMake(imageView.frame.size.width * _selectedImageIndex, 0)];

    NSString *filePath = [[Singleton sharedSingleton] getDocumentDirectoryPath];
    
    for (int i = 0; i < numberOfImages; i++) {
        
        KinPics *kinPic = [_arrPics objectAtIndex:i];
        
        NSString *fileName = [filePath stringByAppendingPathComponent:[kinPic name]];
        
        int xOffset = i * imageView.frame.size.width;
        
        
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(10 + xOffset, 10, theScrollView.frame.size.width-20, theScrollView.frame.size.height-20)];
        [imgView setImage:[UIImage imageWithContentsOfFile:fileName]];
        [imgView setContentMode:UIViewContentModeScaleAspectFit];
        
        [theScrollView addSubview:imgView];
        
    }
    [self setOtherData];
}

- (void)removePreviousAddedImagesFromScrollView {
    
    for (UIView *view in [theScrollView subviews]) {
        [view removeFromSuperview];
    }
}

- (void)setOtherData {
    
    _selectedImageIndex = theScrollView.contentOffset.x/imageView.frame.size.width;

    if (_selectedImageIndex < [_arrPics count]) {
        
        KinPics *kinPic = [_arrPics objectAtIndex:_selectedImageIndex];
        
        self.kpVoice = [kpDb getVoicesOfKinPic:[kinPic name]];

        [_textField setText:[kinPic text]];
        
        [btnSave setEnabled:NO];
//        piData = nil;
//        piDataHeart = nil;
        
//        didAudioRecorded = NO;
//        didHeartAudioRecorded = NO;
        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backBtnPressed:(id)sender {
    
    [_audioRecorder deleteRecording];
    [_audioRecorderForAll deleteRecording];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)homeBtnPressed:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    [[Singleton sharedSingleton] setDidHomeBtnPressed:YES];
}



- (IBAction)audioPlayForAllBtnPressed:(id)sender {
    
    if (_arrPics == nil) {
        if (!_audioRecorderForAll.recording)
        {
            self.heartAudioBtn.enabled = NO;
            self.heartRecordBtn.enabled = YES;
            NSError *error;
            
            self.audioPlayer2 = [[AVAudioPlayer alloc]initWithContentsOfURL:_audioRecorderForAll.url error:&error];
            
            _audioPlayer2.delegate = self;
            
            if (error)
                NSLog(@"Error: %@",[error localizedDescription]);
            else
                [_audioPlayer2 play];
        }
    }
    else {
        
        NSError *error;
        
//        if (piDataHeart != nil) {
//            self.audioPlayer2 = [[AVAudioPlayer alloc] initWithData:piDataHeart error:&error];
//        }
//        else {
//            KinPics *kinPic = [_arrPics objectAtIndex:_selectedImageIndex];
//            
//            kpVoice = [kpDb getVoicesOfKinPic:[kinPic name]];

            NSData *data2 = [_kpVoice heartVoice];
            
            self.audioPlayer2 = [[AVAudioPlayer alloc] initWithData:data2 error:&error];
            
//        }
        _audioPlayer2.delegate = self ;
        
        [_audioPlayer2 prepareToPlay];
        if (error)
            NSLog(@"Error: %@",[error localizedDescription]);
        else
            [_audioPlayer2 play];
        
    }

    //
//    if (_arrPics == nil) {
//        
//        if (!_audioRecorder.recording)
//        {
//            self.btnAudioPlay.enabled = NO;
//            self.btnAudioRecord.enabled = YES;
//            
//            NSError *error;
//            
//            self.audioPlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:_audioRecorder.url error:&error];
//            
//            _audioPlayer.delegate = self;
//            
//            if (error)
//                NSLog(@"Error: %@",[error localizedDescription]);
//            else
//                [_audioPlayer play];
//        }
//    }
//    else {
//        
//        NSError *error;
//        
//        if (piData != nil) {
//            self.audioPlayer = [[AVAudioPlayer alloc] initWithData:piData error:&error];
//        }
//        else {
//            KinPics *kinPic = [_arrPics objectAtIndex:_selectedImageIndex];
//            
//            
//            NSData *data = [kinPic voice];
//            
//            self.audioPlayer = [[AVAudioPlayer alloc] initWithData:data error:&error];
//            
//        }
//        _audioPlayer.delegate = self ;
//        
//        [_audioPlayer prepareToPlay];
//        if (error)
//            NSLog(@"Error: %@",[error localizedDescription]);
//        else
//            [_audioPlayer play];
//        
//    }

}

- (IBAction)audioRecordTouchDownForAllBtnPressed:(id)sender {
    
    if (!_audioRecorderForAll.recording) {
        [_audioRecorderForAll record];
        self.heartRecordBtn.enabled = NO;

    }
    
}


- (IBAction)audioRecordTouchUpInsideForAllBtnPressed:(id)sender {
    
    didHeartAudioRecorded = YES;
    self.heartAudioBtn.enabled = YES;

    if (_audioRecorderForAll.recording) {
        [_audioRecorderForAll stop];
    }
    else if (_audioPlayer2.playing) {
        [_audioPlayer2 stop];
    }
    
}


- (IBAction)saveBtnPressed:(id)sender {
    
    if (!_textField.text.length) {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:@"Text is missing" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }

    if (!didAudioRecorded || !didHeartAudioRecorded) {
        UIAlertView *alView = [[UIAlertView alloc]initWithTitle:nil message:@"Voice is missing" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alView show];
        return;
    }
    
    
    NSTimeInterval timeInterval = [NSDate timeIntervalSinceReferenceDate];
    
    double rand = timeInterval;
    
    NSString *picName = [NSString stringWithFormat:@"%f",rand];
    picName = [picName stringByReplacingOccurrencesOfString:@"." withString:@"0"];
    picName = [picName stringByAppendingString:@".png"];
    
//    int random = (int)round(timeInterval);
//    NSString *picName = [NSString stringWithFormat:@"%d.png", random];
    
    [[Singleton sharedSingleton] removeAllKinPicsData];
    if (_arrPics == nil) {
        
        NSData *data = [NSData dataWithContentsOfURL:_audioRecorder.url];
        NSData *heartData = [NSData dataWithContentsOfURL:_audioRecorderForAll.url];

        [self saveImageintoDocumentDirectoryWihName:picName];
//        [kp_db addKinPicsWithId:(int)roundf(timeInterval) name:picName voice:data text:_textField.text];
        [kpDb addKinPicsWithName:picName voice:data text:_textField.text heartVoice:heartData];

        [self.navigationController popViewControllerAnimated:YES];

    }
    else {
        
//        //KinPics *kp = [_arrPics objectAtIndex:_selectedImageIndex];
//        if (piData != nil) {
//            
//            //[kp_db updateKinPicsVoice:piData text:_textField.text wherePicName:[kp name]];
//            [kpDb updateKinPicsVoice:piData heartVoice:piDataHeart text:_textField.text wherePicName:[_kpVoice name]];
//        }
//        else {
           // [kp_db updateKinPicsVoice:[kp voice] text:_textField.text wherePicName:[kp name]];
            [kpDb updateKinPicsVoice:[_kpVoice textVoice] heartVoice:[_kpVoice heartVoice] text:_textField.text wherePicName:[_kpVoice name]];
//        }
        [btnSave setEnabled:NO];
        _arrPics = [[Singleton sharedSingleton] getDataOfAllKinPics];
        //[kp_db updateKinPicsVoice:data text:_textField.text wherePicIdis:[[_arrPics objectAtIndex:_selectedImageIndex] picsId]];
    }
    
//    piData = nil;
//    piDataHeart = nil;
    
    
}

- (IBAction)audioPlayBtnPressed:(id)sender {
    
    if (_arrPics == nil) {
        
        if (!_audioRecorder.recording)
        {
            self.btnAudioPlay.enabled = NO;
            self.btnAudioRecord.enabled = YES;
            
            NSError *error;
            
            self.audioPlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:_audioRecorder.url error:&error];
            
            _audioPlayer.delegate = self;
            
            if (error)
                NSLog(@"Error: %@",[error localizedDescription]);
            else
                [_audioPlayer play];
        }
    }
    else {
        
        NSError *error;

//        if (piData != nil) {
//            self.audioPlayer = [[AVAudioPlayer alloc] initWithData:piData error:&error];
//        }
//        else {
//            KinPics *kinPic = [_arrPics objectAtIndex:_selectedImageIndex];
            
            
            NSData *data = [_kpVoice textVoice];
            
            self.audioPlayer = [[AVAudioPlayer alloc] initWithData:data error:&error];
            
//        }
        _audioPlayer.delegate = self ;
        
        [_audioPlayer prepareToPlay];
        if (error)
            NSLog(@"Error: %@",[error localizedDescription]);
        else
            [_audioPlayer play];
            
    }
}

- (IBAction)audioRecordTouchDownBtnPressed:(id)sender {
    
    if (!_audioRecorder.recording)
    {
        self.btnAudioRecord.enabled = NO;
        [_audioRecorder record];
        
    }
}

- (IBAction)audioRecordTouchUpInsideBtnPressed:(id)sender {
    
    didAudioRecorded = YES;
    
    self.btnAudioPlay.enabled = YES;
    
    if (_audioRecorder.recording)
    {
        [_audioRecorder stop];
    } else if (_audioPlayer.playing) {
        [_audioPlayer stop];
    }
}

- (IBAction)previousBtnPressed:(id)sender {
    
    if (didScroll) {
        
        CGRect rect = [theScrollView frame];
        rect.origin = [theScrollView contentOffset];
        rect.origin.x -= imageView.frame.size.width;
        if (rect.origin.x >= 0) {
            didScroll = NO;
//            [theScrollView scrollRectToVisible:rect animated:YES];
            [self scrollViewScrollToRect:rect];
        }
    }
}

- (IBAction)forwardBtnPressed:(id)sender {
    
    if (didScroll) {
        
        CGRect rect = [theScrollView frame];
        rect.origin = [theScrollView contentOffset];
        rect.origin.x += imageView.frame.size.width;
        if (rect.origin.x < theScrollView.contentSize.width) {
            didScroll = NO;
//            [theScrollView scrollRectToVisible:rect animated:YES];
            [self scrollViewScrollToRect:rect];
        }
    }
}
- (void)scrollViewScrollToRect:(CGRect )rect {
    
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionAllowAnimatedContent
                     animations:^{
                         [theScrollView setAlpha:0.7];
                         //[self removePreviousAddedImagesFromView];
                     }
                     completion:^(BOOL finished){
                         
                         [UIView animateWithDuration:0.5
                                               delay:0
                                             options:UIViewAnimationOptionCurveEaseIn
                                          animations:^{
                                              [theScrollView setAlpha:1.0];
                                              [theScrollView scrollRectToVisible:rect animated:YES];
                                          }
                                          completion:nil];
                     }];
    
}

#pragma mark --
#pragma mark UIScrollView Delegates

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    didScroll = YES;
    [self setOtherData];
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    
    didScroll = YES;
    [self setOtherData];
}

- (void)saveImageintoDocumentDirectoryWihName:(NSString *)picName {
    
    NSArray *docspaths=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *docsdir=[docspaths objectAtIndex:0];
    
    NSString *docName = [docsdir stringByAppendingPathComponent:picName];
    
    [self addSkipBackupAttributeToItemAtURL:[NSURL fileURLWithPath:docName]];
    
    [UIImagePNGRepresentation(_orgImage) writeToFile:docName atomically:YES];
    UIGraphicsBeginImageContext( CGSizeMake(400, 400));
    [_orgImage drawInRect:CGRectMake(0,0,400,400)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    NSString *docName1 = [docsdir stringByAppendingPathComponent:[NSString stringWithFormat:@"small%@", picName]];

    [self addSkipBackupAttributeToItemAtURL:[NSURL fileURLWithPath:docName1]];

    [UIImagePNGRepresentation(newImage) writeToFile:docName1 atomically:YES];
}


#pragma mark --
#pragma mark AVAudio Player & Record Delegates

-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    self.btnAudioPlay.enabled = YES;
    self.btnAudioRecord.enabled = YES;
    self.heartAudioBtn.enabled = YES;
    self.heartRecordBtn.enabled = YES;
}
-(void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error
{
    self.btnAudioRecord.enabled = YES;
    self.heartRecordBtn.enabled = YES;
}
-(void)audioRecorderDidFinishRecording:(AVAudioRecorder *)recorder successfully:(BOOL)flag
{
    if ([_arrPics count]) {
        
        if (recorder == _audioRecorder) {
            [_kpVoice setTextVoice:[NSData dataWithContentsOfURL:_audioRecorder.url]];
        }
        else if (recorder == _audioRecorderForAll) {
            [_kpVoice setHeartVoice:[NSData dataWithContentsOfURL:_audioRecorderForAll.url]];
        }
    }
//    piData = [NSData dataWithContentsOfURL:_audioRecorder.url];
//    piDataHeart = [NSData dataWithContentsOfURL:_audioRecorderForAll.url];
    [btnSave setEnabled:YES];
}
-(void)audioRecorderEncodeErrorDidOccur:(AVAudioRecorder *)recorder error:(NSError *)error
{
    self.btnAudioRecord.enabled = YES;
    self.heartRecordBtn.enabled = YES;
}

#pragma mark --
#pragma mark UITextField Delegates

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    [btnSave setEnabled:YES];
    
    return YES;
}

- (void)viewDidUnload {
    _textField = nil;
    [self setTextField:nil];
    imageView = nil;
    theScrollView = nil;
    btnPrevious = nil;
    btnForward = nil;
    btnSave = nil;
    [super viewDidUnload];
}
@end
