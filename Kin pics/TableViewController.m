//
//  TableViewController.m
//  Kin pics
//
//  Created by Rizwan on 2/16/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import "TableViewController.h"
#import "SetUpCardsViewController.h"

@interface TableViewController ()

@property (nonatomic, assign) int counter;


- (void)setMask;

@end

@implementation TableViewController

@synthesize btnOption = _btnOption;
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)setMask {
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    

    _array = [[NSMutableArray alloc]init];
    
//    for (int i = 0; i <= [arrPics count]; i ++) {
//        [array addObject:@"0"];
//    }
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.

    UIBarButtonItem *bar = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(barButton:)];
    
     self.navigationItem.rightBarButtonItem = bar;
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [_array removeAllObjects];
    _arrPics = [[Singleton sharedSingleton] getDataOfAllKinPics];
    
    for (int i = 0; i < [_arrPics count]; i ++) {
        
        [_array addObject:@"0"];
    }
    
    NSString *str = [NSString stringWithFormat:@"%dcards", _btnOption];
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:str]) {
        
        for (int i = 0; i < _btnOption; i ++) {
            
            NSString *strKey = [NSString stringWithFormat:@"%d_%d", i%_btnOption, _btnOption];
            NSString *strName = [[NSUserDefaults standardUserDefaults] stringForKey:strKey];
            
            for (int j = 0; j < [_arrPics count]; j++) {
                
                KinPics *kp = _arrPics[j];
                if ([[NSString stringWithFormat:@"small%@",[kp name] ]  isEqualToString:strName]) {
                    [_array replaceObjectAtIndex:j withObject:@"1"];
                    break;
                }
            }
            
        }
        _counter = _btnOption;
    }
    else {
        _counter = 0;
    }
    [self.tableView reloadData];
}

- (void)barButton:(id)sender {
    
    if (_btnOption != _counter){
        
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Enough images not selected" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
    }

    else {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"TestNotification" object:_array];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return [_arrPics count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil){
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        UIImageView *imageView = [[UIImageView alloc ]initWithFrame:CGRectMake(10, 10, 130, 130)];
        [imageView setTag:999];

        [cell.contentView addSubview:imageView];
       
    }
    
        // Configure the cell...
    
    NSString *filePath = [[Singleton sharedSingleton] getDocumentDirectoryPath];
    
    KinPics *kinPics = [_arrPics objectAtIndex:indexPath.row];
    
    NSString *fileName = [filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"small%@",[kinPics name]]];
    
    UIImage *image = [UIImage imageWithContentsOfFile:fileName];
    UIImageView *imageView = (UIImageView *)[cell.contentView viewWithTag:999];
    [imageView setImage:image];

    if ([[_array objectAtIndex:indexPath.row ] boolValue] == 1) {
        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    }
    else {
        
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        
    }

    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 150;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    return [NSString stringWithFormat:@"select %d Image", _btnOption];
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];

    if ([[_array objectAtIndex:indexPath.row] boolValue] == 0) {
        
        if (_btnOption != _counter) {
            
            [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            [_array replaceObjectAtIndex:indexPath.row withObject:@"1"];
            _counter ++;
        }
        else{
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Enough images selected" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];

        }

    }
    else {
        
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        [_array replaceObjectAtIndex:indexPath.row withObject:@"0"];
        _counter --;
    
    }
 
}

@end
