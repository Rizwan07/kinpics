//
//  TableViewController.h
//  Kin pics
//
//  Created by Rizwan on 2/16/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewController : UITableViewController{
    NSMutableArray *_array;
    
    NSArray *_arrPics;
}

@property (nonatomic, retain) NSArray *arrayPics;
@property (nonatomic, assign) int btnOption;

@end
