//
//  PlayDifferentLookViewController.m
//  Kin pics
//
//  Created by Rizwan on 2/3/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import "PlayDifferentLookViewController.h"

@interface PlayDifferentLookViewController ()

@end

@implementation PlayDifferentLookViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    didScroll = YES;
	// Do any additional setup after loading the view.
    mutableArrPics = [[NSMutableArray alloc] initWithCapacity:0];

}

- (void)viewDidAppear:(BOOL)animated {
    
    previousSelectedOption = -1;
    
    [super viewDidAppear:animated];

    [mutableArrPics removeAllObjects];

    [self removePreviousAddedImagesFromScrollView];
    
    NSArray *arrPics = [[Singleton sharedSingleton] getDataOfAllKinPics];
    
    NSInteger numberOfImages = [arrPics count];
    
    [theScrollView setContentSize:CGSizeMake(theScrollView.frame.size.width*numberOfImages, theScrollView.frame.size.height)];
    
    NSString *filePath = [[Singleton sharedSingleton] getDocumentDirectoryPath];
    
    for (int i = 0; i < numberOfImages; i++) {
        
        KinPics *kinPics = [arrPics objectAtIndex:i];
        
        NSString *fileName = [filePath stringByAppendingPathComponent:[kinPics name]];
        
        int xOffset = i * theScrollView.bounds.size.width;
        
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(10 + xOffset, 10, theScrollView.frame.size.width - 20, theScrollView.frame.size.height-20)];
        [imgView setImage:[UIImage imageWithContentsOfFile:fileName]];
        [imgView setContentMode:UIViewContentModeScaleAspectFit];
        [imgView setTag:1000+i];

        [mutableArrPics addObject:imgView.image];

        [theScrollView addSubview:imgView];
        
    }
    if ([mutableArrPics count]) {
        
        myImage = [mutableArrPics[0] copy];
    }
    else {
        myImage = nil;
    }
}

- (void)removePreviousAddedImagesFromScrollView {
    
    for (UIView *view in [theScrollView subviews]) {
        [view removeFromSuperview];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backBtnPressed:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    [[Singleton sharedSingleton] setDidHomeBtnPressed:NO];
}

- (IBAction)homeBtnPressed:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    [[Singleton sharedSingleton] setDidHomeBtnPressed:YES];
    
}

- (void)scrollViewScrollToRect:(CGRect )rect {
    
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionAllowAnimatedContent
                     animations:^{
                         [theScrollView setAlpha:0.7];
                         //[self removePreviousAddedImagesFromView];
                     }
                     completion:^(BOOL finished){
                         
                         [UIView animateWithDuration:0.5
                                               delay:0
                                             options:UIViewAnimationOptionCurveEaseIn
                                          animations:^{
                                              [theScrollView setAlpha:1.0];
                                              [theScrollView scrollRectToVisible:rect animated:YES];
                                          }
                                          completion:nil];
                     }];

}

- (IBAction)previousBtnPressed:(id)sender {
    
    if (didScroll) {
        
        CGRect rect = [theScrollView frame];
        rect.origin = [theScrollView contentOffset];
        rect.origin.x -= theScrollView.frame.size.width;
        if (rect.origin.x >= 0) {
            didScroll = NO;
            [self scrollViewScrollToRect:rect];
        }
    }
    
}

- (IBAction)differentLookBtnPressed:(id)sender {
    int tag = [sender tag];
    
    if (tag == previousSelectedOption || myImage == nil) {
        return;
    }
    previousSelectedOption = tag;
    
    UIImage *anImage = [myImage copy];    
    
    switch (tag) {
        case 1:
            [self lookFour:anImage];
            break;
        case 2:
            [self makeImageSepiton:anImage];
            break;
        case 3:
            [self makeImageNegative:anImage];
            break;
        case 4:
            [self lookOne:anImage];
            break;
        default:
            break;
    }
}
- (void)lookOne:(UIImage *)anImage {
    
    int currentImageIndex = theScrollView.contentOffset.x/theScrollView.frame.size.width;

//    float weight[5] = {0.2270270270, 0.1945945946, 0.1216216216, 0.0540540541, 0.0162162162};
//    // Blur horizontally
//    UIGraphicsBeginImageContextWithOptions(anImage.size, NO, 1.0);
//    [anImage drawInRect:CGRectMake(0, 0, anImage.size.width, anImage.size.height) blendMode:kCGBlendModePlusLighter alpha:weight[0]];
//    for (int x = 1; x < 5; ++x) {
//        [anImage drawInRect:CGRectMake(x, 0, anImage.size.width, anImage.size.height) blendMode:kCGBlendModePlusDarker alpha:weight[x]];
//        [anImage drawInRect:CGRectMake(-x, 0, anImage.size.width, anImage.size.height) blendMode:kCGBlendModePlusDarker alpha:weight[x]];
//    }
//    UIImage *horizBlurredImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    // Blur vertically
//    UIGraphicsBeginImageContextWithOptions(anImage.size, NO, anImage.scale);
//    [horizBlurredImage drawInRect:CGRectMake(0, 0, anImage.size.width, anImage.size.height) blendMode:kCGBlendModePlusLighter alpha:weight[0]];
//    for (int y = 1; y < 5; ++y) {
//        [horizBlurredImage drawInRect:CGRectMake(0, y, anImage.size.width, anImage.size.height) blendMode:kCGBlendModePlusLighter alpha:weight[y]];
//        [horizBlurredImage drawInRect:CGRectMake(0, -y, anImage.size.width, anImage.size.height) blendMode:kCGBlendModePlusLighter alpha:weight[y]];
//    }
//    UIImage *blurredImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
    [(UIImageView *)[theScrollView viewWithTag:1000+currentImageIndex] setImage:anImage];

}
- (void)lookFour:(UIImage *)anImage {
    int currentImageIndex = theScrollView.contentOffset.x/theScrollView.frame.size.width;

    UIImage *bottomImage = anImage;
    UIImage *image       = [UIImage imageNamed:@"Rainbow.png"]; //foreground image
    
    CGSize newSize = bottomImage.size;
    UIGraphicsBeginImageContext( newSize );
    
    // Use existing opacity as is
    [bottomImage drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    
    // Apply supplied opacity if applicable
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height) blendMode:kCGBlendModeNormal alpha:0.8];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    [(UIImageView *)[theScrollView viewWithTag:1000+currentImageIndex] setImage:newImage];

}
- (void)makeImageNegative:(UIImage *)image{
    int currentImageIndex = theScrollView.contentOffset.x/theScrollView.frame.size.width;
    
    UIGraphicsBeginImageContext(image.size);
    CGContextSetBlendMode(UIGraphicsGetCurrentContext(), kCGBlendModeCopy);
    [image drawInRect:CGRectMake(0, 0, image.size.width, image.size.height)];
    CGContextSetBlendMode(UIGraphicsGetCurrentContext(), kCGBlendModeDifference);
    CGContextSetFillColorWithColor(UIGraphicsGetCurrentContext(),[UIColor whiteColor].CGColor);
    CGContextFillRect(UIGraphicsGetCurrentContext(), CGRectMake(0, 0,   image.size.width, image.size.height));
    UIImage *returnImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [(UIImageView *)[theScrollView viewWithTag:1000+currentImageIndex] setImage:returnImage];
    
}
- (void)makeImageSepiton:(UIImage *)anImage {
    
    int currentImageIndex = theScrollView.contentOffset.x/theScrollView.frame.size.width;
    
    UIGraphicsBeginImageContext(anImage.size);
    CGContextSetBlendMode(UIGraphicsGetCurrentContext(), kCGBlendModeCopy);
    [anImage drawInRect:CGRectMake(0, 0, anImage.size.width, anImage.size.height)];
    CGContextSetBlendMode(UIGraphicsGetCurrentContext(), kCGBlendModeColorBurn);
    CGContextSetFillColorWithColor(UIGraphicsGetCurrentContext(),[UIColor redColor].CGColor);
    CGContextFillRect(UIGraphicsGetCurrentContext(), CGRectMake(0, 0,   anImage.size.width, anImage.size.height));
    UIImage *newImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [(UIImageView *)[theScrollView viewWithTag:1000+currentImageIndex] setImage:newImg];
    
}

- (IBAction)forwardBtnPressed:(id)sender {
    
    if (didScroll) {
        
        CGRect rect = [theScrollView frame];
        rect.origin = [theScrollView contentOffset];
        rect.origin.x += theScrollView.frame.size.width;
        if (rect.origin.x < theScrollView.contentSize.width) {
            didScroll = NO;
            [self scrollViewScrollToRect:rect];
        }
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    didScroll = YES;
    
    previousSelectedOption = -1;
    
    int currentImageIndex = theScrollView.contentOffset.x/theScrollView.frame.size.width;
    myImage = [mutableArrPics[currentImageIndex] copy];

}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    
    didScroll = YES;
    
    previousSelectedOption = -1;
    
    int currentImageIndex = theScrollView.contentOffset.x/theScrollView.frame.size.width;
    myImage = [mutableArrPics[currentImageIndex] copy];

}

@end
