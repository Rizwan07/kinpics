//
//  ViewController.m
//  Kin pics
//
//  Created by Rizwan on 1/17/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import "ViewController.h"
#import "Singleton.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
}

- (void)performSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"SetUpSegue"]) {
        NSLog(@"Setup Segue");
        [[Singleton sharedSingleton] setDidEnterInSetUpOptions:YES];
    }
    else {
        NSLog(@"Play Segue");
        [[Singleton sharedSingleton] setDidEnterInSetUpOptions:NO];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
