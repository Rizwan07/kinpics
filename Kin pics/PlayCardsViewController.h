//
//  PlayCardsViewController.h
//  Kin pics
//
//  Created by Rizwan on 2/3/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//
@class GameConfig;
#import <UIKit/UIKit.h>
#import "CardView.h"
#import <AVFoundation/AVFoundation.h>

@interface PlayCardsViewController : UIViewController<UIPopoverControllerDelegate> {
    
    IBOutlet UIButton *btnSixOption;
    IBOutlet UIButton *btnTwelveOption;
    IBOutlet UIButton *btnEighteenOption;
    UIView *_gameView;
    unsigned int _gameState;
    IBOutlet UIButton *playBtn;
    
    NSMutableArray *_cardViews;
    NSMutableArray *_activeCardViews;
    NSMutableArray *_selectedCards;
    
    GameConfig *_gameConfig;
    UIPopoverController *popOver;
    IBOutlet UIImageView *image;
    IBOutlet UILabel *lblJob;
    CardView* newView;
    IBOutlet UIView *jobView;

}

- (IBAction)backBtnPressed:(id)sender;
- (IBAction)homeBtnPressed:(id)sender;

- (IBAction)cardOptionBtnsPressed:(id)sender;
- (IBAction)playBtnPressed:(id)sender;
@property (nonatomic,retain) GameConfig* gameConfig;

@end
