//
//  PuzzleImageView.m
//  Kin pics
//
//  Created by Rizwan on 2/16/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import "PuzzleImageView.h"

@implementation PuzzleImageView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.userInteractionEnabled = YES;

    }
    return self;
}

- (void) shakeImage:(UIImageView *) imageView value: (BOOL) on
{
    if(on==NO)
    {
          [imageView.layer removeAnimationForKey:@"shakeAnimation"];
    }
    else
    {
         CGFloat rotation = 0.05;
        
        CABasicAnimation *shake = [CABasicAnimation animationWithKeyPath:@"transform"];
         shake.duration = 0.13;
         shake.autoreverses = YES;
         shake.repeatCount = 1000;
         shake.removedOnCompletion = NO;
         shake.fromValue = [NSValue valueWithCATransform3D:CATransform3DRotate(self.layer.transform,-rotation, 0.0 ,0.0 ,1.0)];
         shake.toValue = [NSValue valueWithCATransform3D:CATransform3DRotate(self.layer.transform, rotation, 0.0 ,0.0 ,1.0)];
         
         [imageView.layer addAnimation:shake forKey:@"shakeAnimation"];
    }
}


- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
  //  NSLog(@"touches began : %i",self.tag);
    
    if ([self.pDelegate respondsToSelector:@selector(passCallWhenImageViewIsTapped:)])
    {
        [self.pDelegate passCallWhenImageViewIsTapped:self];
    }
}

- (void) touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
   // NSLog(@"touches cancelled");
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
  //  NSLog(@"touches ended");
    
}

- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
   // NSLog(@"touches moved");
  //  NSLog(@"x: %f , y: %f",self.frame.origin.x,self.frame.origin.y);
    if ([touches count]==1) {
        
        UITouch *touch = [touches anyObject];
        CGPoint p0 = [touch previousLocationInView:self];
        CGPoint p1 = [touch locationInView:self];
        
        
        if (self.tag == 1)
        {
            if(CGRectContainsRect(self.originRect, self.frame))
            
//            CGRect rect = CGRectMake(self.originRect.origin.x + self.originRect.size.width/2 - 1, self.originRect.origin.y + self.originRect.size.height/2 - 1, self.originRect.origin.x + self.originRect.size.width/2 + 1, self.originRect.origin.y + self.originRect.size.height/2 + 1);
//            
//            if (CGRectContainsPoint(rect, self.center))
            {
                self.frame = CGRectMake(174, 215, self.frame.size.width, self.frame.size.height);
                if ([self.pDelegate respondsToSelector:@selector(movePuzzlePieceToBG:)])
                {
                    [self.pDelegate movePuzzlePieceToBG:self];
                }
                
                return;
            }
            
            
        }
        else if (self.tag == 2)
        {
            if(CGRectContainsRect(self.originRect, self.frame))
            {
                self.frame = CGRectMake(174, 31, self.frame.size.width, self.frame.size.height);
                if ([self.pDelegate respondsToSelector:@selector(movePuzzlePieceToBG:)])
                {
                    [self.pDelegate movePuzzlePieceToBG:self];
                }
                
                return;
            }
        }
        else if (self.tag == 3)
        {
            if(CGRectContainsRect(self.originRect, self.frame))
            {
                self.frame = CGRectMake(170, 29, self.frame.size.width, self.frame.size.height);
                
                if ([self.pDelegate respondsToSelector:@selector(movePuzzlePieceToBG:)])
                {
                    [self.pDelegate movePuzzlePieceToBG:self];
                }
                
                return;
            }
        }
        else if (self.tag == 4)
        {
            if(CGRectContainsRect(self.originRect, self.frame))
            {
                self.frame = CGRectMake(372, 29, self.frame.size.width, self.frame.size.height);
                if ([self.pDelegate respondsToSelector:@selector(movePuzzlePieceToBG:)])
                {
                    [self.pDelegate movePuzzlePieceToBG:self];
                }
                
                return;
            }
        }
        else if (self.tag == 5)
        {
            if(CGRectContainsRect(self.originRect, self.frame))
            {
                self.frame = CGRectMake(170, 229, self.frame.size.width, self.frame.size.height);
                if ([self.pDelegate respondsToSelector:@selector(movePuzzlePieceToBG:)])
                {
                    [self.pDelegate movePuzzlePieceToBG:self];
                }
                
                return;
            }
        }
        else if (self.tag == 6)
        {
            if(CGRectContainsRect(self.originRect, self.frame))
            {
                self.frame = CGRectMake(320, 229, self.frame.size.width, self.frame.size.height);
                
                if ([self.pDelegate respondsToSelector:@selector(movePuzzlePieceToBG:)])
                {
                    [self.pDelegate movePuzzlePieceToBG:self];
                }
                
                return;
            }
        }
        else if (self.tag == 7)
        {
            if(CGRectContainsRect(self.originRect, self.frame))
            {
                self.frame = CGRectMake(170, 26, self.frame.size.width, self.frame.size.height);
                
                if ([self.pDelegate respondsToSelector:@selector(movePuzzlePieceToBG:)])
                {
                    [self.pDelegate movePuzzlePieceToBG:self];
                }
                
                return;
            }
        }
        else if (self.tag == 8)
        {
            if(CGRectContainsRect(self.originRect, self.frame))
            {
                self.frame = CGRectMake(373, 26, self.frame.size.width, self.frame.size.height);
                
                if ([self.pDelegate respondsToSelector:@selector(movePuzzlePieceToBG:)])
                {
                    [self.pDelegate movePuzzlePieceToBG:self];
                }
                
                return;
            }
        }
        else if (self.tag == 9)
        {
            if(CGRectContainsRect(self.originRect, self.frame))
            {
                self.frame = CGRectMake(170, 121, self.frame.size.width, self.frame.size.height);
                
                if ([self.pDelegate respondsToSelector:@selector(movePuzzlePieceToBG:)])
                {
                    [self.pDelegate movePuzzlePieceToBG:self];
                }
                return;
            }
        }
        else if (self.tag == 10)
        {
            if(CGRectContainsRect(self.originRect, self.frame))
            {
                self.frame = CGRectMake(333, 160, self.frame.size.width, self.frame.size.height);
                if ([self.pDelegate respondsToSelector:@selector(movePuzzlePieceToBG:)])
                {
                    [self.pDelegate movePuzzlePieceToBG:self];
                }
                
                return;
            }
        }
        else if (self.tag == 11)
        {
            if(CGRectContainsRect(self.originRect, self.frame))
            {
                self.frame = CGRectMake(170, 256, self.frame.size.width, self.frame.size.height);
                
                if ([self.pDelegate respondsToSelector:@selector(movePuzzlePieceToBG:)])
                {
                    [self.pDelegate movePuzzlePieceToBG:self];
                }
                
                return;
            }
        }
        else if (self.tag == 12)
        {
            if(CGRectContainsRect(self.originRect, self.frame))
            {
                self.frame = CGRectMake(333, 296, self.frame.size.width, self.frame.size.height);
                if ([self.pDelegate respondsToSelector:@selector(movePuzzlePieceToBG:)])
                {
                    [self.pDelegate movePuzzlePieceToBG:self];
                }
                
                return;
            }
        }
        else if (self.tag == 13)
        {
            if(CGRectContainsRect(self.originRect, self.frame))
            {
                self.frame = CGRectMake(170, 25, self.frame.size.width, self.frame.size.height);
                if ([self.pDelegate respondsToSelector:@selector(movePuzzlePieceToBG:)])
                {
                    [self.pDelegate movePuzzlePieceToBG:self];
                }
                
                return;
            }
        }
        else if (self.tag == 14)
        {
            if(CGRectContainsRect(self.originRect, self.frame))
            {
                self.frame = CGRectMake(334, 25, self.frame.size.width, self.frame.size.height);
                
                if ([self.pDelegate respondsToSelector:@selector(movePuzzlePieceToBG:)])
                {
                    [self.pDelegate movePuzzlePieceToBG:self];
                }
                
                return;
            }
        }
        else if (self.tag == 15)
        {
            if(CGRectContainsRect(self.originRect, self.frame))
            {
                self.frame = CGRectMake(170, 86, self.frame.size.width, self.frame.size.height);
                
                if ([self.pDelegate respondsToSelector:@selector(movePuzzlePieceToBG:)])
                {
                    [self.pDelegate movePuzzlePieceToBG:self];
                }
                
                return;
            }
        }
        else if (self.tag == 16)
        {
            if(CGRectContainsRect(self.originRect, self.frame))
            {
                self.frame = CGRectMake(334, 129, self.frame.size.width, self.frame.size.height);
                
                if ([self.pDelegate respondsToSelector:@selector(movePuzzlePieceToBG:)])
                {
                    [self.pDelegate movePuzzlePieceToBG:self];
                }
                
                return;
            }
        }
        else if (self.tag == 17)
        {
            if(CGRectContainsRect(self.originRect, self.frame))
            {
                self.frame = CGRectMake(170, 190, self.frame.size.width, self.frame.size.height);
                if ([self.pDelegate respondsToSelector:@selector(movePuzzlePieceToBG:)])
                {
                    [self.pDelegate movePuzzlePieceToBG:self];
                }
                
                return;
            }
        }
        else if (self.tag == 18)
        {
            if(CGRectContainsRect(self.originRect, self.frame))
            {
                self.frame = CGRectMake(334, 193, self.frame.size.width, self.frame.size.height);
                if ([self.pDelegate respondsToSelector:@selector(movePuzzlePieceToBG:)])
                {
                    [self.pDelegate movePuzzlePieceToBG:self];
                }
                
                return;
            }
        }
        else if (self.tag == 19)
        {
            if(CGRectContainsRect(self.originRect, self.frame))
            {
                self.frame = CGRectMake(170, 293, self.frame.size.width, self.frame.size.height);
                
                if ([self.pDelegate respondsToSelector:@selector(movePuzzlePieceToBG:)])
                {
                    [self.pDelegate movePuzzlePieceToBG:self];
                }
                
                return;
            }
        }
        else if (self.tag == 20)
        {
            if(CGRectContainsRect(self.originRect, self.frame))
            {
                self.frame = CGRectMake(374, 334, self.frame.size.width, self.frame.size.height);
                
                if ([self.pDelegate respondsToSelector:@selector(movePuzzlePieceToBG:)])
                {
                    [self.pDelegate movePuzzlePieceToBG:self];
                }
                
                return;
            }
        }

        
        CGPoint center = self.center;
        center.x += p1.x - p0.x;
        center.y += p1.y - p0.y;
        self.center = center;
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
