//
//  Card.h
//  Kin pics
//
//  Created by Rizwan on 2/14/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Card : NSObject
{
    unsigned int _identifier;
    UIImage *_image;
}

@property (nonatomic,assign) unsigned int identifier;
@property (nonatomic,retain) UIImage* image;

- (id) initWithIdentifier:(unsigned int)initId image:(UIImage*)initImage;
- (id) initWithCard:(Card*)anotherCard;
@end
