//
//  KinPicsVoice.h
//  Kin pics
//
//  Created by Rizwan on 4/1/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KinPicsVoice : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSData  *textVoice;
@property (nonatomic, strong) NSData  *heartVoice;

@end
