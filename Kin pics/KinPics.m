//
//  KinPics.m
//  Kin pics
//
//  Created by Rizwan on 1/26/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import "KinPics.h"

@implementation KinPics

@synthesize name        = _name;
@synthesize text        = _text;
@synthesize slices      = _slices;

@end
