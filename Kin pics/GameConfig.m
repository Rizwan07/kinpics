//
//  GameConfig.m
//  Kin pics
//
//  Created by Rizwan on 2/14/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import "GameConfig.h"

@implementation GameConfig
@synthesize gameMode = _gameMode;
@synthesize numRows = _numRows;
@synthesize numColumns = _numColumns;
@synthesize imageLibName = _imageLibName;

- (id)initWithImageLibraryName:(NSString *)libname numRows:(unsigned int)rows numColumns:(unsigned int)cols
{
    self = [super init];
    if (self)
    {
        _imageLibName = libname;
        
        unsigned int total = rows * cols;
        if(0 < (total % 2))
        {
           
            if(cols < rows)
            {
                --rows;
            }
            else
            {
                --cols;
            }
        }
        if(2 > rows)
        {
            rows = 2;
        }
        if(2 > cols)
        {
            cols = 2;
        }
        _numRows = rows;
        _numColumns = cols;
        
        _gameMode = GAMEMODE_SINGLEPLAYER;
    }
    return self;
}

@end
