//
//  PuzzleViewController.m
//  Kin pics
//
//  Created by Rizwan on 1/21/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import "SetUpPuzzleViewController.h"
#import "SetUpImageSlideShowViewController.h"
#import "KinPicsDB.h"

#define TILE_SPACING    4
#define SHUFFLE_NUMBER	100

@interface SetUpPuzzleViewController ()

@end

@implementation SetUpPuzzleViewController
@synthesize tiles;
@synthesize orgImage = _orgImage;

int NUM_HORIZONTAL_PIECES = 1;
int NUM_VERTICAL_PIECES = 1;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    didScroll = YES;
    touchCancel = YES;
    
    self.tiles = [[NSMutableArray alloc]init];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self setScrollViewImages];
}

- (void)setScrollViewImages {
    
    [self removePreviousAddedImagesFromScrollView];
    
    arrPics = [[Singleton sharedSingleton] getDataOfAllKinPics];
    
    NSInteger numberOfImages = [arrPics count];
    
    [theScrollView setContentSize:CGSizeMake(theScrollView.frame.size.width*numberOfImages, theScrollView.frame.size.height)];
    
    NSString *filePath = [[Singleton sharedSingleton] getDocumentDirectoryPath];
    
    for (int i = 0; i < numberOfImages; i++) {
        
        KinPics *kinPics = [arrPics objectAtIndex:i];
        
        //NSString *fileName = [filePath stringByAppendingPathComponent:[kinPics name]];
        NSString *fileName = [filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"small%@",[kinPics name]]];
        
        UIImage *image = [UIImage imageWithContentsOfFile:fileName];
        _orgImage = image;
        
        [self initPuzzleWithImageIndex:i];

//        int xOffset = i * theScrollView.bounds.size.width;
//
//        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(10 + xOffset, 10, theScrollView.frame.size.width - 20, theScrollView.frame.size.height)];
//        [imgView setImage:image];
//        [imgView setContentMode:UIViewContentModeCenter];
//        
//        [imgView setTag:1000+i];
//        [theScrollView addSubview:imgView];
        
    }
    [self setOtherData];

}

- (void)removePreviousAddedImagesFromScrollView {
    
    for (UIView *view in [theScrollView subviews]) {
        [view removeFromSuperview];
    }
}

- (void)setOtherData {
    
    int currentImageIndex = theScrollView.contentOffset.x/theScrollView.frame.size.width;
    
    if (currentImageIndex < [arrPics count]) {
        
        //_orgImage = (UIImage *)[(UIImageView *)[theScrollView viewWithTag:1000+currentImageIndex] image];
        
        KinPics *kinPic = [arrPics objectAtIndex:currentImageIndex];
        [lblPicName setText:[kinPic text]];
        //[self setBtnEnabled];
        
        [self setBtnDisableWithTag:[kinPic slices]];
        //[self initPuzzleWithImageIndex:currentImageIndex];
        
    }
}

-(void)setBtnEnabled {
    
    [btnTwoOption setEnabled:YES];
    [btnFourOption setEnabled:YES];
    [btnSixOption setEnabled:YES];
    [btnEightOption setEnabled:YES];
}


-(void) initPuzzleWithImageIndex:(int )imageIndex {
	
    [self setBtnDisableWithTag:[[arrPics objectAtIndex:imageIndex] slices]];
    [self setBtnOptions:[[arrPics objectAtIndex:imageIndex] slices]];
    
    int horizontalSpace = (theScrollView.frame.size.width - _orgImage.size.width)/2;
    int verticalSpace = (theScrollView.frame.size.height - _orgImage.size.height)/2;
	if( _orgImage == nil ){
        
		return;
	}
    
	tileWidth = _orgImage.size.width /NUM_HORIZONTAL_PIECES;
	tileHeight = _orgImage.size.height/NUM_VERTICAL_PIECES;
    
    int counter = 0;
    
	for( int x=0; x<NUM_HORIZONTAL_PIECES; x++ ){
		for( int y=0; y<NUM_VERTICAL_PIECES; y++ ){
			CGPoint orgPosition = CGPointMake(x,y);
			
			CGRect frame = CGRectMake(10 + tileWidth*x, tileHeight*y,tileWidth, tileHeight );
			CGImageRef tileImageRef = CGImageCreateWithImageInRect( _orgImage.CGImage, frame );
			UIImage *tileImage = [UIImage imageWithCGImage:tileImageRef];
			
			CGRect tileFrame =  CGRectMake((tileWidth+TILE_SPACING)*x + horizontalSpace + theScrollView.frame.size.width * imageIndex, (tileHeight+TILE_SPACING)*y + verticalSpace, tileWidth, tileHeight );
			
			tileImageView = [[Tile alloc] initWithImage:tileImage];
			tileImageView.frame = tileFrame;
			tileImageView.originalPosition = orgPosition;
			tileImageView.currentPosition = orgPosition;
            
            [tileImageView setTag:1000*(imageIndex+1)+counter];
            
			CGImageRelease( tileImageRef );
			
			//[tiles addObject:tileImageView];
            [theScrollView addSubview:tileImageView];
            counter ++;
		}
	}
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backBtnPressed:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    [[Singleton sharedSingleton] setDidHomeBtnPressed:NO];
}

- (IBAction)homeBtnPressed:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    [[Singleton sharedSingleton] setDidHomeBtnPressed:YES];
}

- (IBAction)previousBtnPressed:(id)sender {
    
    if (didScroll) {
        
        CGRect rect = [theScrollView frame];
        rect.origin = [theScrollView contentOffset];
        rect.origin.x -= theScrollView.frame.size.width;
        if (rect.origin.x >= 0) {
            didScroll = NO;
          //  [theScrollView scrollRectToVisible:rect animated:YES];
            [self scrollViewScrollToRect:rect];
            [self setBtnEnabled];
        }
    }
    
}

- (IBAction)forwardBtnPressed:(id)sender {
    
    if (didScroll) {
        
        CGRect rect = [theScrollView frame];
        rect.origin = [theScrollView contentOffset];
        rect.origin.x += theScrollView.frame.size.width;
        if (rect.origin.x < theScrollView.contentSize.width) {
            didScroll = NO;
      //      [theScrollView scrollRectToVisible:rect animated:YES];
            [self scrollViewScrollToRect:rect];
            [self setBtnEnabled];
        }
    }
}
- (void)scrollViewScrollToRect:(CGRect )rect {
    
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionAllowAnimatedContent
                     animations:^{
                         [theScrollView setAlpha:0.7];
                         //[self removePreviousAddedImagesFromView];
                     }
                     completion:^(BOOL finished){
                         
                         [UIView animateWithDuration:0.5
                                               delay:0
                                             options:UIViewAnimationOptionCurveEaseIn
                                          animations:^{
                                              [theScrollView setAlpha:1.0];
                                              [theScrollView scrollRectToVisible:rect animated:YES];
                                          }
                                          completion:nil];
                     }];
    
}

- (IBAction)sliceBtnsPressed:(id)sender {
    
    int tag = [sender tag];
    [self setBtnDisableWithTag:tag];
    [self setBtnOptions:tag];
}

- (void)setBtnDisableWithTag:(int )tag {
    
    [btnTwoOption setEnabled:tag != 0];
    [btnFourOption setEnabled:tag != 1];
    [btnSixOption setEnabled:tag != 2];
    [btnEightOption setEnabled:tag != 3];

}

- (void)setBtnOptions:(int )tag {
    
    
    switch (tag) {
        case 0:
            NUM_HORIZONTAL_PIECES = 1;
            NUM_VERTICAL_PIECES = 2;
            break;
        case 1:
            NUM_HORIZONTAL_PIECES = 2;
            NUM_VERTICAL_PIECES = 2;
            break;
        case 2:
            NUM_HORIZONTAL_PIECES = 2;
            NUM_VERTICAL_PIECES = 3;
            break;
        case 3:
            NUM_HORIZONTAL_PIECES = 2;
            NUM_VERTICAL_PIECES = 4;
            break;
            
        default:
            NUM_HORIZONTAL_PIECES = 1;
            NUM_VERTICAL_PIECES = 1;
            break;
    }
}

- (IBAction)pieceBtnPressed:(id)sender {
    
    if (![arrPics count]) {
        return;
    }
    
    if ([btnTwoOption isEnabled] && [btnFourOption isEnabled] && [btnSixOption isEnabled] && [btnEightOption isEnabled]) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please select slice option first" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else {
        
        int currentImageIndex = theScrollView.contentOffset.x/theScrollView.frame.size.width;

        KinPics *kinPic = [arrPics objectAtIndex:currentImageIndex];
        
        KinPicsDB *kinPicsDb = [[KinPicsDB alloc] init];
        
        int noOfSlices = 0;
        
        if (![btnTwoOption isEnabled]) {
            noOfSlices = 0;
        }
        else if (![btnFourOption isEnabled]) {
            noOfSlices = 1;
        }
        else if (![btnSixOption isEnabled]) {
            noOfSlices = 2;
        }
        else if (![btnEightOption isEnabled]) {
            noOfSlices = 3;
        }
        [kinPicsDb updateKinPicsImageSlicesWithName:[kinPic name] andNumberOfSlices:noOfSlices];
        //[kinPicsDb updateKinPicsImageSlicesWithName:[kinPic picsId] andNumberOfSlices:noOfSlices];
        
        [[Singleton sharedSingleton] removeAllKinPicsData];
        
        arrPics = [[Singleton sharedSingleton] getDataOfAllKinPics];
        
        [self setScrollViewImages];
        
        KinPics *kp = [arrPics objectAtIndex:currentImageIndex];
        
        CGAffineTransform transform = CGAffineTransformMakeScale(0.8, 0.8);
        
        [UIView transitionWithView:theScrollView duration:1.0 options:UIViewAnimationOptionTransitionNone animations:^ {
            
            for (int i=0; i<=([kp slices]+1)*2; i++) {
                Tile *tile = (Tile *)[theScrollView viewWithTag:1000*(currentImageIndex+1)+i];
                [tile setTransform:transform];
            }
            
        } completion:^(BOOL finish) {
            for (int i=0; i<=([kp slices]+1)*2; i++) {
                Tile *tile = (Tile *)[theScrollView viewWithTag:1000*(currentImageIndex+1)+i];
                [tile setTransform:CGAffineTransformIdentity];
            }
        }];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    didScroll = YES;
   
    [self setOtherData];
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    
    didScroll = YES;
    
    [self setOtherData];
}

- (void)viewDidUnload {
    theScrollView = nil;
    lblPicName = nil;
    btnTwoOption = nil;
    btnFourOption = nil;
    btnSixOption = nil;
    btnEightOption = nil;
    [super viewDidUnload];
}
@end
