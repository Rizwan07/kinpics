//
//  ImageLibrary.m
//  Kin pics
//
//  Created by Rizwan on 2/19/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import "ImageLibrary.h"

@implementation ImageLibrary

@synthesize images = _images;

- (id) init {
    self = [super init];
    if(self)
    {
        int selectedOption = [[Singleton sharedSingleton] selectedOption];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setBool:YES forKey:@"cards"];
        NSString *filePath = [[Singleton sharedSingleton] getDocumentDirectoryPath];
        
        _images = [NSMutableDictionary dictionary];
        
        NSString *str = [NSString stringWithFormat:@"%dcards", selectedOption];
        
        if ([defaults boolForKey:str]) {
            
            for (int i=0; i<selectedOption; i++) {
                
                NSString *key = [NSString stringWithFormat:@"%d_%d", i, selectedOption];
                NSString *fileName = [filePath stringByAppendingPathComponent:[defaults stringForKey:key]];
                
                
                UIImage* newImage = [UIImage imageWithContentsOfFile:fileName];
                if(newImage)
                {
                    [_images setObject:newImage forKey:key];
                }

            }
        }
        else {
            [defaults setBool:NO forKey:@"cards"];
            NSString* path = [[NSBundle mainBundle] pathForResource:@"CardImage" ofType:@"plist"];
            NSDictionary* config = [NSDictionary dictionaryWithContentsOfFile:path];
            NSArray* nameArray = [config objectForKey:@"images"];
            
            
            for(NSString* cur in nameArray)
            {
                UIImage* newImage = [UIImage imageNamed:cur];
                if(newImage)
                {
                    [_images setObject:newImage forKey:cur];
                }
            }

        }
        
    }
    return self;
}


@end
