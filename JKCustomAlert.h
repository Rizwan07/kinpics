//
//  JKCustomAlert.h
//  CustomAlert
//
//  Created by Rizwan on 2/16/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface JKCustomAlert : UIAlertView {
	UILabel *alertTextLabel;
	UIImage *backgroundImage;
}

@property(readwrite, retain) UIImage *backgroundImage;
@property(readwrite, retain) NSString *alertText;

- (id) initWithImage:(UIImage *)backgroundImage text:(NSString *)text;

@end
