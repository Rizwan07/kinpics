//
//  ImageLibrary.h
//  Kin pics
//
//  Created by Rizwan on 2/19/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageLibrary : NSObject

{
    NSMutableDictionary *_images;
}
@property (nonatomic,retain) NSMutableDictionary *images;

- (id) init;

@end
